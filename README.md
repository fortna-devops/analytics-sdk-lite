# Analytics Toolbox

This library contains a series of functions, objects, and tools to be used across all analytics development. Standardized implementation of multiple common functions and operations will be contained in various modules. Will be available for usage within the AWS environment

i.e.

`from analytics_sdk_core.data_init.data_pull import DataQueryEngineLite as DQE`

Currently planned features before 1.0.0:

- Type checking for DQE, DateWriter, CacheManager, ConfigManager, testing_utilities
- Email configuration management
- SES module


*under development*

To install:

Activate the environment you would like to install the toolbox into, run the follwing line:
`python setup.py install`


