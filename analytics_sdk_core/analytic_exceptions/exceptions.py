#!/usr/bin/env python3
"""
Module for custom exception classes
..author: Oluwatosin Sonuyi <oluwatosinsonuyi@mhsinc.net>
"""


class WrongDataType(Exception):
    """used when someone passes in wrong data type"""

class TableColumnMismatch(Exception):
    """used when someone passes in data that will break table writing"""

class TimestampFormatError(Exception):
    """used when someone passes in bad data format for timestamp"""
