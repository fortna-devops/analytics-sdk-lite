#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Functions to create mock data.

.. sectionauthor:: Julia Varzari

"""
# pylint: disable=line-too-long, too-many-locals, too-many-arguments, too-many-nested-blocks
# pylint: disable=invalid-name, too-many-branches, no-else-raise, dangerous-default-value
# to generate random strings and integers
from collections import OrderedDict
import datetime
import json
import random
import string
import time
from typing import List, Union, Optional
import warnings

from analytics_sdk_core.constants.databases import schemas as table_schemas
from analytics_sdk_core.testing_utilities import table_types, float_ranges


# hardcoded from location_81 data
THRESHOLDS = {"temperature": {"red": 140, "orange": 122}, "hf_rms_acceleration_x": {"red": 6.0, "orange": 1.5}, "hf_rms_acceleration_z": {
    "red": 6.0, "orange": 1.5}, "rms_velocity_z": {"red": 0.37, "orange": 0.3}, "rms_velocity_x": {"red": 0.37, "orange": 0.3}}
RESIDUAL_THRESHOLDS = {"rms_velocity_x_residuals": [-0.011410781574327133, -0.006972585729418752, 0.007701059612683913, 0.012139255457592295], "rms_velocity_z_residuals": [
    -0.0037199919098117196, -0.0019822223966851774, 0.003763223368442768, 0.00550099288156931], "temperature_residuals": [-6.417759549039968, -3.2984853540498706, 8.636347073047634, 11.755621268037732]}
ALARMS_CONFIDENCE = {"rms_velocity_x": 1.0,
                     "rms_velocity_z": 0.3807, "temperature": 0.3197}

SENSOR_DISTRIBUTIONS = ['random', 'gaussian']


def generate_random_string(length: int = 5) -> str:
    """
    Generates a string with random uppercase letters, lowercase letters, and numbers with
    the length given.

    :param length: how many characters long the string will be
    :type length: int

    :returns: a random string
    :rtype: str

    """
    return ''.join(random.choice(string.ascii_letters + string.digits) for _ in range(length))


def generate_random_int(lowerlim: int = 0, upperlim: int = 50) -> int:
    """
    Generates a random integer between the limits given.

    :param lowerlim: the minimum value for the random integer
    :type lowerlim: int

    :param upperlim: the maximum value for the random integer
    :type upperlim: int

    :returns: a random integer
    :rtype: int

    """

    return random.randint(lowerlim, upperlim)


def generate_random_timestamp(lowerlim: Union[float, int, None] = None, upperlim: Union[float, int, None] = None) -> str:
    """
    Generates a random timestamp between the limits given.

    :param lowerlim: the minimum time as a unix timestamp for the random date generated
    :type lowerlim: float or int

    :param upperlim: the maxumum time as a unix timestamp for the random date generated
    :type upperlim: float or int

    :returns: a random timestamp as a string in the format yyyy-mm-dd HH:MM:SS. example: '2019-02-24 00:43:02'
    :rtype: string

    """

    if not lowerlim:
        # the time a week (604800 seconds) ago from now
        lowerlim = time.time() - 604800

    if not upperlim:
        # the current time
        upperlim = time.time()

    # generating a random timestamp within the range
    random_timestamp = random.uniform(lowerlim, upperlim)

    # converting the timestamp into a string
    timestamp_string = datetime.datetime.fromtimestamp(
        int(random_timestamp)).strftime('%Y-%m-%d %H:%M:%S')

    return timestamp_string


def create_mock_alarms_data(alarms: int = 5, fields: List[str] = table_schemas.ALARMS, location_table: Optional[List[OrderedDict]] = None, timestamp_lowerlim: Union[float, int, None] = None, timestamp_upperlim: Union[float, int, None] = None, backticks: bool = True, num_sensors: int = 1) -> List[OrderedDict]:
    """
    This function creates a mock alarms data table. If a location table is passed in, the fields
    that both tables share will be used. Otherwise, the function will call `create_mock_location_data`
    itself. If fields are missing from the location table, it will raise an error.

    String data types will have a random string with a length of 5 generated. Lowercase letters, uppercase letters,
    and numbers will be used. Integer data types will have a random number between 0 and 50 generated.
    Float data types will have a random float between 0 and 50 generated. Those with a string
    dictionary will be hardcoded. Unix timestamps will have a random date within the last week.

    :param alarms: number of alarms per sensor
    :type alarms: int

    :param fields: fields for which data needs to be created
    :type fields: list

    :param location_table: a location table
    :type location_table: dict

    :param timestamp_lowerlim: The minimum time as a **unix** timestamp to generate a random date. If not \
    passed in, the date and time a week ago will be used.
    :type timestamp_lowerlim: float or int

    :param timestamp_upperlim: The maxumum time as a **unix** timestamp to generate a random date. If not \
    passed in, the current date and time will be used.
    :type timestamp_upperlim: float or int

    :param backticks: option to include backticks for the stringdict thresholds
    :type backticks: boolean

    :returns: alarm mock data
    :rtype: list of dict

    .. todo:: maybe give the option of sensors as a parameter if a location table is not \
    passed in

    ..todo:: maybe add the option of generating random values for 'value' instead of only \
    normally distributed values

    """

    alarm_types = table_types.ALARM_TYPES

    gaussian_values = float_ranges.ALARM_KEY_VALUES_GAUSSIAN

    # warning the user that if you want the `value` field without the `key` field then
    # a random float between 0 and 50 will be calculated since `value` floats are based
    # on the key
    if 'value' in fields and 'key' not in fields:
        warnings.warn(
            "'value' field passed in without 'key' field. Random float between 0 and 100 will be generated.")

    if not location_table:
        # if there isn't a location table, generate one with the fields needed
        location_table = create_mock_location_data(
            sensors=num_sensors, fields=['sensor', 'conveyor', 'equipment']
        )

    mock_alarms_data = []

    if location_table:

        # since the length of the list should be equal to the amount of sensors
        for i, _ in enumerate(location_table):

            # and there should be `alarms` amount of alarms per sensor
            for _ in range(alarms):

                alarms_data: OrderedDict = OrderedDict()

                for field in fields:

                    # #validating the fields because user input is accepted
                    if field not in alarm_types:
                        raise AttributeError(
                            'Field(s) given does not match alarms table fields')

                    # if these fields are needed they should be taken from the location table
                    if field in ['sensor', 'conveyor', 'equipment']:
                        # making sure that the location table has 'sensor', 'conveyor', and 'equipment' fields
                        # placed in this if statement because the user might not need these fields in their alarms table
                        if field not in location_table[i]:
                            raise AttributeError(
                                'Location table is missing fields.')

                        location_table_row = location_table[i]
                        what_to_fill = location_table_row[field]
                        alarms_data[field] = what_to_fill

                    elif alarm_types[field] == str:

                        if field == 'key':
                            alarms_data['key'] = random.choice(
                                table_types.ALARM_KEY_VALUES)
                        else:
                            alarms_data[field] = generate_random_string()

                    elif alarm_types[field] == int:
                        alarms_data[field] = generate_random_int()

                    elif alarm_types[field] == float:

                        if field == 'value' and 'key' in alarms_data:
                            alarm_key = alarms_data['key']

                            mu = gaussian_values[alarm_key]['mean']
                            sigma = gaussian_values[alarm_key]['standard deviation']

                            # generating data from a normal distribution with the mean and standard devation
                            # taken from `float_ranges` constants
                            normal_float = round(random.gauss(mu, sigma), 4)

                            # assigning the normal float to the field we are on
                            alarms_data['value'] = normal_float

                        else:
                            # generating a random float number between 0 and 50
                            alarms_data[field] = round(
                                random.uniform(0.0, 100.0), 4)

                    elif alarm_types[field] == 'stringdict':

                        strdict = json.dumps(THRESHOLDS)

                        if backticks:
                            strdict = "`{}`".format(strdict)

                        # using hardcoded 'threshold' data from location 81
                        alarms_data[field] = strdict

                    elif alarm_types[field] == 'unix':
                        alarms_data[field] = generate_random_timestamp(
                            lowerlim=timestamp_lowerlim, upperlim=timestamp_upperlim)

                mock_alarms_data.append(alarms_data)
    else:
        raise AttributeError('Location table missing.')

    return mock_alarms_data


def create_mock_sensor_data(sensors: int = 0, location_table: Optional[List[OrderedDict]] = None, distribution: str = 'random', datapts: int = 5, fields: List[str] = table_schemas.SENSOR, timestamp_lowerlim: Union[float, int, None] = None, timestamp_upperlim: Union[float, int, None] = None) -> List[OrderedDict]:
    """

    :param sensors: number of sensors to generate data for (EITHER this param OR the `location_table` can be \
    passed in but NOT both)
    :type sensors: int

    :param location_table: location table to match to sensor column (EITHER this param OR the `sensors` can be \
    passed in but NOT both)
    :type location_table: list of dicts

    :param distribution: how the data should be distributed. current options: random, gaussian distribution
    :type distribution: str
    :examples: flatline, random, skew right, skew left, standard distribution

    :param datapts: number of datapoints/records/rows per sensor
    :type datapts: int

    :param fields: which fields data needs to be created for
    :type fields: list

    :param timestamp_lowerlim: The minimum time as a **unix** timestamp to generate a random date. If not \
    passed in, the date and time a week ago will be used.
    :type timestamp_lowerlim: float or int

    :param timestamp_upperlim: The maxumum time as a **unix** timestamp to generate a random date. If not \
    passed in, the current date and time will be used.
    :type timestamp_upperlim: float or int

    :returns: sensor mock data
    :rtype: list of dict

    """
    # data types for each field
    sensor_types = table_types.SENSOR_TYPES

    # ranges to generate a floats
    random_ranges = float_ranges.SENSOR_RANDOM

    # means and standard deviations to generate a normal distribution
    gaussian_values = float_ranges.SENSOR_GAUSSIAN

    # the user can pass in either `sensors` or `location_table` but not both
    if sensors > 0 and location_table:
        raise ValueError('Cannot pass in both sensors and location_table.')
    elif sensors == 0 and not location_table:
        raise ValueError('Either sensors number or location_table needed')

    # checking that the distribution passed in is a valid option
    if distribution not in SENSOR_DISTRIBUTIONS:
        warning_message = "Distribution passed does not match any valid options. Distribution has been set to random.\nDistribution options: {distrs}".format(
            distrs=SENSOR_DISTRIBUTIONS)
        warnings.warn(warning_message)
        distribution = 'random'

    # if the location table is passed in, we need to know how many sensors there are
    if location_table:
        sensors = len(location_table)

    # if we want something other than random generation then this loop would be needed
    # in order to do the data generation and the loop below it would be used for
    # assigning the data only

    # for sensor in range(sensors):
    # 	for field in fields:
    # 		for dp in range(datapts):
    # 			#generate data by passing in the num of datapoints
    # 			#and save it somehow?? maybe a list of lists

    mock_sensor_table = []

    for sensor in range(sensors):

        for _ in range(datapts):

            sensor_data: OrderedDict = OrderedDict()

            for field in fields:

                # if the user gives a list of fields this will validate that the fields
                # match the sensor table fields
                if field not in sensor_types:
                    raise AttributeError(
                        'Field(s) given does not match sensor table fields')

                if sensor_types[field] == 'unix':
                    sensor_data['timestamp'] = generate_random_timestamp(
                        lowerlim=timestamp_lowerlim, upperlim=timestamp_upperlim)

                elif sensor_types[field] == str:

                    if field == 'sensor':
                        if location_table:
                            location_table_row = location_table[sensor]
                            what_to_fill = location_table_row['sensor']
                            sensor_data['sensor'] = what_to_fill
                        else:
                            sensor_data['sensor'] = str(sensor)
                    else:
                        sensor_data[field] = generate_random_string()

                elif sensor_types[field] == float:

                    if distribution == 'random':

                        a = random_ranges[field]['lowerlim']
                        b = random_ranges[field]['upperlim']

                        # generating a random float and rounding it to 4 digits after the decimal
                        random_float = round(random.uniform(a, b), 4)

                        # assigning the random float to the field we are on
                        sensor_data[field] = random_float

                    elif distribution == 'gaussian':

                        mu = gaussian_values[field]['mean']
                        sigma = gaussian_values[field]['standard deviation']

                        # generating data from a normal distribution with the mean and standard devation
                        # taken from `float_ranges` constants
                        normal_float = round(random.gauss(mu, sigma), 4)

                        # assigning the normal float to the field we are on
                        sensor_data[field] = normal_float

            mock_sensor_table.append(sensor_data)

    return mock_sensor_table


def create_mock_stats_data(sensors: int = 0, location_table: Optional[List[OrderedDict]] = None, fields: List[str] = table_schemas.STATS) -> List[OrderedDict]:
    """
    :param sensors: number of sensors to generate data for (EITHER this param OR the `location_table` can be \
    passed in but NOT both)
    :type sensors: int

    :param location_table: location table to match to sensor column (EITHER this param OR the `sensors` can be \
    passed in but NOT both)
    :type location_table: list of dicts

    :param fields: which fields data needs to be created for
    :type fields: list

    :returns: stats mock data
    :rtype: list of dicts

    .. note:: there are many zeros in the queried data which definitely affects the values for a normal \
    distribution. should i do something about that?? for now fix: if there is a negative number then \
    force it to be 0 since there are no negative values

    """

    # data types for each field
    stats_types = table_types.STATS_TYPES

    gaussian_values = float_ranges.STATS_GAUSSIAN

    mock_stats_table = []

    # the user can pass in either `sensors` or `location_table` but not both
    if sensors > 0 and location_table:
        raise ValueError('Cannot pass in both sensors and location_table.')
    elif sensors == 0 and not location_table:
        raise ValueError('Either sensors number or location_table needed')

    # if the location table is passed in, we need to know how many sensors there are
    if location_table:
        sensors = len(location_table)

    for sensor in range(sensors):

        stats_data: OrderedDict = OrderedDict()

        for field in fields:

            # if the user gives a list of fields this will validate that the fields
            # match the stats table fields
            if field not in stats_types:
                raise AttributeError(
                    'Field(s) given does not match stats table fields')

            if field == 'sensor':
                if location_table:
                    location_table_row = location_table[sensor]
                    what_to_fill = location_table_row['sensor']
                    stats_data['sensor'] = what_to_fill
                else:
                    stats_data['sensor'] = str(sensor)

            elif stats_types[field] == float:

                mu = gaussian_values[field]['mean']
                sigma = gaussian_values[field]['standard deviation']

                # generating data from a normal distribution with the mean and standard devation
                # taken from `float_ranges` constants
                normal_float = round(random.gauss(mu, sigma), 4)

                # if number happens to be negative force it to be a 0 since there are no negative
                # values in the real data
                if normal_float < 0.0:
                    normal_float = 0.0

                # assigning the normal float to the field we are on
                stats_data[field] = normal_float

        mock_stats_table.append(stats_data)

    return mock_stats_table


def create_mock_location_data(sensors: int = 10, fields: List[str] = table_schemas.LOCATION, backticks: bool = True) -> List[OrderedDict]:
    """
    This function creates a mock location data table. The data returned from this function
    can also be used with other functions to match data. For example, the data from the fields
    'sensor', 'conveyor', and 'equipment' can be used in the alarms table.

    String data types will have a random string with a length of 5 generated. Lowercase letters, uppercase letters,
    and numbers will be used. Integer data types will have a random number between 0 and 50 generated.
    Float data types will have a random float between 0 and 50 generated. Those with a string
    dictionary will be hardcoded.

    :param sensors: number of sensors
    :type sensors: int

    :param fields: which fields data needs to be created for
    :type fields: list

    :param backticks: option to include backticks for the stringdict thresholds
    :type backticks: boolean

    :returns: mock location table data
    :rtype: list of dict

    .. todo:: allow the user to give their own values to some of the fields

    """

    # Data types per field
    location_types = table_types.LOCATION_TYPES

    # the list that will be filled with dictionaries
    mock_location_table = []

    for sensor in range(sensors):

        location_data: OrderedDict = OrderedDict()

        for field in fields:

            # #validating the fields because user input is accepted
            if field not in location_types:
                raise AttributeError(
                    'Field(s) given does not match location table fields')

            # lookup isinstance() for == type
            if location_types[field] == str:

                # sensor numbers must be unique
                if field == 'sensor':
                    location_data['sensor'] = str(sensor)

                else:
                    location_data[field] = generate_random_string()

            elif location_types[field] == int:
                location_data[field] = generate_random_int()

            elif location_types[field] == 'stringdict':

                if field == 'thresholds':
                    strdict = json.dumps(THRESHOLDS)
                    if backticks:
                        strdict = "`{}`".format(strdict)
                    location_data[field] = strdict

                elif field == 'residual_thresholds':
                    strdict = json.dumps(RESIDUAL_THRESHOLDS)
                    if backticks:
                        strdict = "`{}`".format(strdict)
                    location_data[field] = strdict

                elif field == 'alarms_confidence':
                    strdict = json.dumps(ALARMS_CONFIDENCE)
                    if backticks:
                        strdict = "`{}`".format(strdict)
                    location_data[field] = strdict

        mock_location_table.append(location_data)

    return mock_location_table


def create_mock_timeseries_predictions_data(sensors: int = 0, location_table: Optional[List[OrderedDict]] = None, datapts: int = 5, fields: List[str] = table_schemas.TIMESERIES_PREDICTIONS, timestamp_lowerlim: Union[float, int, None] = None, timestamp_upperlim: Union[float, int, None] = None) -> List[OrderedDict]:
    """
    :param sensors: number of sensors to generate data for (EITHER this param OR the `location_table` can be \
    passed in but NOT both)
    :type sensors: int

    :param location_table: location table to match to sensor column (EITHER this param OR the `sensors` can be \
    passed in but NOT both)
    :type location_table: list of dicts

    :param datapts: number of datapoints/records/rows/timestamps per sensor
    :type datapts: int

    :param fields: which fields data needs to be created for
    :type fields: list

    :param timestamp_lowerlim: The minimum time as a **unix** timestamp to generate a random date. If not \
    passed in, the date and time a week ago will be used.
    :type timestamp_lowerlim: float or int

    :param timestamp_upperlim: The maxumum time as a **unix** timestamp to generate a random date. If not \
    passed in, the current date and time will be used.
    :type timestamp_upperlim: float or int

    :returns: timeseries predictions mock data
    :rtype: list of dicts

    """

    # data types for each field
    time_predict_types = table_types.TIMESERIES_PREDICTIONS_TYPES

    gaussian_values = float_ranges.TIMESERIES_PREDICTIONS_GAUSSIAN

    mock_time_predict_table = []

    # the user can pass in either `sensors` or `location_table` but not both
    if sensors and location_table:
        raise ValueError('Cannot pass in both sensors and location_table.')
    elif not sensors and not location_table:
        raise ValueError('Either sensors number or location_table needed')

    # if the location table is passed in, we need to know how many sensors there are
    if location_table:
        sensors = len(location_table)

    for sensor in range(sensors):

        for _ in range(datapts):

            time_predict_data: OrderedDict = OrderedDict()

            for field in fields:

                # if the user gives a list of fields this will validate that the fields
                # match the timeseries predictions table fields
                if field not in time_predict_types:
                    raise AttributeError(
                        'Field(s) given does not match timestamp predictions table fields')

                # special cases if a location table is passed in
                if location_table and (field in ['sensor', 'conveyor']):
                    #making sure that the location table has 'sensor', 'conveyor', and fields if these
                    #fields are passed as a parameter

                    if field not in location_table[sensor]:
                        raise AttributeError(
                            'Location table is missing fields')

                    location_table_row = location_table[sensor]
                    what_to_fill = location_table_row[field]
                    time_predict_data[field] = what_to_fill

                if time_predict_types[field] == 'unix':
                    time_predict_data['timestamp'] = generate_random_timestamp(
                        lowerlim=timestamp_lowerlim, upperlim=timestamp_upperlim)

                elif time_predict_types[field] == str:

                    if field == 'sensor':
                        if not location_table:
                            time_predict_data['sensor'] = str(sensor)
                    else:
                        time_predict_data[field] = generate_random_string()

                elif time_predict_types[field] == float:

                    mu = gaussian_values[field]['mean']
                    sigma = gaussian_values[field]['standard deviation']

                    # generating data from a normal distribution with the mean and standard devation
                    # taken from `float_ranges` constants
                    normal_float = round(random.gauss(mu, sigma), 4)

                    # assigning the normal float to the field we are on
                    time_predict_data[field] = normal_float

            mock_time_predict_table.append(time_predict_data)

    return mock_time_predict_table


def create_mock_acknowledgements_data(ackn_num: int = 1) -> List[OrderedDict]:
    """
    :ackn_num: the number of mock acknowledgements to create
    :type ackn_num: int

    :returns: acknowledgements mock data
    :rtype: list of dicts
    """
    # Create `ack_num` of alarms
    alarms = create_mock_alarms_data(alarms=ackn_num)

    acknowledgements = []

    alarms_schema = table_schemas.ALARMS

    for i in range(ackn_num):
        acknowledgement: OrderedDict = OrderedDict()

        ackn_schema = table_schemas.ACKNOWLEDGEMENTS_TABLE_DTYPES
        for ackn_field, ackn_field_type in ackn_schema.items():

            if ackn_field in alarms_schema:
                acknowledgement[ackn_field] = alarms[i][ackn_field]

            else:

                if ackn_field_type == str:
                    rand_str_len = generate_random_int()
                    rand_str = generate_random_string(length=rand_str_len)
                    acknowledgement[ackn_field] = rand_str

                elif ackn_field_type == int:
                    rand_int = generate_random_int()
                    acknowledgement[ackn_field] = rand_int

                elif ackn_field_type == 'unix':
                    acknowledgement[ackn_field] = alarms[i]['timestamp']

        # Check that all acknowledgement values are of the correct types
        acknowledgement_types = table_schemas.ACKNOWLEDGEMENTS_TABLE_DTYPES
        for field, field_value in acknowledgement.items():
            # Timestamp table fields should be strings for now
            if 'timestamp' in field:
                if not isinstance(field_value, str):
                    raise TypeError('table field values must be of the correct type')
            else:
                if not type(field_value) == acknowledgement_types[field]:
                    raise TypeError('table field values must be of the correct type')

        # Check that acknowledgement has correct number of fields
        if len(acknowledgement.values()) != len(acknowledgement_types.items()):
            raise Exception('table must have correct number of fields')

        acknowledgements.append(acknowledgement)

    return acknowledgements
