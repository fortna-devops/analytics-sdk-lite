#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Specifying data types of each field/column for each table.
TODO: use the constants module for this

.. sectionauthor:: Julia Varzari

"""

ALARM_TYPES = {
    'sensor': str,
    'conveyor': str,
    'equipment': str,
    'severity': int,
    'type': str,
    'key': str,
    'value': float,  # in reality, this is currently a str
    'criteria': 'stringdict',
    'timestamp': 'unix'
}

ALARM_KEY_VALUES = [
    'temperature',
    'rms_velocity_z',
    'rms_velocity_x',
    'temperature_residuals',
    'rms_velocity_z_residuals',
    'rms_velocity_x_residuals'
]

SENSOR_TYPES = {
    'timestamp': 'unix',
    'gateway': str,
    'port': str,
    'sensor': str,  # from location table
    'rms_velocity_z': float,
    'temperature': float,
    'rms_velocity_x': float,
    'peak_acceleration_z': float,
    'peak_acceleration_x': float,
    'peak_frequency_z': float,
    'peak_frequency_x': float,
    'rms_acceleration_z': float,
    'rms_acceleration_x': float,
    'kurtosis_z': float,
    'kurtosis_x': float,
    'crest_acceleration_z': float,
    'crest_acceleration_x': float,
    'peak_velocity_z': float,
    'peak_velocity_x': float,
    'hf_rms_acceleration_z': float,
    'hf_rms_acceleration_x': float,
}

STATS_TYPES = {
    'sensor': str,
    'temperature_mean': float,
    'temperature_perc95': float,
    'rms_velocity_z_mean': float,
    'rms_velocity_z_perc95': float,
    'hf_rms_acceleration_x_mean': float,
    'hf_rms_acceleration_x_perc95': float,
    'hf_rms_acceleration_z_mean': float,
    'hf_rms_acceleration_z_perc95': float,
    'rms_velocity_x_mean': float,
    'rms_velocity_x_perc95': float
}

LOCATION_TYPES = {
    'sensor': str,
    'description': str,
    'manufacturer': str,
    'conveyor': str,
    'location': str,
    'equipment': str,
    'm_sensor': str,
    'standards_score': int,
    'temp_score': int,
    'vvrms_score': int,
    'thresholds': 'stringdict',  # `{}` hardcoded from location_81.csv
    'facility_organization': str,
    'residual_thresholds': 'stringdict',  # `{}` hardcoded from location_81.csv
    'alarms_confidence': 'stringdict'  # `{}` hardcoded from location_81.csv
}

TIMESERIES_PREDICTIONS_TYPES = {
    'timestamp': 'unix',
    'rms_velocity_x': float,
    'rms_velocity_x_residuals': float,
    'rms_velocity_z': float,
    'rms_velocity_z_residuals': float,
    'temperature': float,
    'temperature_residuals': float,
    'sensor': str,
    'conveyor': str,
    'equipment_type': str
}
