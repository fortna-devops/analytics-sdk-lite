#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Specifying the ranges that float data types should be generated based on field. Fields are
from sensor table schema.

.. sectionauthor:: Julia Varzari

"""

SENSOR_RANDOM = {
    'rms_velocity_z': {
        'lowerlim': 0.004,
        'upperlim': 0.0111},
    'temperature': {
        'lowerlim': 62.0,
        'upperlim': 80.5},
    'rms_velocity_x': {
        'lowerlim': 0.004,
        'upperlim': 0.0111},
    'peak_acceleration_z': {
        'lowerlim': 0.03,
        'upperlim': 0.045},
    'peak_acceleration_x': {
        'lowerlim': 0.03,
        'upperlim': 0.045},
    'peak_frequency_z': {
        'lowerlim': 9.7,
        'upperlim': 26.8},
    'peak_frequency_x': {
        'lowerlim': 9.7,
        'upperlim': 26.8},
    'rms_acceleration_z': {
        'lowerlim': 0.006,
        'upperlim': 0.009},
    'rms_acceleration_x': {
        'lowerlim': 0.006,
        'upperlim': 0.009},
    'kurtosis_z': {
        'lowerlim': 2.8,
        'upperlim': 3.08},
    'kurtosis_x': {
        'lowerlim': 2.8,
        'upperlim': 3.08},
    'crest_acceleration_z': {
        'lowerlim': 3.3,
        'upperlim': 4.2},
    'crest_acceleration_x': {
        'lowerlim': 3.3,
        'upperlim': 4.2},
    'peak_velocity_z': {
        'lowerlim': 0.0058,
        'upperlim': 0.0158},
    'peak_velocity_x': {
        'lowerlim': 0.0058,
        'upperlim': 0.0158},
    'hf_rms_acceleration_z': {
        'lowerlim': 0.0009,
        'upperlim': 0.01},
    'hf_rms_acceleration_x': {
        'lowerlim': 0.0009,
        'upperlim': 0.01},
}


SENSOR_GAUSSIAN = {
    'rms_velocity_z': {
        'mean': 0.0060,
        'standard deviation': 0.0012},
    'temperature': {
        'mean': 69.233,
        'standard deviation': 5.5562},
    'rms_velocity_x': {
        'mean': 0.0064,
        'standard deviation': 0.0020},
    'peak_acceleration_z': {
        'mean': 0.0377,
        'standard deviation': 0.0023},
    'peak_acceleration_x': {
        'mean': 0.0395,
        'standard deviation': 0.0032},
    'peak_frequency_z': {
        'mean': 12.89,
        'standard deviation': 5.7657},
    'peak_frequency_x': {
        'mean': 11.91,
        'standard deviation': 5.3341},
    'rms_acceleration_z': {
        'mean': 0.0069,
        'standard deviation': 0.0006},
    'rms_acceleration_x': {
        'mean': 0.0074,
        'standard deviation': 0.0007},
    'kurtosis_z': {
        'mean': 2.9819,
        'standard deviation': 0.0597},
    'kurtosis_x': {
        'mean': 2.9743,
        'standard deviation': 0.0725},
    'crest_acceleration_z': {
        'mean': 3.8392,
        'standard deviation': 0.2266},
    'crest_acceleration_x': {
        'mean': 3.8873,
        'standard deviation': 0.2430},
    'peak_velocity_z': {
        'mean': 0.0085,
        'standard deviation': 0.0017},
    'peak_velocity_x': {
        'mean': 0.0091,
        'standard deviation': 0.0028},
    'hf_rms_acceleration_z': {
        'mean': 0.0094,
        'standard deviation': 0.0005},
    'hf_rms_acceleration_x': {
        'mean': 0.0098,
        'standard deviation': 0.0004},
}


ALARM_KEY_VALUES_GAUSSIAN = {
    'temperature': {
        'mean': 155.1979,
        'standard deviation': 4.0570},
    'rms_velocity_z': {
        'mean': 0.2081,
        'standard deviation': 0.0190},
    'rms_velocity_x': {
        'mean': 0.2121,
        'standard deviation': 0.0240},
    'temperature_residuals': {
        'mean': 6.6907,
        'standard deviation': 8.4894},
    'rms_velocity_z_residuals': {
        'mean': -0.0065,
        'standard deviation': 0.0153},
    'rms_velocity_x_residuals': {
        'mean': -0.0042,
        'standard deviation': 0.0023}
}


STATS_GAUSSIAN = {
    'temperature_mean': {
        'mean': 83.9572,
        'standard deviation': 30.3177},
    'temperature_perc95': {
        'mean': 86.1984,
        'standard deviation': 32.1232},
    'rms_velocity_z_mean': {
        'mean': 0.1036,
        'standard deviation': 0.6632},
    'rms_velocity_z_perc95': {
        'mean': 0.1105,
        'standard deviation': 0.6631},
    'hf_rms_acceleration_x_mean': {
        'mean': 0.8440,
        'standard deviation': 6.8966},
    'hf_rms_acceleration_x_perc95': {
        'mean': 0.8548,
        'standard deviation': 6.8956},
    'hf_rms_acceleration_z_mean': {
        'mean': 0.9203,
        'standard deviation': 6.8915},
    'hf_rms_acceleration_z_perc95': {
        'mean': 0.9344,
        'standard deviation': 6.8903},
    'rms_velocity_x_mean': {
        'mean': 0.1305,
        'standard deviation': 0.6861},
    'rms_velocity_x_perc95': {
        'mean': 0.1386,
        'standard deviation': 0.6857}
}

TIMESERIES_PREDICTIONS_GAUSSIAN = {
    'rms_velocity_x': {
        'mean': 0.0353,
        'standard deviation': 0.0391},
    'rms_velocity_x_residuals': {
        'mean': 0.0014,
        'standard deviation': 0.0109},
    'rms_velocity_z': {
        'mean': 0.0326,
        'standard deviation': 0.0408},
    'rms_velocity_z_residuals': {
        'mean': 0.0019,
        'standard deviation': 0.0156},
    'temperature': {
        'mean': 88.5550,
        'standard deviation': 11.7032},
    'temperature_residuals': {
        'mean': 0.4124,
        'standard deviation': 7.9646},
}
