# Mock Data
----------------------------

#### Background

Functions for creating mock data for each of the tables (alarms, sensor, plc, location, stats, and timeseries_predictions).

Fields/columns needed can be specified.

For tables that use information from a location table, a location table can be passed in.


----------------------------
#### Methodology

Each function has similar but differenent sets of paramaters based on how the table should look like. The paramaters specify needed data, how much of it, and what it should look like. With this, a table is generated.

Most data is generated randomly. Sensors are unique unless specified otherwise.

###### Location table

With the specified number of sensors(default is 10) and a list of fields(default is all), a mock location table is created. Strings and integers are randomly generated. Thresholds and confidence is constant.

###### Alarms table

With the specified number of alarms per sensor(default is 5), list of fields(default is all), the option of passing a location table to match values, and the lower and upper limits in order to generate a timestamp(default is timestamps within the last week), a mock alarms data table is created. Data is randomly generated.

###### Sensor table

With either a specified number of sensors or a location table, type of distribution(default is random), number of data points(default is 5), list of fields needed(default is all), and the lower and upper limits in order to generate a timestamp(default is timestamps within the last week), a mock sensor data table is created. Data is generated based on what kind of distribution is specified. For example, gaussian will generate a normal distribution and random will generate random data.

###### Stats table

With either a specified number of sensors or a location table and list of fields needed(default is all), a mock stats data table is created. Values are generated based on a normal distribution.

###### Timeseries Predictions table

With either a specified number of sensors or a location table, number of data points(default is 5), list of fields needed(default is all), and the lowerlimit and upperlimit in order to generate a timestamp(default is timestamps within the last week), a mock timeseries predictions data table is created. Values are generated based on a normal distribution.

----------------------------
#### Configurable Parameters

###### from `mock_data.py` file

1. **thresholds**: thresholds for temperature, hf_rms_acceleration_x, hf_rms_acceleration_z, rms_velocity_z, and rms_velocity_x

2. **residual_thresholds**: rms_velocity_x_residuals, rms_velocity_z_residuals, and temperature_residuals specifications

3. **alarms_confidence**: rms_velocity_x, rms_velocity_z, and temperature confidence

###### from `table_types.py` file

The data type that will be returned for each field.

```python
ALARM_TYPES = {
	'sensor' : str,
    'conveyor': str,
    'equipment' : str,
    'severity' : int,
    'type' : str,
    'key' : str,
    'value' : float,
    'criteria' : 'stringdict',
    'timestamp' : 'unix'
}
```

For example, values under `'sensor'` key will be returned as strings as specified in `ALARM_TYPES`.

###### from `float_ranges.py` file

When `float` is specified as the data type for any key in `table_types.py`, the ranges specified in `float_ranges.py` will be used to generate data.

Values for generating floats are located in dictionaries indicating what the value is.

If the float should be random, the value under the key `lowerlimit` is the minimum and the value under the key `upperlimit` is the maximum. A random number is generated within that range.

```python
'rms_velocity_z' : {
        'lowerlim' : 0.004,
        'upperlim' : 0.0111}
```

For example, a random number between 0.004 and 0.0111 will be generated for `'rms_velocity_z'`.


If the float should be generated according to a normal distribution, a value will be generated where the value under the key `'mean'` is the mean and the value under the key `'standard devation'` will be the standard deviation.

```python
'rms_velocity_z' : {
        'mean' : 0.0060, 
        'standard deviation' : 0.0012}
```

For example, a normally distributed data point where the mean is 0.0060 and the standard deviation is 0.0012 will be generated for `'rms_velocity_z'`.
