"""pint unit registry quantity class to handle conversions"""
# pylint: disable=invalid-name
from pint import UnitRegistry

ureg = UnitRegistry()
Q_ = ureg.Quantity
