#!/usr/bin/env python3
"""
module for sending emails and wrapping
around boto3 ses client functions
"""
# pylint: disable=logging-format-interpolation, logging-fstring-interpolation, too-few-public-methods
import logging
from typing import List, Optional

import boto3
from botocore.exceptions import ClientError


LOGGER = logging.getLogger()
LOGGER.setLevel(logging.INFO)


class EmailSender:
    """
    Class for sending emails via SES
    """

    def __init__(self, source='no-reply@mhsinsights.com', session=None) -> None:
        """initialize sesions for boto3 if required. set source address for all emails"""

        if session:
            self.email_client = session.client('ses')
        else:
            self.email_client = boto3.client('ses', region_name="us-east-1")
        self.source_address = source

    def _send_one_email(self, body: str, subject: str, recipient: str) -> Optional[str]:
        """
        send a single email to one recipient address
        """
        try:
            response = self.email_client.send_email(
                Destination={
                    'ToAddresses': [recipient],
                },
                Message={
                    'Body': {
                        'Html': {
                            'Charset': 'utf-8',
                            'Data': body,
                        },
                    },
                    'Subject': {
                        'Charset': 'utf-8',
                        'Data': subject,
                    },
                },
                Source=self.source_address
            )
        except ClientError as client_error:
            LOGGER.error(client_error.response['Error']['Message'])
            return None
        else:
            LOGGER.info(f"Email sent! Message ID: {response['MessageId']}")
            return response['MessageId']

    def send_emails(self, body: str, subject: str, recipient_list: List[str]) -> bool:
        """wrapper around _send_one_email function to receive a list"""
        flag = True
        for address in recipient_list:
            response = self._send_one_email(body, subject, address)
            if not response:
                flag = False
        return flag
