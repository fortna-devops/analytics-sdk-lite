"""
initialization of toolbox
only used for setting up logger
in local environments w/ file writing
"""
# pylint: disable=invalid-name, wrong-import-position
# ignoring import position because it is auto-generated code so assuming its delicate
import logging
import os

logger = logging.getLogger('analytics_toolbox')
logger.setLevel(logging.DEBUG)
ch = logging.StreamHandler()
ch.setLevel(logging.INFO)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
logger.addHandler(ch)

# only use file handler if not in AWS environment & Debug is set
if "AWS_REGION" not in os.environ and os.environ.get('ANALYTICS_TOOLBOX_DEBUG'):
    fh = logging.FileHandler('analytics-toolbox.log')
    fh.setLevel(logging.DEBUG)
    fh.setFormatter(formatter)
    logger.addHandler(fh)

from ._version import get_versions
__version__ = get_versions()['version']
del get_versions
