#!/usr/bin/env python3
"""
Module for handling cache layer
Retrieval and sending of the key/values in cache for aws
.. sectionauthor:: Oluwatosin Sonuyi <oluwatosinsonuyi@mhsinc.net>
.. sectionauthor:: Julia Varzari

"""
# pylint: disable=logging-format-interpolation, line-too-long, logging-fstring-interpolation
# pylint: disable=keyword-arg-before-vararg, no-self-use
import os
import logging
import hashlib
import pickle
from typing import Union, Any, Callable, Optional

import elasticache_auto_discovery
from pymemcache.client.hash import HashClient
from pymemcache.exceptions import MemcacheError


from analytics_sdk_core.units_manager import Q_

LOGGER = logging.getLogger('analytics_toolbox.{}'.format(__name__))

CACHE_ENDPOINT = "mhspredict-cache.cvqk3d.cfg.use1.cache.amazonaws.com:11211"


class CacheClientEmulator():
    """
    Class to mock managing retrieval and setting of cache key/value
    pairs utilizing a local dictionary.
    """

    def __init__(self):
        """init the fake cache client"""
        self.cache_object = {}

    def get(self, cache_key):
        """return the object from the dictionary `cache` """
        return self.cache_object.get(cache_key)

    def set(self, cache_key, data, expire):
        """add an object to the diction `cache`"""
        self.cache_object[cache_key] = data

    def __repr__(self):
        """string representation of cache is just the dictionary itself"""
        return f"cache: {self.cache_object}"

CacheClients = Union[HashClient, CacheClientEmulator]


class CacheManager():
    """
    Class for managing retrieval and setting
    of cache key/value pairs utilizing amazon elasticache service.

    *for usage within lambdas make sure to configure the network
    Virtual Private Cloud settings.

    :example:
    ```
    >>> cm = CacheManager()
    >>> z = [1,34,5,6]
    >>> cm.place_in_cache("cachekey",pickle.dumps(z))
    >>> z_from_cache = cm.retriev_in_cache("cachekey")
    >>> z_from_cache = pickle.loads(z_from_cache)
    ```
    """

    def __init__(self, cache_client: Optional[CacheClients] = None, cache_expiration: Union[str, int] = 7200) -> None:
        """
        :param cache_client: if there is a cache client that has been initialized already
        :type cache_client: pymemcache.client.hash.HashClient

        :param cache_expiration: how long should the cached data last in seconds
        :type cache_expiration: int
        """

        if "AWS_REGION" not in os.environ and not cache_client:
            self.memcache_client = CacheClientEmulator()
        elif "AWS_REGION" in os.environ and not cache_client:
            nodes = elasticache_auto_discovery.discover(CACHE_ENDPOINT)
            nodes = map(lambda x: (x[1], int(x[2])), nodes)
            self.memcache_client = HashClient(nodes)
        elif cache_client:
            self.memcache_client = cache_client

        self.set_expiration(cache_expiration)

    def set_expiration(self, cache_expiration: Union[str, int]) -> None:
        """set the expiration time of the objects placed in the cache"""
        if isinstance(cache_expiration, str):
            self.cache_expiration = Q_(cache_expiration).to('seconds').magnitude
        elif isinstance(cache_expiration, int):
            self.cache_expiration = cache_expiration

    def _translate_key(self, cache_key: str) -> str:
        """

        Taking a cache key that likely has special characters and converting it into
        hash values as a hex string.

        :param cache_key: The key that needs to be translated
        :type cache_key: str

        :returns: the translation
        :rtype: str

        """

        hash_object = hashlib.md5(bytes(cache_key, 'utf-8'))

        hash_key = hash_object.hexdigest()

        LOGGER.debug(f'Transforming cache key {cache_key} to {hash_key}')

        return hash_key

    def retrieve_from_cache(self, cache_key: str) -> Any:
        """
        Retrieve a value from cache memory using a given key

        :param cache_key: The key corresponding to the value to be retrieved.
        :type cache_key: string

        :returns: the cached data

        """

        # translate the key into hex string
        hash_key = self._translate_key(cache_key)

        try:
            cached_data = self.memcache_client.get(hash_key)
        except MemcacheError as mem_cache_err:
            LOGGER.warning('Memcache Error: {}'.format(mem_cache_err))
            return None
        if cached_data is None:
            LOGGER.info(f'Cache Key {cache_key} had no value associated with it in cache, returning None')
            return None
        LOGGER.info(f'Successfully retrieved data from cache key: {cache_key}')
        depickled_data = pickle.loads(cached_data)
        return depickled_data

    def place_in_cache(self, cache_key: str, data2cache: Any) -> None:
        """
        Set a key/value pair to be placed inside memcache

        :param cache_key: the key to be placed in the cache
        :type cache_key: string

        :param data2cache: the data or value to be placed in the cache with cache_key
        :type data2cache : Anything
        """

        if not isinstance(cache_key, str):
            LOGGER.error(f'cache_key must be type string, not {type(cache_key)}\n exiting execution')
            return

        # translate the key into hex string
        hash_key = self._translate_key(cache_key)

        try:
            pickled_data = pickle.dumps(data2cache)
            self.memcache_client.set(
                hash_key,
                pickled_data,
                expire=self.cache_expiration
            )
            LOGGER.info(f'Successfully placed value for {cache_key} in cache')

        except MemcacheError as mem_cache_err:
            LOGGER.warning(f'Memcache Error: {mem_cache_err}')

    def cache_function_result(self, func: Callable[..., Any], cache_key: str, update_cache: bool = False, *f_args: Any, **f_kwargs: Any) -> Any:
        """
        Use the CacheManager as a wrapper around a whole function and cache the
        return value of the function

        :param func: the name of the function to execute
        :type func: function

        :param cache_key: string that represents the key to cache result of function with
        :type cache_key: string

        :param update_cache: whether or not to replace key with new function result
        :type update_cache: boolean

        """

        # if we are not updating cache then try and get the cached result
        if not update_cache:

            cached_res = self.retrieve_from_cache(cache_key)

            # check explicitly against None, because caching boolean values, or the
            # number 0 will ruin the conditional check
            if cached_res is not None:
                try:
                    return cached_res.decode('utf-8')
                except AttributeError:
                    return cached_res
                except UnicodeDecodeError:
                    return cached_res

        # if updating cache or cache key not present, then run function and cache result
        res = func(*f_args, **f_kwargs)
        # cache_key is used as the parameter so that the key does not hash twice because
        # cache_key is translated again in the function place_in_cache
        # if we passed a hashed key, the key would be hashed AGAIN

        # pickle the result so it is guaranteed to be a string
        self.place_in_cache(cache_key, data2cache=res)

        return res
