#!/usr/bin/env python3
"""
Module for specifying which colors correspond to which cards
..author: Oluwatosin Sonuyi <oluwatosinsonuyi@mhsinc.net>
"""
RED = 3
YELLOW = 2
GREEN = 1
GRAY = 999
