"""
constants used within system athena tables
to denote different data states corresponding
to bad data.
"""
NULL_STR = 'NULL'
INVALID_DATA_STR = 'Invalid Data'
POSITIVE_INVALID_FLOAT = 999.9
NEGATIVE_INVALID_FLOAT = -1.0
NEGATIVE_INVALID_INT = -1


INVALID_STRING = {'conveyor_status': [NULL_STR, INVALID_DATA_STR],
                  'alarms': [NULL_STR]}
INVALID_FLOAT = {'sensor': [POSITIVE_INVALID_FLOAT], 'alarms': [NEGATIVE_INVALID_FLOAT]}
INVALID_INT = {'mode_transition_detection': [NEGATIVE_INVALID_INT]}
