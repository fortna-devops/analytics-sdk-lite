"""key word strings used in athena boto3 query responses and status tracking"""
SUCCESS = 'SUCCEEDED'
FAILURE = 'FAILED'
RUNNING = 'RUNNING'
QUEUED = 'QUEUED'
CANCELLED = 'CANCELLED'
THROTTLED = 'we got throttled, carry on'

THROTTLING_EXCEPTIONS = ('ThrottlingException',
                         'ProvisionedThroughputExceededException',
                         'TooManyRequestsException')
