#!/usr/bin/env python3
"""
Module for specifying schemas for database tables
..author: Oluwatosin Sonuyi <oluwatosinsonuyi@mhsinc.net>
"""
from collections import OrderedDict

ALARMS = [
    'sensor',
    'conveyor',
    'equipment',
    'severity',
    'type',
    'key',
    'value',
    'criteria',
    'timestamp'
]

ALARMS_TABLE_DTYPES = OrderedDict({
    'sensor': str,
    'conveyor': str,
    'equipment': str,
    'severity': int,
    'type': str,
    'key': str,
    'value': float,
    'criteria': dict,
    'timestamp': int,
    'data_timestamp': int
})

SENSOR = [
    'timestamp',
    'gateway',
    'port',
    'sensor',
    'rms_velocity_z',
    'temperature',
    'rms_velocity_x',
    'peak_acceleration_z',
    'peak_acceleration_x',
    'peak_frequency_z',
    'peak_frequency_x',
    'rms_acceleration_z',
    'rms_acceleration_x',
    'kurtosis_z',
    'kurtosis_x',
    'crest_acceleration_z',
    'crest_acceleration_x',
    'peak_velocity_z',
    'peak_velocity_x',
    'hf_rms_acceleration_z',
    'hf_rms_acceleration_x',
]
PLC = [
    'timestamp',
    'gateway',
    'port',
    'sensor',
    'belt_speed',
    'vfd_current',
    'vfd_frequency',
    'vfd_voltage',
    'vfd_fault_code',
    'sorter_induct_rate',
    'sorter_reject_rate',
    'status',
    'torque',
    'hours_running',
    'power',
    'rpm',
    'horsepower_nameplate',
    'voltage_nameplate',
    'run_speed_nameplate',
    'current_nameplate',
    'num_phases_nameplate'
]

STATS_TABLE_DTYPES = OrderedDict({
    'sensor': str,
    'temperature_mean': float,
    'temperature_perc95': float,
    'rms_velocity_z_mean': float,
    'rms_velocity_z_perc95': float,
    'hf_rms_acceleration_x_mean': float,
    'hf_rms_acceleration_x_perc95': float,
    'hf_rms_acceleration_z_mean': float,
    'hf_rms_acceleration_z_perc95': float,
    'rms_velocity_x_mean': float,
    'rms_velocity_x_perc95': float
})

STATS = [
    'sensor',
    'temperature_mean',
    'temperature_perc95',
    'rms_velocity_z_mean',
    'rms_velocity_z_perc95',
    'hf_rms_acceleration_x_mean',
    'hf_rms_acceleration_x_perc95',
    'hf_rms_acceleration_z_mean',
    'hf_rms_acceleration_z_perc95',
    'rms_velocity_x_mean',
    'rms_velocity_x_perc95'
]

LOCATION = [
    'sensor',
    'description',
    'manufacturer',
    'conveyor',
    'location',
    'equipment',
    'm_sensor',
    'standards_score',
    'temp_score',
    'vvrms_score',
    'thresholds',
    'facility_organization',
    'residual_thresholds',
    'alarms_confidence'
]

LOCATION_TABLE_DTYPES = OrderedDict({
    'sensor': str,
    'description': str,
    'manufacturer': str,
    'conveyor': str,
    'location': str,
    'equipment': str,
    'm_sensor': str,
    'standards_score': int,
    'temp_score': int,
    'vvrms_score': int,
    'thresholds': dict,
    'facility_organization': str,
    'residual_thresholds': dict,
    'alarms_confidence': dict
})

TIMESERIES_PREDICTIONS = [
    'timestamp',
    'rms_velocity_x',
    'rms_velocity_x_residuals',
    'rms_velocity_z',
    'rms_velocity_z_residuals',
    'temperature',
    'temperature_residuals',
    'sensor',
    'conveyor',
    'equipment_type'
]

ACKNOWLEDGEMENTS = [
    'alarm_description',
    'sensor',
    'conveyor',
    'equipment',
    'work_order',
    'downtime_schedule',
    'severity',
    'type',
    'key',
    'value',
    'criteria',
    'acknowledgement_timestamp',
    'alarm_timestamp',
    'work_order_lead'
]

ACKNOWLEDGEMENTS_TABLE_DTYPES = OrderedDict({
    'alarm_description': str,
    'sensor': str,
    'conveyor': str,
    'equipment': str,
    'work_order': str,
    'downtime_schedule': str,
    'severity': int,
    'type': str,
    'key': str,
    'value': float,
    'criteria': str,
    'acknowledgement_timestamp': 'unix',
    'alarm_timestamp': 'unix',
    'work_order_lead': str
})
