#!/usr/bin/env python3
"""
Module for specifying which data tags reside in which table
..author: Oluwatosin Sonuyi <oluwatosinsonuyi@mhsinc.net>
"""

TIMESTAMP_COLS = ['read_time', 'datetime', 'timestamp']

DATA_MAP = {
    'X_CREST': 'crest_acceleration_x',
    'Z_CREST': 'crest_acceleration_z',
    'X_HF_RMS_G': 'hf_rms_acceleration_x',
    'Z_HF_RMS_G': 'hf_rms_acceleration_z',
    'X_KURT': 'kurtosis_x',
    'Z_KURT': 'kurtosis_z',
    'X_PEAK_G': 'peak_acceleration_x',
    'Z_PEAK_G': 'peak_acceleration_z',
    'X_PEAK_V_HZ': 'peak_frequency_x',
    'Z_PEAK_V_HZ': 'peak_frequency_z',
    'X_PEAK_V_IPS': 'peak_velocity_x',
    'Z_PEAK_V_IPS': 'peak_velocity_z',
    'X_RMS_G': 'rms_acceleration_x',
    'Z_RMS_G': 'rms_acceleration_z',
    'X_RMS_IPS': 'rms_velocity_x',
    'Z_RMS_IPS': 'rms_velocity_z',
    'X_RMS_V_IPS': 'rms_velocity_x',
    'Z_RMS_V_IPS': 'rms_velocity_z',
    'TEMP_F': 'temperature',
    'read_time': 'timestamp',
    'motor_current': 'vfd_current',
    'motor_freq': 'vfd_frequency',
    'motor_voltage': 'vfd_voltage',
    'vfd_fault_code': 'vfd_fault_code',
    'induct_rate': 'sorter_induct_rate',
    'reject_rate': 'sorter_reject_rate',
    'conveyor_status': 'status'
}


TABLE_CONTENTS = {
    'plc': [
        'belt_speed',
        'vfd_current',
        'vfd_frequency',
        'vfd_voltage',
        'vfd_fault_code',
        'sorter_induct_rate',
        'sorter_reject_rate',
        'torque',
        'hours_running',
        'power',
        'rpm',
        'status'
    ],
    'sensor': [
        'rms_velocity_z',
        'temperature',
        'rms_velocity_x',
        'peak_acceleration_z',
        'peak_acceleration_x',
        'peak_frequency_z',
        'peak_frequency_x',
        'rms_acceleration_z',
        'rms_acceleration_x',
        'kurtosis_z',
        'kurtosis_x',
        'crest_acceleration_z',
        'crest_acceleration_x',
        'peak_velocity_z',
        'peak_velocity_x',
        'hf_rms_acceleration_z',
        'hf_rms_acceleration_x'
    ]
}

TAG_LOCATIONS = {}
# build a dictionary where the keys are the tags and the value is table name
for t_name, taglist in TABLE_CONTENTS.items():
    for tag in taglist:
        TAG_LOCATIONS[tag] = t_name
