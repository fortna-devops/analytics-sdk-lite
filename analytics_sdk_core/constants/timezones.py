#!/usr/bin/env python3
"""
Module for specifying timezones
..author: Oluwatosin Sonuyi <oluwatosinsonuyi@mhsinc.net>
"""
DEFAULT_TZ_OFFSET = -4
