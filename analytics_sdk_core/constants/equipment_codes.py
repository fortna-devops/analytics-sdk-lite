#!/usr/bin/env python3
"""
Module for specifying which equipment codes correspond to which equipment
..author: Oluwatosin Sonuyi <oluwatosinsonuyi@mhsinc.net>
"""
BEARING = 'B'
GEARBOX = 'G'
MOTOR = 'M'
STRUCTURE = 'STRUCT'
