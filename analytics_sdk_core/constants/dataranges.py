"""
all of the valid numerical ranges
for the sensor data on banner qm42vt2
deprecated, this should be deleted since
it is redundant with constants/databaes module
"""
# pylint: disable=invalid-name
sensor_datarange = {
    'rms_velocity_x': [0, 4],  # in/sec
    'rms_velocity_z': [0, 4],  # in/sec
    'temperature': [0, 300],  # Fahrenheit
    'hf_rms_acceleration_x': [0, 20],  # G (m/sec^2)
    'hf_rms_acceleration_z': [0, 20],  # G (m/sec^2)
    'rms_acceleration_x': [0, 20],  # G (m/sec^2)
    'rms_acceleration_z': [0, 20]  # G (m/sec^2)
}
