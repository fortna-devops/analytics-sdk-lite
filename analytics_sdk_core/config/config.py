#!/usr/bin/env python3
"""
module for managing configurable parameters.
Class is responsible for the emails lists,
gathering bucket/site names per organization,
and configurable variables that get passed
into lambdas.

..author: Oluwatosin Sonuyi <oluwatosinsonuyi@mhsinc.net>
..author: Hasan Khan <hasan.khan@mhsglobal.com>
"""
# pylint: disable=line-too-long, logging-fstring-interpolation, invalid-name

from ast import literal_eval
from configparser import ConfigParser
import logging
from typing import Dict, Any

import botocore
import boto3
import s3fs

from analytics_sdk_core.analytic_exceptions.exceptions import WrongDataType
from analytics_sdk_core.cache.cache_functions import CacheManager

LOGGER = logging.getLogger()
LOGGER.setLevel(logging.INFO)

DEFAULT_ENV = 'DEFAULT'
MASTER_ENV = 'master'
DEV_ENV = 'develop'
PREPROD_ENV = 'preprod'

# lambda names as seen in AWS for dictionary keys
ADMIN_KEY = 'admin_api'
AMF_KEY = 'advanced-multi-factor'
ALERTS_KEY = 'alerts'
ALERT_UNSUB_KEY = 'alerts_unsubscribe'
ALL_API_KEY = 'api'
ANALYTICS_DEMO_KEY = 'analytics-demo'
AUTH_API_KEY = 'auth-api-gateway'
AUTH_PROXY_DEMO_KEY = 'auth-proxy-demo'
AUTH_PROXY_KEY = 'auth-proxy'
COMP_COLOUR_KEY = 'comp_colour_card'
CONVEY_STATUS_KEY = 'conveyor-status'
DATA_KEY = 'data'
DEMO_LAMB_KEY = 'demo-lambda'
DOWNSAMPLE_KEY = 'downsample'
HEARTBEAT_KEY = 'ses_heartbeat'
ISO_THRESH_KEY = 'iso-thresholds'
LAMB_DIAG_KEY = 'lambda-diagnostics'
LOAD_STATS_KEY = 'load_stats'
LOCATION_KEY = 'location'
MAIN_ANALYTICS_KEY = 'main_analytics'
ML_ALARMS_KEY = 'ml_alarms_lambda'
MTD_KEY = 'mode-transition-detection'
MODEL_RUN_KEY = 'model_runner'
PARTITIONS_KEY = 'partitions'
QA_CHECK_KEY = 'qa_data_check'
SEC_HEADER_KEY = 'security-headers'
SENSONIX_DATA_KEY = 'sensonix-data-engineering'
TESTING_KEY = 'testing_config'

# Tuples specify data type of each config var and its list items or dict values
VARS_BY_LAMBDA: Dict[str, dict] = {
        ADMIN_KEY: {},
        AMF_KEY: {},
        ALERTS_KEY: {
            'num_unsubscribe_days': int,
            'unsubscribe_url': str,
            'secret': str
        },
        ALERT_UNSUB_KEY: {
            'secret': str
        },
        ALL_API_KEY: {}, # Do not use `cognito_user_pool` as config var to avoid S3 calls by API.
        ANALYTICS_DEMO_KEY: {
            'STAGE': str,
            'data_range': str
        },
        AUTH_API_KEY: {},
        AUTH_PROXY_DEMO_KEY: {},
        AUTH_PROXY_KEY: {},
        COMP_COLOUR_KEY: {},
        CONVEY_STATUS_KEY: {},
        DATA_KEY: {},
        DEMO_LAMB_KEY: {},
        DOWNSAMPLE_KEY: {},
        HEARTBEAT_KEY: {
            'heartbeat_threshold': int,
            'email_threshold': int
        },
        ISO_THRESH_KEY: {},
        LAMB_DIAG_KEY: {},
        LOAD_STATS_KEY: {},
        LOCATION_KEY: {},
        MAIN_ANALYTICS_KEY: {'persistence_minutes': int},
        ML_ALARMS_KEY: {
            'percentile2compare': (list, float)
        },
        MTD_KEY: {},
        MODEL_RUN_KEY: {
            'ignored_sites': (list, str),
            'endpoint_name': str
        },
        PARTITIONS_KEY: {},
        QA_CHECK_KEY: {
            'low_missing': float,
            'high_missing': float,
            'flatline_threshold': float,
            'datarange_tolerance': float,
            'stale_hours': int
        },
        SEC_HEADER_KEY: {},
        SENSONIX_DATA_KEY: {},
        TESTING_KEY: {
            'valid_int': int,
            'valid_float': float,
            'valid_string': str,
            'valid_bool': bool,
            'string_list': (list, str),
            'int_list': (list, int),
            'float_list': (list, float),
            'dict_str': (dict, str),
            'dict_int': (dict, int),
            'dict_float': (dict, float)
        }
    }

# email list management (slightly different than lambda configs)
DEV_GROUP_KEY = 'dev_group'
CMMS_GROUP_KEY = 'cmms_group'
SUPPORT_GROUP_KEY = 'support_group'
OTHER_GROUP_KEY = 'other_group'
INTERNAL_DIAGNOSTICS_GROUP_KEY = 'internal_diagnostics_group'

EMAIL_VARS = {
    DEV_GROUP_KEY: (list, str),
    CMMS_GROUP_KEY: (list, str),
    SUPPORT_GROUP_KEY: (list, str),
    OTHER_GROUP_KEY: (list, str),
    INTERNAL_DIAGNOSTICS_GROUP_KEY: (list, str)
}


class ConfigManager():
    """
    email lists and bucket names per site.
    And, management of configurable variables.
    """

    s3_config_kwargs = {'retries': {'max_attempts': 0}}

    def __init__(self, env=DEFAULT_ENV, lambda_name=None, boto_session=None, update_cache=False, testing=False):
        """
        set up variables for sharing across class
        """
        if not boto_session:
            boto_session = boto3.Session()
        self._boto_session = boto_session
        self._update_cache = update_cache
        self._env = env
        self._config_bucket_name = 'mhs-analytics-config'
        self._emails_file_name = 'emails'
        self.lambda_name = lambda_name
        self._cache_manager = None

        self._s3fs = s3fs.S3FileSystem(session=self._boto_session, config_kwargs=self.s3_config_kwargs)

        if not testing and env:
            self._config = {}
            assert self.lambda_env in [DEFAULT_ENV, MASTER_ENV, DEV_ENV]

        if lambda_name:
            assert lambda_name in VARS_BY_LAMBDA.keys(), f"{lambda_name} not a valid option for lambda_name argument"

    @property
    def cache_manager(self):
        """Make cache manager shared across class"""
        self._cache_manager = self._cache_manager if self._cache_manager else CacheManager()
        return self._cache_manager

    @property
    def config(self):
        """retrieve full config dictionary, includes sites, config variables, and email information"""
        return self._config

    @property
    def lambda_env(self):
        """environment used to get correct configurable variables"""
        if not self._env:
            return DEFAULT_ENV
        elif self._env == PREPROD_ENV:
            return MASTER_ENV
        return self._env

    def get_config_vars(self, get_emails: bool = False) -> dict:
        """
        Get all config variables for the given `self.lambda_name` and `self._env`.

        :param get_emails: optional parameter for returning email recipients' addresses
        :type get_emails: bool

        :returns: dict with the name and value of the config variables
        :rtype: dict
        """

        file_name = ''
        if get_emails:
            file_name = f'{self._emails_file_name}.ini'
        else:
            file_name = f'{self.lambda_name}.ini'

        parser = ConfigParser()

        with self._s3fs.open(f'{self._config_bucket_name}/{file_name}') as cf:
            contents = cf.read().decode()
        parser.read_string(contents)

        # convert the values into the intended data types
        # check that the files have the expected sections and variables
        valid_vars: dict = {}
        if get_emails:
            valid_vars = self._validate_and_convert_vars(parser, is_emails=True)
            self._is_valid_file(parser, is_emails_file=True)
        else:
            valid_vars = self._validate_and_convert_vars(parser)
            self._is_valid_file(parser)

        return valid_vars

    def get_email_group(self, group: str) -> list:
        """
        Get a single configurable email lists
        """
        valid_groups = list(EMAIL_VARS.keys())
        assert group in valid_groups, f"expected {group} to be in {valid_groups}"
        emails = self.get_config_vars(get_emails=True)
        return emails[group]

    def get_all_email_groups(self) -> dict:
        """
        Get dictionary of all email lists
        """
        return self.get_config_vars(get_emails=True)

    def _is_valid_file(self, parser: ConfigParser, is_emails_file: bool = False) -> None:
        """
        check that the .ini file in s3 is a valid file.
        meaning it should have default, master and develop sections,
        and default should have the right number of variables.
        Raises KeyError if invalid file.

        :param parser: the ConfigParser object used to read the .ini file in s3
        :type parser: ConfigParser

        :param emails_file: whether the file in S3 is a file containing email addresses
        :type emails_file: bool

        :returns: Nothing
        :rtype: None
        """

        VARS: dict = {}

        if is_emails_file:
            VARS = EMAIL_VARS
        else:
            VARS = VARS_BY_LAMBDA[self.lambda_name]

        # Check if section specified by `self._env` exists in config file
        #  The [DEFAULT] section is not acknowledged.
        if self.lambda_env in [MASTER_ENV, DEV_ENV]:
            section_exists = parser.has_section(self.lambda_env)
            if not section_exists:
                raise KeyError(f'Config file does not have a [{self.lambda_env}] section')

        # if each var exists in the default section of the file
        for var in VARS.keys():
            if not parser.has_option(self.lambda_env, var) and not parser.has_option(DEFAULT_ENV, var):
                raise KeyError(f'{var} does not exist within either the {self.lambda_env} section or {DEFAULT_ENV} of this config file')

    def _validate_and_convert_vars(self, parser: ConfigParser, is_emails: bool = False) -> dict:
        """
        Check that the type of each config var is the correct expected type and
        convert them to the correct type using the ConfigParser

        :param parser: the ConfigParser object used to read the .ini file in s3
        :type parser: ConfigParser

        :param is_emails: bool for validating and converting email addresses returned from config file in S3
        :type is_emails: bool

        :returns: dict with the name and value of the config variables
        :rtype: dict
        """

        vars_with_valid_types = {}
        var_types: dict = {}

        if is_emails:
            var_types = EMAIL_VARS
        else:
            var_types = VARS_BY_LAMBDA[self.lambda_name]

        for var, var_type in var_types.items():

            # initialize value because of dynamic assignment
            var_value: Any

            try:
                if var_type == int:
                    var_value = parser.getint(self.lambda_env, var)
                elif var_type == float:
                    var_value = parser.getfloat(self.lambda_env, var)
                elif var_type == bool:
                    var_value = parser.getboolean(self.lambda_env, var)
                elif var_type == str:
                    var_value = parser.get(self.lambda_env, var)

                elif isinstance(var_type, tuple):
                    if var_type[0] == list:
                        list_items_type = var_type[1]
                        if list_items_type == str:
                            var_value = [str_var.strip() for str_var in parser.get(self.lambda_env, var).split(',')]
                        elif list_items_type == int:
                            var_value = [int(num) for num in parser.get(self.lambda_env, var).split(',')]
                        elif list_items_type == float:
                            var_value = [float(num) for num in parser.get(self.lambda_env, var).split(',')]

                    elif var_type[0] == dict:
                        dict_values_type = var_type[1]
                        var_value_pre_check = literal_eval(parser.get(self.lambda_env, var))
                        if dict_values_type == str:
                            var_value = {k: str(v) for k, v in var_value_pre_check.items()}
                        elif dict_values_type == int:
                            var_value = {k: int(v) for k, v in var_value_pre_check.items()}
                        elif dict_values_type == float:
                            var_value = {k: float(v) for k, v in var_value_pre_check.items()}
                else:
                    LOGGER.error(f'{var_type} is not an acceptable config var type')

                if is_emails:
                    for email in var_value:
                        if '@' not in email:
                            LOGGER.error(f'Error for {var_value} for {var} because it is not a valid email address.')
                            raise WrongDataType(f'{self.lambda_name} requires emails with @ symbol in them, passed {email} as an email value in the {self.lambda_env} section')

                vars_with_valid_types[var] = var_value

            except ValueError:
                raise WrongDataType(f'{self.lambda_name} lambda passed an invalid type for config variable "{var}", env {self._env}. expected {var_type}')

        return vars_with_valid_types

if __name__ == '__main__':
    cm = ConfigManager(env=DEFAULT_ENV, lambda_name=TESTING_KEY)
    print(cm.get_config_vars())
