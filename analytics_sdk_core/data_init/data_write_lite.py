#!/usr/bin/env python3
"""
Module for writing data after calculations in analytics.
..author: Oluwatosin Sonuyi <oluwatosinsonuyi@mhsinc.net>
..author: Coco Wu <Jiewu@mhsinc.net>
"""
# pylint: disable=line-too-long, too-many-locals, too-many-arguments, logging-fstring-interpolation, logging-format-interpolation
# pylint: disable=protected-access, invalid-name
import ast
from collections import OrderedDict
import copy
import csv
from datetime import datetime
import io
import json
import logging
import sys
import time
import warnings
from typing import List, Dict, Any, Tuple, Union

import boto3
import s3fs

from analytics_sdk_core.constants.databases import schemas
from analytics_sdk_core.analytic_exceptions import exceptions
from analytics_sdk_core.data_init import data_pull

LOGGER = logging.getLogger(f'analytics_toolbox.{__name__}')


class DataWriterLite():
    """
    General purpose data pulling from Athena with capabilities for
    retrying failed queries and error handling. Abstracts a few
    commonly used queries such as getting timeseries data and
    the sensor information. Also allows for querying tags across
    multiple tables without having to know where each tag is stored.
    """

    def __init__(self, s3_bucket_name: str, boto_session=None) -> None:
        """
        Need to have credentials stored in ~/.aws/credentials
        and region stored in ~/.aws/config
        """
        if boto_session is None:
            boto_session = boto3.Session()

        self._boto_session = boto_session
        self.s3_client = self._boto_session.client('s3')
        self.s3_bucket_name = s3_bucket_name
        self.s3fs = s3fs.S3FileSystem(session=self._boto_session)

    @property
    def s3_bucket_name(self) -> str:
        """get the current bucket the writer is configured to write to"""
        return self._s3_bucket_name

    @s3_bucket_name.setter
    def s3_bucket_name(self, s3name: str) -> None:
        """set the bucket for the writer to write to"""
        self._s3_bucket_name = s3name

    def write_alarm_data_wrapper(self, alarm_data_list: List[Dict[str, Any]]) -> None:
        """
        Write new record entries into the alarms table
        checks the data format for each field input and also
        creates a timestamp automatically if one is not included
        validates that all the required columns are included (except timestamp)


        :param alarm_data_list: list of alarm data dictionaries to write to database
        :type alarm_data_list: list

        :returns: nothing
        :rtype: None
        """

        columns_dtypes = schemas.ALARMS_TABLE_DTYPES
        table_columns = list(schemas.ALARMS_TABLE_DTYPES.keys())
        table_name = 'alarms'
        nested_partitions = set()

        alarms_data_to_write = []

        for alarm_row in alarm_data_list:
            # copy to avoid changing data during iteration
            alarm_row_copy = copy.deepcopy(alarm_row)

            columns_included = set(alarm_row_copy.keys())
            columns_needed = set(table_columns)

            columns_not_included = columns_needed - \
                columns_included.union(set(['timestamp']))
            if columns_not_included:
                raise exceptions.TableColumnMismatch(
                    f'Required columns not included for {table_name} table.\n'
                    f'Missing: {columns_not_included}')

            # add timestamp if not included
            if 'timestamp' not in alarm_row.keys():
                # ensure that datetime is written in UTC-0 TZ
                # epoch = datetime.utcfromtimestamp(0)
                datetime_obj = datetime.utcnow()
                # unix_ts_utc0 = (datetime_obj - epoch).total_seconds() * 1000.
                unix_ts_utc0 = int(round(time.time() * 1000))
                alarm_row_copy['timestamp'] = unix_ts_utc0
                datestr = datetime_obj.strftime('%Y-%m-%d')

            else:

                check_list = ['timestamp'] + ['data_timestamp']
                for item2check in check_list:
                    unix_ts_utc0 = alarm_row_copy[item2check]
                    try:
                        datetime_obj = datetime.utcfromtimestamp(
                            unix_ts_utc0 / 1000.)
                    except TypeError:

                        raise exceptions.TimestampFormatError(
                            f"Timestamp format should be unix timestamp*1000, received: {unix_ts_utc0} for {item2check}"
                        )

                datestr = datetime_obj.strftime('%Y-%m-%d')
            nested_partitions.add(('date', datestr))

            for k, v in alarm_row.items():
                if k not in table_columns:
                    # raise error for bad writing
                    raise exceptions.TableColumnMismatch(
                        '{k} does not exist in expected schema for {table_name}:/n{table_columns}'
                    )
                elif not isinstance(v, columns_dtypes[k]):
                    # raise error for wrong data type writing
                    raise exceptions.WrongDataType(
                        f'Expected {columns_dtypes[k]}, Received {type(v)} for column {k}'
                    )

                else:
                    # Handle special things such as dictionaries
                    if columns_dtypes[k] is dict:
                        dict_str = json.dumps(v)
                        alarm_row_copy[k] = dict_str

            alarms_data_to_write.append(alarm_row_copy)

            self.write_csv_data(
                path=table_name,
                file_name=datetime_obj.strftime('%Y-%m-%d_%H:%M:%S') + '.csv',
                field_names=table_columns,
                dict_rows=alarms_data_to_write,
                table_name=table_name,
                # order should not matter so casting from set to list is okay
                nested_partitions=list(nested_partitions)
            )

    def write_stats_data_wrapper(self, stats_data_list: List[Dict[str, Any]]) -> None:
        """
        Write new record entries into the stats table
        checks the data format for each field input and also
        validates that all the required columns are included

        :param stats_data_list: list of stats data dictionaries to write to database
        :type stats_data_list: list

        :returns: nothing
        :rtype: None
        """

        columns_dtypes = schemas.STATS_TABLE_DTYPES
        table_columns = list(schemas.STATS_TABLE_DTYPES.keys())
        table_name = 'stats'

        gateway_name = self.s3_bucket_name.split('mhspredict-site-')[1]

        path = f'{gateway_name}/{table_name}'

        for stats_row in stats_data_list:

            columns_included = set(stats_row.keys())
            columns_needed = set(table_columns)

            columns_not_included = columns_needed - columns_included
            if columns_not_included:
                raise exceptions.TableColumnMismatch(
                    f'Required columns not included for {table_name} table.\n'
                    f'Missing: {columns_not_included}'
                )

            for k, v in stats_row.items():
                if k not in table_columns:
                    # raise error for bad writing
                    raise exceptions.TableColumnMismatch(
                        f'{k} does not exist in expected schema for {table_name}:/n{table_columns}'
                    )
                elif not isinstance(v, columns_dtypes[k]):
                    # raise error for wrong data type writing
                    raise exceptions.WrongDataType(
                        f'Expected {columns_dtypes[k]}, Received {type(v)} for column {k}'
                    )

            self.write_csv_data(
                path=path,
                file_name=f"stats_table_{stats_row['sensor']}.csv",
                field_names=table_columns,
                dict_rows=[stats_row],
                table_name=table_name,
            )

    def write_csv_data(self, path: str, file_name: str, field_names: List[str], dict_rows: List[Dict[str, Any]], table_name: str = None, nested_partitions: List[Tuple[str, str]] = []) -> None:
        """
        Writes dict rows to s3 bucket as CSV file.
        Updates the partitions if table name and nested partitions is set.

        :param path: path to file inside s3 bucket
        :type path: str

        :param file_name: file name
        :type file_name: str

        :param field_names: list of field names
        :type field_names: list

        :param dict_rows: list of rows where each row is dict
        :type dict_rows: list

        :param table_name: table name
        :type table_name: str

        :param nested_partitions: nested partitions
        :type nested_partitions: list of 2-tuples

        :returns: nothing
        :rtype: None
        """
        supported_types = (int, float, str)
        add_nested_partitions = []
        full_path = 's3://%s/%s' % (self.s3_bucket_name, path)
        for nested_partition in nested_partitions:
            k, v = nested_partition
            full_path = '%s/%s=%s' % (full_path, k, v)
            if not self.s3fs.exists(full_path):
                add_nested_partitions.append(nested_partition)
        if table_name is not None and len(add_nested_partitions) > 0:
            partitions = ', '.join(['%s=\'%s\'' % (k, v)
                                    for k, v in add_nested_partitions])
            query = 'ALTER TABLE `%s`.`%s` ADD IF NOT EXISTS PARTITION (%s)' % (self.s3_bucket_name, table_name,
                                                                                partitions)
            engine = data_pull.DataQueryEngineLite(
                self.s3_bucket_name, boto_session=self._boto_session)
            engine.execute_generic_query(query, pull_result=False)
        full_path = '%s/%s' % (full_path, file_name)
        with io.StringIO() as out:
            writer = csv.DictWriter(
                out, fieldnames=field_names, delimiter=',', quotechar='`')
            writer.writeheader()
            columns_needed = set(field_names)
            for dict_row in dict_rows:
                columns_included = set(dict_row.keys())
                columns_not_included = columns_needed - columns_included
                if columns_not_included:
                    raise exceptions.TableColumnMismatch(
                        f'Required columns not included for {table_name} table being written to {full_path}.\n'
                        f'Missing columns: {columns_not_included}'
                    )
                for key, value in dict_row.items():
                    if not isinstance(value, supported_types):
                        raise ValueError('Unexpected value "%s" for key "%s". Supported value types: %s.' %
                                         (value, key, supported_types))
                writer.writerow(dict_row)
            with self.s3fs.open(full_path, 'wb') as f:
                f.write(out.getvalue().encode())

    def _writable_dict(self, dict2write: Union[dict, OrderedDict, str]) -> str:
        """
        Write a dictionary correctly with the parsing quote characters
        This writing is to be used for input into a csv where commas
        are special characters therefore dictionaries need special
        consideration when they are put into csvs

        :param dict2write: the dictionary of data to write
        :type dict2write: dict or OrderedDict or json loadable string

        :returns: A properly formatted string with special quotations around
        :rtype: string

        :raises: WrongDataType exception when the input cannot be cast as a dict
        """

        if isinstance(dict2write, OrderedDict):
            # ensure that dictionary is written with double quotes
            # so that it can be loaded with json.loads()

            dict_w_double_quotes = json.dumps(dict(dict2write))
            return f"`{dict_w_double_quotes}`"
        elif isinstance(dict2write, dict):
            dict_w_double_quotes = json.dumps(dict2write)
            return f"`{dict_w_double_quotes}`"
        elif isinstance(dict2write, str):
            try:
                json.loads(dict2write)
                return f'`{dict2write}`'
            except json.decoder.JSONDecodeError:
                raise exceptions.WrongDataType(f'Attempted to write {dict2write}'
                                               'As a dictionary but it is a non json loadable string.')

        else:
            raise exceptions.WrongDataType(f'Attempted to write {dict2write}'
                                           f'As a dictionary but it has type: {type(dict2write)}')

    def write_to_stats_table(self, sensor: str, new_data: Dict[str, Any], gateway_name: str) -> None:
        """
        Write to or update data within a specific stats table file.
        All of the table fields must be updated simulataneously

        :param sensor: the name of the sensor to update
        :type sensor: string

        :param new_data: the new data to update keys are column names, values are new data
        :type new_data: dictionary

        :param gateway_name: name of gateway is the s3 directory of the location file to write to
        :type gateway_name: string

        :returns: nothing
        :rtype: None
        """

        warning_message = f"{sys._getframe().f_code.co_name} is a deprecated function. Avoid usage."
        warnings.warn(warning_message, DeprecationWarning)

        s3_fpath = f"s3://{self.s3_bucket_name}/{gateway_name}/stats/stats_table_{sensor}.csv"

        # update values
        ordered_headers = schemas.STATS
        # if there is a column that is in new data but not ordered headers thats an error
        in_new_data_only = set(new_data.keys()) - set(ordered_headers)
        # if there is a column in ordered headers thats not in new data, just write the old value.
        in_headers_only = set(ordered_headers) - set(new_data.keys())

        if in_new_data_only:
            LOGGER.error(f'Attempting to write data that does not exist in stats table: {in_new_data_only}')
            return

        try:
            with self.s3fs.open(s3_fpath, 'rb') as fl:
                data = fl.read().decode("utf-8").split('\n')
                # when reading directly from stats table the quotechar is a single quote
                # compare this to reading directly from an athena query result of location table
                # in this case the quotechar is the double quote. This should not come into play
                # in the stats table because it has no jsons inside.
                reader = csv.DictReader(data, delimiter=',', quotechar="`")
                query_result_lines = [row for row in reader]
        except FileNotFoundError:
            LOGGER.warning(f'There is no stats table entry at: {s3_fpath}')
            LOGGER.debug(f'Creating a new stats table entry with data: {new_data}')
            query_result_lines = [OrderedDict()]
            # if any data is in_headers_only, that means it is missing
            # since this is for writing brand new sensor entry, there cannot be anything missing
            if in_headers_only:
                raise exceptions.TableColumnMismatch('Attempting to write an incomplete set of data for a sensor that does not exist.\n'
                                                     f'Missing the following columns: {in_headers_only}')

        updated_vals = []
        # Note, there should only be one line. stats table csv is per sensor
        for stats_ordered_dict in query_result_lines:
            # retain order of the column as input data not guaranteed
            # to be an ordered dictionary
            for col in ordered_headers:
                try:
                    new_val = str(new_data[col])
                    updated_vals.append(new_val)
                except KeyError:
                    if col in in_headers_only:
                        # use the original stats data to update
                        updated_vals.append(stats_ordered_dict[col])
                        continue

                    else:
                        LOGGER.error(
                            f'Attempted to write to a column not present in stats table schema\n{col}')

        # there is only one line because its location row
        headers = ','.join(list(ordered_headers))
        values = ','.join(list(updated_vals))

        data_to_write = f"{headers}\n{values}\n"

        self.s3_client.put_object(
            Bucket=self.s3_bucket_name,
            Body=data_to_write,
            Key=f"{gateway_name}/stats/stats_table_{sensor}.csv"
        )

        LOGGER.debug(f'Successfully Wrote Stats Table Data: {new_data}')

    def write_location_data(self, sensor: str, col: str, value: Any, gateway_name: str) -> None:
        """
        Write to or update data within a specific location table file.

        :param sensor: the name of the sensor to update
        :type sensor: string

        :param col: the column to update
        :type col: string

        :param value: the value to update the column with
        :type value: string

        :param gateway_name: name of gateway is the s3 directory of the location file to write to
        :type gateway_name: string

        :returns: nothing
        :rtype: None
        """

        s3_fpath = f"s3://{self.s3_bucket_name}/{gateway_name}/location/location_{sensor}.csv"

        # TODO: catch errors if entry puts in a sensor number that has no location entry
        with self.s3fs.open(s3_fpath, 'rb') as fl:
            data = fl.read().decode("utf-8").split('\n')
            # when reading directly from locations table the quotechar is a single quote
            # compare this to reading directly from an athena query result of location table
            # in which case the quotechar is the double quote
            reader = csv.DictReader(data, delimiter=',', quotechar="`")
            query_result_lines = [row for row in reader]

        # update column value
        updated_lines = []
        for location_ordered_dict in query_result_lines:
            for curr_col_name, curr_val in location_ordered_dict.items():
                # if the column that we are iterating to is the column that we would like to change
                # then change the value to the value passed into the function
                value_to_change_to = str(
                    value) if col == curr_col_name else str(curr_val)
                try:
                    # check to see if value is a dictionary and can be json loaded
                    # if it is a json then it needs to have special quotes around
                    # it so that commas do not get parses as separate columns within the csv
                    curr_val_as_dict = json.loads(str(curr_val))
                    if isinstance(curr_val_as_dict, dict):
                        LOGGER.debug(
                            "Found json value, adding single quotes to enable"
                            f"parsing after writing: {curr_val}"
                        )

                        new_value_as_dict = json.loads(value_to_change_to)
                        if not isinstance(new_value_as_dict, dict):
                            LOGGER.error(
                                "Attempting to change column that contains"
                                "json to a column without json!\n"
                                f"Original Data: {curr_val}\nNew Data You Attempted To Write: {value_to_change_to}"
                            )
                            raise exceptions.WrongDataType(
                                "Changing json field to a non json value!")
                        location_ordered_dict[curr_col_name] = self._writable_dict(
                            new_value_as_dict)
                        continue

                except (json.decoder.JSONDecodeError) as e:
                    if "property name enclosed in double quote" in str(e):
                        # ast.literal_eval will evalues json even with single quote properties
                        # json.dumps will ensure that it is a string with double quote properties
                        updated_quotes = json.dumps(
                            ast.literal_eval(value_to_change_to))
                        location_ordered_dict[curr_col_name] = self._writable_dict(
                            updated_quotes)
                        continue

                except (ValueError, TypeError) as e:
                    LOGGER.debug(
                        f"Exception {e} while trying to load value: {value}")

                location_ordered_dict[curr_col_name] = value_to_change_to
            updated_lines.append(copy.copy(location_ordered_dict))

        # there is only one line because its location row
        headers = ','.join(list(updated_lines[0].keys()))
        values = ','.join(list(updated_lines[0].values()))

        data_to_write = f"{headers}\n{values}\n"

        self.s3_client.put_object(
            Bucket=self.s3_bucket_name,
            Body=data_to_write,
            Key=f"{gateway_name}/location/location_{sensor}.csv"
        )
