#!/usr/bin/env python3
"""
Module for handling data prior to consumption in analytics.
Retrieval, formatting, and massaging data until it is in the most manageable format
possible. Without pandas dataframe or numpy.
..author: Oluwatosin Sonuyi <oluwatosinsonuyi@mhsinc.net>
"""
# pylint: disable=line-too-long, logging-format-interpolation, protected-access, logging-fstring-interpolation, too-many-locals, too-many-arguments
# pylint: disable=too-many-instance-attributes, invalid-name
import csv
from collections import OrderedDict
from datetime import datetime, timezone, timedelta
import inspect
import logging
import pickle
import sys
import time
from typing import Union, Optional, Tuple, List, Any

import boto3
from botocore.exceptions import ClientError
import s3fs

from analytics_sdk_core.constants.databases import athena_query
from analytics_sdk_core.constants.databases.invalid_entry import INVALID_STRING
from analytics_sdk_core.cache.cache_functions import CacheManager
from analytics_sdk_core.units_manager import Q_

LOGGER = logging.getLogger('analytics_toolbox.{}'.format(__name__))


class DataQueryEngineLite():
    """
    General purpose data pulling from Athena or direct S3 file reads
    with capabilities for retrying failed queries and error handling. Abstracts a few
    commonly used queries such as getting timeseries data and
    the sensor information. Also allows for querying tags across
    multiple tables without having to know where each tag is stored.
    """

    def __init__(
            self,
            s3_bucket_name=None,
            max_retries=2,
            throttle_delay_seconds=0.25,
            retry_sleep_time='5 seconds',
            boto_session=None
    ):
        """
        Need to have credentials stored in ~/.aws/credentials
        and region stored in ~/.aws/config
        otherwise pass in a boto session
        """
        if boto_session is None:
            boto_session = boto3.Session()

        self._boto_session = boto_session
        self.athena_client = self._boto_session.client('athena')
        self.glue_client = self._boto_session.client('glue')
        self.query_id = None
        self.s3_resource = self._boto_session.resource('s3')
        self.s3fs = s3fs.S3FileSystem(session=self._boto_session)
        self.max_retries = max_retries
        self.attempt_num = 0
        self.s3_bucket_name = s3_bucket_name
        self.retry_sleep_time = Q_(retry_sleep_time)
        self.throttle_delay_seconds = Q_(throttle_delay_seconds, 'seconds')
        self.quit_querying = False
        self._cache_manager = None


        # proactive protection against queries that return huge amounts of data
        # https://stackoverflow.com/questions/15063936/csv-error-field-larger-than-field-limit-131072
        while True:
            try:
                max_int = sys.maxsize
                csv.field_size_limit(max_int)
                break
            except OverflowError:
                max_int = int(max_int / 10)

    @property
    def cache_manager(self):
        """Make cache manager shared across class"""
        self._cache_manager = self._cache_manager if self._cache_manager else CacheManager()
        return self._cache_manager

    def _supersede_bucket(self, s3_bucket_name: Optional[str]) -> str:
        """
        Determine if class s3 bucket name needs to be overwritten with
        a method specific s3 bucket name.

        :param s3_bucket_name: new bucket name to use
        :type s3_bucket_name: str

        :returns: overwritten bucket name
        :rtype: str
        """

        if not s3_bucket_name and not self.s3_bucket_name:
            raise ValueError('An S3 bucket name is required within queries')

        if s3_bucket_name:
            return s3_bucket_name

        elif not s3_bucket_name:
            return self.s3_bucket_name

        assert False, 'S3 bucket name not specified and no ValueError raised'

    def _cache_key_construct(self) -> str:
        """
        calculate a key for the calling funciton data
        """
        # get the name of the calling function
        calling_func_name = sys._getframe().f_back.f_back.f_code.co_name
        # get the values of all the parameters passed into the calling function
        # but remove the self param since it includes object address which will
        # likely change between executions
        # AND remove cache_expiration since that should be able to change without
        # invalidating the data cached
        local_vars_dict = sys._getframe().f_back.f_back.f_locals
        local_vars_dict.pop('self')
        try:
            local_vars_dict.pop('cache_expiration')
        except KeyError:
            pass

        # get a list of the parameter names for the calling function
        list_of_params_of_calling_func = list(inspect.signature(getattr(self, calling_func_name)).parameters)

        # filter the local variables in the stack to include those passed into the calling function
        filtered_params = {k: v for k, v in local_vars_dict.items() if k in list_of_params_of_calling_func}

        cache_key = f"{calling_func_name} {filtered_params} {self.s3_bucket_name}"

        return cache_key

    def cache_retrieve(self, cache_expiration: Optional[Union[str, int]]) -> Optional[Any]:
        """
        retrieve data from cache
        uses sys._getframe() instead of inspect.currentframe() because of type checks with Nones
        """

        if cache_expiration:

            cache_key = self._cache_key_construct()
            cached_data = self.cache_manager.retrieve_from_cache(cache_key.lower())

            if cached_data:
                decoded_py_obj = pickle.loads(cached_data)
                return decoded_py_obj

        return None

    def cache_place(self, cache_expiration: Optional[Union[str, int]], data2cache: Any) -> None:
        """
        place an object within the cache using shared cache manager
        """
        if cache_expiration:
            self.cache_manager.set_expiration(cache_expiration)
            py_obj_dump = pickle.dumps(data2cache)
            cache_key = self._cache_key_construct()
            self.cache_manager.place_in_cache(cache_key.lower(), py_obj_dump)

    def _poll_status(self) -> Tuple[str, Optional[str]]:
        """
        Determine the execution status of the currently executing query

        :returns: indicates the state of the query and the failure message if there is one
        :rtype: tuple
        """
		# sleep before polling status to reduce load on athena
		# https://docs.aws.amazon.com/athena/latest/ug/service-limits.html

        time.sleep(self.throttle_delay_seconds.to('seconds').magnitude)
        try:
            query_status = self.athena_client.get_query_execution(QueryExecutionId=self.query_id)
            query_state = query_status['QueryExecution']['Status']['State']
            # get the failure reason
            if query_state == athena_query.FAILURE:
                return query_state, query_status['QueryExecution']['Status']['StateChangeReason']
        except ClientError as err:
            # check if being throttled, if so, return a bogus state
            err_code = err.response['Error']['Code']
            if err_code in athena_query.THROTTLING_EXCEPTIONS:
                query_state = athena_query.THROTTLED
            else:
                raise

        # if there is no query reason dont include
        return query_state, None

    def _start_query_execution(self, query_raw_str: str, s3_bucket_name: Optional[str] = None) -> Optional[dict]:
        """
        begins the execution of a raw query. stops attempt after number of
        max retries has been exceeded.
        """

        s3_bucket_name = self._supersede_bucket(s3_bucket_name)

        # put output in tmp folder of bucket name
        s3_output_dest = "s3://{s3bucketname}/tmp".format(
            s3bucketname=s3_bucket_name)

        self.attempt_num += 1

        # quit the engine if the attempt number is greater than the number of retries
        if self.attempt_num > self.max_retries:
            self.athena_client.stop_query_execution(QueryExecutionId=self.query_id)
            LOGGER.warning('Last Query Attempt for {id} Failed, Exiting Engine.'.format(id=self.query_id))
            self.quit_querying = True
            return None

        LOGGER.debug(f'Query: {query_raw_str}\nAttempt #{self.attempt_num}\nS3 Output: {s3_output_dest}\n')

        query_retries = 0
        query_response = None

        while not query_response:
            try:
                # catch queries that have database stated in the query
                if s3_bucket_name + "`.`" in query_raw_str:
                    query_response = self.athena_client.start_query_execution(
                        QueryString=query_raw_str,
                        ResultConfiguration={
                            'OutputLocation': s3_output_dest
                        }
                    )
                else:
                    query_response = self.athena_client.start_query_execution(
                        QueryString=query_raw_str,
                        QueryExecutionContext={
                            'Database': s3_bucket_name
                        },
                        ResultConfiguration={
                            'OutputLocation': s3_output_dest
                        }
                    )
            except ClientError as err:
                query_retries += 1
                err_code = err.response['Error']['Code']
                if err_code in athena_query.THROTTLING_EXCEPTIONS:
                    time.sleep(self.throttle_delay_seconds.to('seconds').magnitude)
                # raise any errors that are not throttling exceptions
                else:
                    raise
                # break out and raise exception if it happens too many times
                if query_retries > self.max_retries:
                    raise

        return query_response

    def execute_generic_query(self, raw_query_string: str, return_as_df: bool = False, s3_bucket_name: Optional[str] = None, cache_expiration: Union[int, str, None] = None, pull_result: bool = True) -> Optional[List[OrderedDict]]:
        """
        Query Data from Athena

        :param raw_query_string: the raw sql query to execute with the engine
        :raw_query_string type: string

        :param return_as_df: boolean value indicating if the results of
        the query should be returned as a dataframe
        :return_as_df type: boolean

        :param s3_bucket_name: name of s3 bucket, only needed if AthenaQueryEngine was not initialized with one
        :s3_bucket_name type: string

        TODO: allow for different units as input into cache_expiration parameter and convert them to seconds using pint.
        :param cache_expiration: optional parameter specifying how long to cache result for
        :cache_expiration type: int or str

        :param pull_result: optional parameter specifying whether or not to actually return the data
        :pull_result type: boolean


        :returns: data that was requested based on the query string
        """

        s3_bucket_name = self._supersede_bucket(s3_bucket_name)

        cached_data = self.cache_retrieve(cache_expiration)
        if cached_data:
            return cached_data

        self.attempt_num = 0

        query_response = self._start_query_execution(
            query_raw_str=raw_query_string,
            s3_bucket_name=s3_bucket_name)

        if not query_response:
            return None

        self.query_id = query_response['QueryExecutionId']

        LOGGER.debug('Query Execution ID: {}'.format(self.query_id))

        query_state, failure_rsn = self._poll_status()

        LOGGER.info('Running: {q}'.format(q=raw_query_string))
        # wait until query completes successfully
        while query_state != athena_query.SUCCESS:
            query_state, failure_rsn = self._poll_status()

            if query_state == athena_query.FAILURE:

                LOGGER.warning(f'Athena Query {self.query_id} failed: {failure_rsn}\
					           {self.attempt_num}/{self.max_retries}\
					           \nAttempting again')

                self.athena_client.stop_query_execution(QueryExecutionId=self.query_id)

                # sleep before retrying query
                time.sleep(self.retry_sleep_time.to('seconds').magnitude)

                self._start_query_execution(
                    query_raw_str=raw_query_string,
                    s3_bucket_name=s3_bucket_name)

            # stop the engine if the quit querying attribute has been set to True
            if self.quit_querying:
                return None

        # if pull_result is False do not read results from s3 and return early
        if not pull_result:
            return None

        # load data from athena s3 output location
        query_full_path = f"s3://{s3_bucket_name}/tmp/{self.query_id}.csv"
        with self.s3fs.open(query_full_path, 'rb') as fl:
            data = fl.read().decode("utf-8").split('\n')
			# this quote character is double quotes
			# because athena deposits data different than we write it
			# in our reads direct from our own csvs the quotechar is "`"
            reader = csv.DictReader(data, delimiter=',', quotechar='"')
            query_result_lines = [row for row in reader]

        # remove file & its metadata after its been read
        self.s3fs.rm(query_full_path)
        self.s3fs.rm("{}.metadata".format(query_full_path))

        LOGGER.debug(f'Query {self.query_id} returned {len(query_result_lines)} lines')

        if return_as_df:
            query_result = self._format_query_results_as_df(query_result_lines)
        else:
            query_result = query_result_lines

        self.cache_place(cache_expiration, query_result)

        return query_result

    def _construct_ts_query_string(self, conveyor: str, equip: str, taglist: List[str], timerange_tuple: tuple) -> None:
        """
        input is a list of tags to query and datetimes
        and conveyor and equipment
        returns a dictionary of queries,
        the key is the table name to execute the query on
        and the value is the query string to execute

        :param conveyor: the name of the conveyor for data you are looking for
        :conveyor type: string

        :param equip: the equipment type, 'B' for bearings, 'G' for gearbox 'M' for motor
        :equip type: string

        :param taglist: list of tags that you hope to pull
        :taglist type: list of strings

        :param timerange_tuple: two tuples that indicate timerange for data of interest
        :timerange_tuple type: 2-tuple of datetime objects


        :returns: dictionary of queries, keys are the table to execute query on, value is the raw query string
        :rtype: (dictionary,Pandas DataFrame)
        """

        raise NotImplementedError(
            f'{sys._getframe().f_code.co_name} is implemented in full version. Not sdk lite.')

    def is_table_partitioned(self, table_name: str, s3_bucket_name: Optional[str] = None) -> bool:
        """
        determine whether a particular table has partitions

        :param table_name: name of table of interest
        :table_name type: string

        :returns: whether or not the table is partitioned
        :rtype: boolean
        """

        s3_bucket_name = self._supersede_bucket(s3_bucket_name)

        pkeys = self.glue_client.get_table(
            DatabaseName=s3_bucket_name,
            Name=table_name)['Table']['PartitionKeys']

        return bool(pkeys)

    def execute_timeseries_data_query(self, taglist: List[str], conveyor: str, equip: str, timerange_tuple: tuple, return_as_df: bool = True, s3_bucket_name: Optional[str] = None, cache_expiration: Union[int, str, None] = None) -> None:
        """
        Takes a list of tags, a conveyor, and the equipment of interest
        and returns a dataframe with the data
        for the specified timerange

        if the equip type is all, the return type will be a dictionary, not a dataframe

        Returns a string of data lines or a pandas dataframe

        This function cannot be used without a dataframe so implementation is only
        in whole version
        """

        raise NotImplementedError(
            f'{sys._getframe().f_code.co_name} is implemented in full version. Not sdk lite.')

    def execute_location_query(self, sensor_name: str, gateway_name: str, s3_bucket_name: Optional[str] = None, return_as_df: bool = False, cache_expiration: Union[int, str, None] = None) -> List[OrderedDict]:
        """
        execute location query for a particular sensor
        returns dataframe for all fields within the location table for a particular sensor

        :param sensor_name: name of sensor to query location table for
        :sensor_name type: string of sensor name

        :returns: DataFrame withe one column for each entry
        :rtype: pandas DataFrame
        """

        s3_bucket_name = self._supersede_bucket(s3_bucket_name)

        cached_data = self.cache_retrieve(cache_expiration)
        if cached_data:
            return cached_data

        query_full_path = "s3://{bucket}/{gwn}/location/location_{sn}.csv".format(
            bucket=s3_bucket_name,
            gwn=gateway_name,
            sn=sensor_name
        )

        with self.s3fs.open(query_full_path, 'rb') as fl:
            data = fl.read().decode("utf-8").split('\n')
            reader = csv.DictReader(data, delimiter=',', quotechar='`')
            query_result_lines = [row for row in reader]

        if return_as_df:
            location_df = self._format_query_results_as_df(query_result_lines)
        else:
            location_df = query_result_lines

        self.cache_place(cache_expiration, location_df)

        return location_df

    def execute_stats_table_query(self, sensor_name: str, gateway_name: str, s3_bucket_name: Optional[str] = None, return_as_df: bool = False, cache_expiration: Union[int, str, None] = None) -> List[OrderedDict]:
        """
        execute stats table query for a particular sensor
        returns dataframe for all fields within the stats table for a particular sensor
        TODO: can this be easier to do without athena and just pull directly from S3

        :param sensor_name: name of sensor to query stats table for
        :sensor_name type: string of sensor name

        :returns: DataFrame withe one column for each entry
        :rtype: pandas DataFrame
        """

        s3_bucket_name = self._supersede_bucket(s3_bucket_name)

        cached_data = self.cache_retrieve(cache_expiration)
        if cached_data:
            return cached_data

        query_full_path = "s3://{bucket}/{gwn}/stats/stats_table_{snumber}.csv".format(
            bucket=s3_bucket_name,
            gwn=gateway_name,
            snumber=sensor_name
        )

        with self.s3fs.open(query_full_path, 'rb') as fl:
            data = fl.read().decode("utf-8").split('\n')
            reader = csv.DictReader(data, delimiter=',', quotechar='`')
            query_result_lines = [row for row in reader]

        if return_as_df:
            stats_df = self._format_query_results_as_df(query_result_lines)
        else:
            stats_df = query_result_lines

        self.cache_place(cache_expiration, stats_df)

        return stats_df

    def get_conveyors(self, s3_bucket_name: Optional[str] = None, cache_expiration: Union[int, str, None] = None, filter_na: Optional[bool] = True) -> List[OrderedDict]:
        """
        execute query for getting all the conveyors that exist for a particular site

        :param cache_expiration: amount of time the cache should exist for

        :returns: list of conveyors available at one site
        :rtype: list
        """
        invalid_entry: Tuple[str, ...]
        s3_bucket_name = self._supersede_bucket(s3_bucket_name)

        cached_data = self.cache_retrieve(cache_expiration)
        if cached_data:
            return cached_data

        location_table_name = 'location'
        flattened_list = []
        for x in list(INVALID_STRING.values()):
            flattened_list.extend(x)
        invalid_entry = tuple(set(flattened_list))
        if filter_na:
            find_conveyor_query = f"""SELECT DISTINCT conveyor FROM "{s3_bucket_name}".{location_table_name} WHERE conveyor not in {invalid_entry} """
        else:
            find_conveyor_query = f"""SELECT DISTINCT conveyor FROM "{s3_bucket_name}".{location_table_name} """

        conveyor_lines = self.execute_generic_query(raw_query_string=find_conveyor_query,
                                                    return_as_df=False,
                                                    # cache_expiration=cache_expiration,
                                                    s3_bucket_name=s3_bucket_name
                                                    )

        if not conveyor_lines:
            return []

        results = []
        for cl in conveyor_lines:
            tmp_list = list(cl.values())
            try:
                results.append(tmp_list[0])
            except IndexError:
                continue

        self.cache_place(cache_expiration, results)

        return results

    def _format_query_results_as_df(self, result_lines):
        """
        Take result lines from athena query and
        turn them into a pandas dataframe with numeric
        column datatypes where applicable
        """

        raise NotImplementedError(
            f'{sys._getframe().f_code.co_name} is implemented in full version. Not sdk lite.')

    def get_conveyor_sensors(self, conveyor: str, cache_expiration: Union[int, str, None] = None, s3_bucket_name: Optional[str] = None) -> Optional[List[OrderedDict]]:
        """
        get all of the sensors and information for sensors of a particular conveyor
        TODO: change to return all of the sensor columns

        :param conveyor: string of conveyor name to get sensors information for
        :conveyor type: string

        :param cache_expiration: optional parameter specifying how long to cache result for
        :cache_expiration type: int or str

        :returns: pandas dataframe of location fields for all the sensors of a particular conveyor
        :rtype: pandas DataFrame
        """

        s3_bucket_name = self._supersede_bucket(s3_bucket_name)

        cached_data = self.cache_retrieve(cache_expiration)
        if cached_data:
            return cached_data

        location_table_name = 'location'

        find_sensor_query = """SELECT sensor,conveyor,equipment,location FROM "{bucket}".{ltablename} where sensor!= 'sensor' and conveyor='{c}'""".format(
            bucket=s3_bucket_name,
            ltablename=location_table_name,
            c=conveyor
        )

        sensor_lines = self.execute_generic_query(
            raw_query_string=find_sensor_query,
            return_as_df=False,
            # cache_expiration=cache_expiration
        )

        self.cache_place(cache_expiration, sensor_lines)

        return sensor_lines

    def get_recent_alarms(self, conveyor: Optional[str] = None, severity: Optional[int] = None, alarm_type: Optional[Union[str, list]] = None, start_datetime: Optional[datetime] = None, end_datetime: Optional[datetime] = None, limit: Optional[int] = None, s3_bucket_name: Optional[str] = None) -> Optional[List[OrderedDict]]:
        """
        Query for gathering alarms from database with no repeats.

        :param conveyor: the conveyor from which to gather alarms, defaults to None, returning all conveyors
        :type conveyor: str

        :param severity: the severity limit of the alarms to return, defaults to None, returning all severities
        :type severity: int

        :param alarm_type: filter only on this type of alarm, defaults to None, returning all types of alarms
        :type alarm_type: str

        :parm start_datetime: start datetime
        :type start_datetime: datetime

        :parm end_datetime: end datetime
        :type end_datetime: datetime

        :param limit: the limit on the number of alarm objects to return, defaults to None, returning as many alarms that meet filters
        :type limit: int

        :param s3_bucket_name: name of the s3 bucket to query data from (will overwrite object instance's s3_bucket_name if provided)
        :type s3_bucket_name: str

        :returns: alarm objects that meet the criteria outlined by parameters
        :rtype: list of OrderedDict()
        """

        s3_bucket_name = self._supersede_bucket(s3_bucket_name)

        if not end_datetime:
            end_datetime = datetime.utcnow()

        if not start_datetime:
            start_datetime = end_datetime - timedelta(days=1)

        assert end_datetime >= start_datetime, \
            f"end_datetime: {end_datetime} less than start_datetime: {start_datetime}"

        query = """
        SELECT
            sensor,
            key,
            MAX_BY(type, data_timestamp) AS type,
            MAX_BY(conveyor, data_timestamp) AS conveyor,
            MAX_BY(equipment, data_timestamp) AS equipment,
            MAX_BY(severity, data_timestamp) AS severity,
            MAX_BY(value, data_timestamp) AS value,
            MAX_BY(criteria, data_timestamp) AS criteria,
            MAX(data_timestamp) AS data_timestamp,
            MAX(timestamp) AS timestamp

        FROM
            "%(bucket_name)s"."%(table_name)s"
        WHERE
        %(date_query)s
        %(timestamp_query)s
        %(conveyor_query)s
        %(severity_query)s
        %(type_query)s
        GROUP BY
            sensor, key
        ORDER BY
            timestamp
        DESC
        %(limit_query)s
        """
        date_query = 'date BETWEEN date \'%s\' AND date \'%s\'' % (start_datetime.strftime('%Y-%m-%d'),
                                                                   end_datetime.strftime('%Y-%m-%d'))
        timestamp_query = 'AND timestamp BETWEEN timestamp \'%s\' AND timestamp \'%s\'' % (
            start_datetime.strftime('%Y-%m-%d %H:%M:%S'), end_datetime.strftime('%Y-%m-%d %H:%M:%S')
        )
        conveyor_query = ''
        if conveyor is not None:
            conveyor_query = 'AND conveyor = \'%s\'' % conveyor
        severity_query = ''
        if severity is not None:
            severity_query = 'AND severity = %s' % severity
        limit_query = ''
        if limit is not None:
            limit_query = 'LIMIT %s' % limit
        type_query = ''
        if alarm_type is not None and isinstance(alarm_type, str):
            type_query = 'AND type = \'%s\'' % alarm_type
        elif alarm_type is not None and isinstance(alarm_type, list):
            alarm_type_list = set(alarm_type)
            type_query = 'AND type IN %s' % alarm_type_list
            type_query = type_query.replace('{', '(').replace('}', ')')
        query = query % {
            'bucket_name': s3_bucket_name,
            'table_name': 'alarms',
            'date_query': date_query,
            'timestamp_query': timestamp_query,
            'conveyor_query': conveyor_query,
            'severity_query': severity_query,
            'type_query': type_query,
            'limit_query': limit_query
        }
        return self.execute_generic_query(query, return_as_df=False)


def construct_relative_timestamp_tuple(time_back: str, tzoffset=0) -> Tuple[datetime, datetime]:
    """
    Construct a tuple of timestamps with one entry being the current time
    and the other one being the currenttime minus the time back entered

    time_back can be entered as a string with units attached
    '12 hours'
    '43 weeks'
    '17 seconds'
    '1.45 days'

    the unist_manager module will convert and calculate the appropriate timestamps
    """
    # use unit manager to convert units to days before passing into timedelta

    time_back_in_days = Q_(time_back).to('days').magnitude
    right_now = datetime.now(timezone(timedelta(hours=tzoffset)))

    before = (datetime.now(timezone(timedelta(hours=tzoffset))) - timedelta(days=time_back_in_days))

    return before, right_now
