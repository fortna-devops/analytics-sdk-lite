# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## [0.93.10] - 2020-02-19

### Fixes
- Attempt to fix s3 timeout error by updating requirements to use s3fs version `0.2.1`, previously version `0.1.6`

## [0.93.9] - 2020-02-03

### Changes
- Functionality for reading org-data and site buckets from S3 removed from `ConfigManager`.

## [0.93.8] - 2020-02-03

### Fixes
- `ConfigManager.get_config_vars()` updated to use s3fs instead of boto3 in attempts to resolve timeout issues while reading config vars from S3.

## [0.93.7] - 2019-12-27

### Changes
- `ConfigManager` updated to have 4 retries and 15 second connection timeout limit on boto3 s3 client

## [0.93.6] - 2019-12-16

### Adds
- `ConfigManager` updated to allow `preprod` environment option, remove `rds` environment option

## [0.93.5] - 2019-12-04

### Adds
- `ConfigManager` updated to allow `rds` environment option

## [0.93.4] - 2019-11-27

### Adds
- `ConfigManager` updated to include `persistence_minutes` variable for main analytics

## [0.93.3] - 2019-11-11

### Adds
- `ConfigManager` updated to include UPS buckets

## [0.93.2] - 2019-11-11

### Fixes
- `ConfigManager` bug with passing in boto3 session

## [0.93.1] - 2019-09-10

### Fixes
- `ConfigManager` bug for leading and trailing whitespaces in list of strings returned from S3

## [0.93.0] - 2019-09-05

### Changed
- `get_recent_alarms` in `DataQueryEngineLite` changed to take maximum result based on new database column `data_timestamp` 
- `get_recent_alarms` also now allows `type` parameter to be a `list` type so that one can query multiple alarm types with one function call.
- `constants.databases.schemas` for alarms table now includes `data_timestamp` column

### Added
- `get_config_vars`, `get_email_group`, and `get_all_email_groups` functions to `ConfigManager` to allow configurable variables and emails to be retrieved from S3.
- static type checking in `DataWriterLite`
- validation of schema in `data_write_lite.write_csv_data()` before writing to avoid corruption of data in Athena

## [0.92.8] - 2019-08-02

### Added
- `csv.field_size_limit` update on DataQueryEngineLite initialization to handle reading of large csvs returned from big queries.

## [0.92.7] - 2019-07-19

### Changed
- changed invalid data entry for rms_acceleration_x, rms_acceleration_z, rms_velocity_x, rms_velocity_z from `constants`

## [0.92.6] - 2019-07-03

### Added
- `EmailSender` optional init variable for boto session

### Fixes
- `ConfigManager` bug with accessing `config` attribute keys


## [0.92.5] - 2019-07-02

### Fixes
- `_cache_key_construct` method in `DataQueryEngineLite` included local variables that could change from call to call
- `get_conveyors` and `get_conveyor_sensors` both were caching data twice per call.

## [0.92.4] - 2019-07-02

### Fixes
- `execute_generic_query` method in `DataQueryEngineLite.get_conveyors` was missing `s3_bucket_name` parameter


## [0.92.3] - 2019-06-28

### Fixes
- `DataQueryEngineLite` `get_conveyors` method bug in query construction

## [0.92.2] - 2019-06-26

### Fixes
- `DataQueryEngineLite` was having cache key overlaps for methods


## [0.92.1] - 2019-06-24

### Changed
- updates email list in config manager

### Fixes
- `email_tools` module import was missing `__init__.py`


## [0.92.0] - 2019-06-20

### Added
- `ConfigManager` internal email list groupings management
- `EmailSender` class for abstracting boto3 ses client 
- `testing_utilities` create mock acknowledgements data function
- `constants` module schema for acknowledgements table

### Changed
- `testing_utilities` create mock alarms data includes parameter for number of sensors

### Fixes
- `ConfigManager` cache key not unique across environments for site bucket names

## [0.91.0] - 2019-06-06

### Changed
- Constants module organizational heirarchy



## [0.90.0] - 2019-05-24
### Added
- Automated versioning
- Typechecking for CacheManager and DataQueryEngineLite
- DataQueryEngineLite uses a shared CacheManager object
- CacheManager method to update expiration time: `set_expiration(cache_expiration)`

### Changed
- Scheme for creating cache keys within DataQueryEngineLite [minor change]


## [0.0.1] - 2015-10-06
This was never officially released, it was just ongoing initial development from
2018-07-24 until release 0.9.0
