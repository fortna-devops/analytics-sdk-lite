#!/usr/bin/env python3

from moto import mock_ses
import boto3
import random
from analytics_sdk_core.email_tools.email_tools import EmailSender
from jinja2 import Template

def test_send_unverified_email():
    """
    Moto does not throw the expected ClientError, boto3 does
    hence, this doesn't use moto
    """

    sender = EmailSender(source='no-reply@fake.com')
    recipient = ['fake.developer@email.com']

    with open('tests/test_emails/example_email_template.html', 'r') as f:
        template = Template(f.read())

    email_message = template.render(input_string="this email was made with a variable")
    result = sender.send_emails(email_message, "test email", recipient)

    assert not result, f"Expected an error but instead got a Message ID of {result}"

@mock_ses
def test_send_simple_email():
    sender = EmailSender(source='no-reply@fake.com')
    recipient = ['fake.developer@email.com']
    sender.email_client.verify_email_identity(EmailAddress=recipient[0])
    sender.email_client.verify_email_identity(EmailAddress="no-reply@fake.com")

    with open('tests/test_emails/example_email_template.html', 'r') as f:
        template = Template(f.read())

    email_message = template.render(input_string="this email was made with a variable")
    sender.send_emails(email_message, "test email", recipient)

    send_quota = sender.email_client.get_send_quota()
    sent_count = int(send_quota['SentLast24Hours'])
    assert sent_count == 1, f"Expected 1 sent email but instead sent {sent_count}"

@mock_ses
def test_more_than_one_recipient():
    sender = EmailSender(source='no-reply@fake.com')
    recipients = ['fake.developer@email.com','fake.manager@email.com']
    sender.email_client.verify_email_identity(EmailAddress=recipients[0])
    sender.email_client.verify_email_identity(EmailAddress=recipients[1])
    sender.email_client.verify_email_identity(EmailAddress="no-reply@fake.com")

    with open('tests/test_emails/example_email_template.html', 'r') as f:
        template = Template(f.read())

    email_message = template.render()
    sender.send_emails(email_message, "test email", recipients)

    send_quota = sender.email_client.get_send_quota()
    sent_count = int(send_quota['SentLast24Hours'])
    assert sent_count == 2, f"Expected 2 sent emails but instead sent {sent_count}"

@mock_ses
def test_random_number_of_emails():
    sender = EmailSender(source='no-reply@fake.com')
    sender.email_client.verify_email_identity(EmailAddress="no-reply@fake.com")

    num_emails = random.randint(2, 50)
    recipients = []
    for x in range(num_emails):
        address = f"email{x}@fake.org"
        sender.email_client.verify_email_identity(EmailAddress=address)
        recipients.append(address)

    recipients = tuple(recipients)
    with open('tests/test_emails/example_email_template.html', 'r') as f:
        template = Template(f.read())

    email_message = template.render()
    sender.send_emails(email_message, "test email", recipients)

    send_quota = sender.email_client.get_send_quota()
    sent_count = int(send_quota['SentLast24Hours'])
    assert sent_count == num_emails, f"Expected {num_emails} sent emails but instead sent {sent_count}"

@mock_ses
def test_email_without_template():
    sender = EmailSender(source='no-reply@fake.com')
    sender.email_client.verify_email_identity(EmailAddress="no-reply@fake.com")
    recipient = ['fake.developer@email.com']
    sender.email_client.verify_email_identity(EmailAddress=recipient[0])

    sender.send_emails("this email did not use a template", "test email", recipient)

    send_quota = sender.email_client.get_send_quota()
    sent_count = int(send_quota['SentLast24Hours'])
    assert sent_count == 1, f"Expected 1 sent email but instead sent {sent_count}"
