#!/usr/bin/env python3
"""
Test writing cvs data functionality from the Data Writer Lite class
.. sectionauthor:: Oluwatosin Sonuyi <oluwatosinsonuyi@mhsinc.net>

"""

import ast
import json
import pytest
import csv
from datetime import datetime, timezone, timedelta
from collections import OrderedDict
from dateutil import parser

from moto import mock_s3
import boto3

import s3fs

from analytics_sdk_core.analytic_exceptions import exceptions
from analytics_sdk_core.constants import timezones
from analytics_sdk_core.constants.card_colors import RED, YELLOW, GREEN
from analytics_sdk_core.data_init.data_pull import DataQueryEngineLite as DQE
from analytics_sdk_core.data_init.data_write_lite import DataWriterLite as DWL
from analytics_sdk_core.testing_utilities import mock_data

@mock_s3
def test_generic_csv_write_method():
    """
    test for generic csv data writing method
    """

    bname = 'mock-testing-bk'
    conn = boto3.resource('s3', region_name='us-east-1')
    conn.create_bucket(Bucket=bname)

    pathname = 'platontest'
    filename = 'test-file-1.csv'
    field_names = ['first', 'second']
    dictrow_data = [{
        'first': '1',
        'second': '2'
    }, {
        'first': '11-11',
        'second': '22-22'
    }]

    # session = boto3.Session(profile_name='mhs')
    engine = DWL(bname)
    engine.write_csv_data(
        path=pathname,
        file_name=filename,
        field_names=field_names,
        dict_rows=dictrow_data
    )

    query_full_path = "s3://{bucket}/{path}/{fname}".format(
        bucket=bname,
        path=pathname,
        fname=filename
    )

    fs = s3fs.S3FileSystem()
    with fs.open(query_full_path, 'rb') as fl:
        data = fl.read().decode("utf-8").split('\n')
        reader = csv.DictReader(data, delimiter=',', quotechar='`')
        query_result_lines = [row for row in reader]
    fs.rm(query_full_path)

    # check new column data is updated
    for idx, row in enumerate(query_result_lines):
        # assert field names are there for every row
        assert list(row.keys()) == field_names
        # assert data is correct
        assert row == OrderedDict(dictrow_data[idx])

@mock_s3
def test_generic_csv_write_method_with_dictionary_json_data():
    """
    test for generic csv data writing method with generic dictionary
    throws an error
    """

    bname = 'mock-testing-bk'
    conn = boto3.resource('s3', region_name='us-east-1')
    conn.create_bucket(Bucket=bname)

    pathname = 'platontest'
    filename = 'test-file-2.csv'
    field_names = ['first', 'second']
    dictrow_data = [{
        'first': {'somedata': 23},
        'second': {13: 1.33,
                   'key1': 'dsaf'}
    }, {
        'first': {'somedata': 44},
        'second': {13: 3,
                   'key1': 'eww'}
    }]

    # session = boto3.Session(profile_name='mhs')
    engine = DWL(bname)

    with pytest.raises(ValueError) as e:

        engine.write_csv_data(
            path=pathname,
            file_name=filename,
            field_names=field_names,
            dict_rows=dictrow_data
        )

    assert e

@mock_s3
def test_writing_data_with_missing_columns():
    """
    test that a TableColumnMismatch exception is raised when trying to write
    data that does not have all of the required columns to a csv
    """

    bname = 'mock-testing-bk'
    conn = boto3.resource('s3', region_name='us-east-1')
    conn.create_bucket(Bucket=bname)

    pathname = 'platontest'
    filename = 'test-file-3.csv'
    field_names = ['first', 'second', 'third']
    dictrow_data = [{
        'first': '1',
        'second': '2'
    }, {
        'first': '11-11',
        'second': '22-22'
    }]

    engine = DWL(bname)

    with pytest.raises(exceptions.TableColumnMismatch) as e:
        engine. write_csv_data(
            path = pathname,
            file_name = filename,
            field_names = field_names,
            dict_rows = dictrow_data
            )
    assert e

