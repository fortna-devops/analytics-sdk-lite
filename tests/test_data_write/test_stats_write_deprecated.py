#!/usr/bin/env python3
"""
Test deprecated writing stats functionality from the Data Writer Lite class
.. sectionauthor:: Oluwatosin Sonuyi <oluwatosinsonuyi@mhsinc.net>

"""

import ast
import json
import pytest
import csv
from datetime import datetime, timezone, timedelta
from collections import OrderedDict
from dateutil import parser

from moto import mock_s3
import boto3

import s3fs

from analytics_sdk_core.analytic_exceptions import exceptions
from analytics_sdk_core.constants import timezones
from analytics_sdk_core.constants.card_colors import RED, YELLOW, GREEN
from analytics_sdk_core.data_init.data_pull import DataQueryEngineLite as DQE
from analytics_sdk_core.data_init.data_write_lite import DataWriterLite as DWL
from analytics_sdk_core.testing_utilities import mock_data



@mock_s3
def test_data_write_stats_no_csv_entry_in_place_missing_columns():
    """
    tests that data can be written to stats table
    when the sensor csv does not yet exist but is 
    missing some columns
    This should raise an error as the stats table
    entry for a particular sensor does not yet exist
    so we need to write everything
    """
    bucket_name = 'mock-testing-bk'
    conn = boto3.resource('s3', region_name='us-east-1')
    conn.create_bucket(Bucket=bucket_name)

    snumber = '873'
    data2write = {'temperature_perc95': 121.0032,
                  'temperature_mean': 43.2323,
                  'hf_rms_acceleration_x_perc95': 323.343,
                  'sensor': snumber}

    # create s3 bucket with mock location file in it
    conn = boto3.resource('s3', region_name='us-east-1')

    conn.create_bucket(Bucket=bucket_name)
    s3_client = boto3.client('s3')

    # testing data writer functionality
    data_writer = DWL(s3_bucket_name=bucket_name)

    with pytest.raises(exceptions.TableColumnMismatch) as e:
        data_writer.write_to_stats_table(
            sensor=snumber,
            new_data=data2write,
            gateway_name='test'
        )


@mock_s3
def test_data_write_stats_no_csv_entry_in_place():
    """
    tests that data can be written to stats table
    when the sensor csv does not yet exist
    """
    bucket_name = 'mock-testing-bk'
    conn = boto3.resource('s3', region_name='us-east-1')
    conn.create_bucket(Bucket=bucket_name)

    snumber = '999'
    data2write = {'temperature_perc95': 121.0032,
                  'temperature_mean': 43.2323,
                  'rms_velocity_z_mean': 90.0,
                  'rms_velocity_z_perc95': 0.09,
                  'rms_velocity_x_mean': 232.329,
                  'rms_velocity_x_perc95': 4554.43,
                  'hf_rms_acceleration_z_mean': 23.11,
                  'hf_rms_acceleration_z_perc95': 423.642,
                  'hf_rms_acceleration_x_mean': 767.43,
                  'hf_rms_acceleration_x_perc95': 323.343,
                  'sensor': snumber}

    # create s3 bucket with mock location file in it
    conn = boto3.resource('s3', region_name='us-east-1')

    conn.create_bucket(Bucket=bucket_name)
    s3_client = boto3.client('s3')

    # testing data writer functionality
    data_writer = DWL(s3_bucket_name=bucket_name)

    data_writer.write_to_stats_table(
        sensor=snumber,
        new_data=data2write,
        gateway_name='test'
    )

    # read data from location table and see if it has been appropriately updated

    query_full_path = "s3://{bucket}/test/stats/stats_table_{sname}.csv".format(
        bucket=bucket_name,
        sname=snumber)
    fs = s3fs.S3FileSystem()
    with fs.open(query_full_path, 'rb') as fl:
        data = fl.read().decode("utf-8").split('\n')
        reader = csv.DictReader(data, delimiter=',', quotechar="`")
        query_result_lines = [row for row in reader]

    fs.rm(query_full_path)

    # check the data that has been written
    for k, v in query_result_lines[0].items():
        assert float(v) == float(data2write[k])


@mock_s3
def test_data_one_col_write_to_stats_table():
    """
    tests that one data can be written to stats table
    and other columns remain unchanged
    """
    bucket_name = 'mock-testing-bk'
    conn = boto3.resource('s3', region_name='us-east-1')
    conn.create_bucket(Bucket=bucket_name)

    snumber = '444'
    col2update = 'temperature_mean'
    val2update = 9999.999
    # TODO: generate data2store string from this dictionary
    original_data = {'temperature_perc95': 121.0032,
                     'temperature_mean': 43.2323,
                     'rms_velocity_x_mean': 11.22,
                     'rms_velocity_x_perc95': 22.33,
                     'rms_velocity_z_mean': 232.329,
                     'rms_velocity_z_perc95': 4554.43,
                     'hf_rms_acceleration_z_mean': 23.11,
                     'hf_rms_acceleration_z_perc95': 423.642,
                     'hf_rms_acceleration_x_mean': 767.43,
                     'hf_rms_acceleration_x_perc95': 323.343,
                     'sensor': snumber}

    # create s3 bucket with mock location file in it
    conn = boto3.resource('s3', region_name='us-east-1')

    conn.create_bucket(Bucket=bucket_name)
    s3_client = boto3.client('s3')
    data2store = 'sensor,temperature_mean,temperature_perc95,rms_velocity_x_mean,rms_velocity_x_perc95,rms_velocity_z_mean,rms_velocity_z_perc95,hf_rms_acceleration_z_mean,hf_rms_acceleration_z_perc95,hf_rms_acceleration_x_mean,hf_rms_acceleration_x_perc95\n444,43.2323,121.0032,11.22,22.33,232.329,4554.43,23.11,423.642,767.43,323.343\n'
    s3_client.put_object(Bucket=bucket_name, Body=data2store, Key='test/stats/stats_table_{}.csv'.format(snumber))

    # testing data writer functionality
    data_writer = DWL(s3_bucket_name=bucket_name)

    data_writer.write_to_stats_table(
        sensor=snumber,
        new_data={col2update: val2update},
        gateway_name='test'
    )

    # read data from location table and see if it has been appropriately updated

    query_full_path = "s3://{bucket}/test/stats/stats_table_{sname}.csv".format(
        bucket=bucket_name,
        sname=snumber)
    fs = s3fs.S3FileSystem()
    with fs.open(query_full_path, 'rb') as fl:
        data = fl.read().decode("utf-8").split('\n')
        reader = csv.DictReader(data, delimiter=',', quotechar="`")
        query_result_lines = [row for row in reader]

    # check new column data is updated
    assert float(query_result_lines[0][col2update]) == val2update
    # check unchanged column data is not new
    for k, v in query_result_lines[0].items():
        if not k == col2update:
            assert float(v) == float(original_data[k])

