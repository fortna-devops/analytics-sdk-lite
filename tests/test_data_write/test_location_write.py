#!/usr/bin/env python3
"""
Test writing location data functionality from the Data Writer Lite class
.. sectionauthor:: Oluwatosin Sonuyi <oluwatosinsonuyi@mhsinc.net>

"""

import ast
import json
import pytest
import csv
from datetime import datetime, timezone, timedelta
from collections import OrderedDict
from dateutil import parser

from moto import mock_s3
import boto3

import s3fs

from analytics_sdk_core.analytic_exceptions import exceptions
from analytics_sdk_core.constants import timezones
from analytics_sdk_core.constants.card_colors import RED, YELLOW, GREEN
from analytics_sdk_core.data_init.data_pull import DataQueryEngineLite as DQE
from analytics_sdk_core.data_init.data_write_lite import DataWriterLite as DWL
from analytics_sdk_core.testing_utilities import mock_data


@mock_s3
def test_data_json_write_to_location():
    """
    test that a field containing json can be updated
    """
    bucket_name = 'mock-testing-bk'
    conn = boto3.resource('s3', region_name='us-east-1')
    conn.create_bucket(Bucket=bucket_name)

    snumber = '444'
    new_col_val = {"rms_velocity_z": {"red": 999, "yellow": 35}, "temperature": {"red": 999, "yellow": 95}}
    col_to_update = 'thresholds'
    # create s3 bucket with mock location file in it
    conn = boto3.resource('s3', region_name='us-east-1')

    conn.create_bucket(Bucket=bucket_name)
    s3_client = boto3.client('s3')
    data2store = 'sensor,description,manufacturer,conveyor,location,equipment,m_sensor,standards_score,temp_score,vvrms_score,thresholds\n10,Recirc 1,banner,SR1-8,M,M,10,1,1,1,`{"temperature": {"red": 140, "orange": 122}, "rms_acceleration": {"red": 0.37, "orange": 0.18, "blue": 0.07}}`\n'
    s3_client.put_object(Bucket=bucket_name, Body=data2store, Key='test/location/location_{}.csv'.format(snumber))

    # testing data writer functionality
    data_writer = DWL(s3_bucket_name=bucket_name)

    data_writer.write_location_data(
        sensor=snumber,
        col=col_to_update,
        value=new_col_val,
        gateway_name='test'
    )

    # read data from location table and see if it has been appropriately updated

    query_full_path = "s3://{bucket}/test/location/location_{sname}.csv".format(
        bucket=bucket_name,
        sname=snumber)
    fs = s3fs.S3FileSystem()

    with fs.open(query_full_path, 'rb') as fl:
        data = fl.read().decode("utf-8").split('\n')
        reader = csv.DictReader(data, delimiter=',', quotechar='`')
        query_result_lines = [row for row in reader]

    assert col_to_update in query_result_lines[0].keys()
    assert json.loads(query_result_lines[0][col_to_update]) == new_col_val

    fs.rm(query_full_path)


@mock_s3
def test_data_json_write_to_location_throws_error():
    """
    test that a field containing json can be updated to something
    not containing json and an error is thrown
    """
    bucket_name = 'mock-testing-bk'
    conn = boto3.resource('s3', region_name='us-east-1')
    conn.create_bucket(Bucket=bucket_name)

    snumber = '444'
    new_col_val = [3432]
    col_to_update = 'thresholds'
    # create s3 bucket with mock location file in it
    conn = boto3.resource('s3', region_name='us-east-1')

    conn.create_bucket(Bucket=bucket_name)
    s3_client = boto3.client('s3')
    data2store = 'sensor,description,manufacturer,conveyor,location,equipment,m_sensor,standards_score,temp_score,vvrms_score,thresholds\n10,Recirc 1,banner,SR1-8,M,M,10,1,1,1,`{"temperature": {"red": 140, "orange": 122}, "rms_acceleration": {"red": 0.37, "orange": 0.18, "blue": 0.07}}`\n'
    s3_client.put_object(Bucket=bucket_name, Body=data2store, Key='test/location/location_{}.csv'.format(snumber))

    # testing data writer functionality
    data_writer = DWL(s3_bucket_name=bucket_name)

    with pytest.raises(exceptions.WrongDataType) as e:
        data_writer.write_location_data(
            sensor=snumber,
            col=col_to_update,
            value=new_col_val,
            gateway_name='test'
        )

    query_full_path = "s3://{bucket}/test/location/location_{sname}.csv".format(
        bucket=bucket_name,
        sname=snumber)
    # remove data
    fs = s3fs.S3FileSystem().rm(query_full_path)