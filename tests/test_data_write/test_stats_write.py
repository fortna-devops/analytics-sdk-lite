#!/usr/bin/env python3
"""
Test writing stats functionality from the Data Writer Lite class
.. sectionauthor:: Oluwatosin Sonuyi <oluwatosinsonuyi@mhsinc.net>

"""

import pytest
import csv
from collections import OrderedDict

from moto import mock_s3
import boto3
import s3fs

from analytics_sdk_core.analytic_exceptions import exceptions
from analytics_sdk_core.data_init.data_write_lite import DataWriterLite as DWL
from analytics_sdk_core.testing_utilities import mock_data


@mock_s3
def test_stats_wrapper_creates_new_file_when_it_did_not_prevoiusly_exist():
    """
    tests that a new file is created if it did not previously exist when
    using the stats wrapper function

    """
    loc_table = [OrderedDict([('sensor', '7')])]

    stats_data = mock_data.create_mock_stats_data(location_table=loc_table)

    bname = 'mhspredict-site-mock-testing-bk'
    conn = boto3.resource('s3', region_name='us-east-1')
    conn.create_bucket(Bucket=bname)
    engine = DWL(bname)

    path = 'mock-testing-bk/stats'
    fname = 'stats_table_{}.csv'.format(loc_table[0]['sensor'])

    engine.write_stats_data_wrapper(stats_data_list=stats_data)

    fs = s3fs.S3FileSystem()

    full_path = "s3://{bucket}/{path}/{fname}".format(
        bucket=bname,
        path=path,
        fname=fname
    )

    assert fs.exists(full_path)

@mock_s3
def test_data_matches_using_write_stats_data_wrapper_with_one_row():
    """
    test that values given matches what is stored in the s3 bucket when 
    using write_stats_data_wrapper function with one row (one stats entry)
    """

    stats_data = mock_data.create_mock_stats_data(sensors=1)

    bname = 'mhspredict-site-mock-testing-bk'
    conn = boto3.resource('s3', region_name='us-east-1')
    conn.create_bucket(Bucket=bname)
    engine = DWL(bname)

    engine.write_stats_data_wrapper(stats_data_list=stats_data)


    for row in stats_data:
        path = 'mock-testing-bk/stats'
        fname = 'stats_table_{}.csv'.format(row['sensor'])

        full_path = "s3://{bucket}/{path}/{fname}".format(
            bucket=bname,
            path=path,
            fname=fname
        )


        fs = s3fs.S3FileSystem()
        with fs.open(full_path, 'rb') as fl:
            data = fl.read().decode("utf-8").split('\n')
            reader = csv.DictReader(data, delimiter=',', quotechar='`')
            query_result_lines = [row for row in reader]


        for key,value in row.items():
            assert str(value) == query_result_lines[0][key]

        fs.rm(full_path)


@mock_s3
def test_data_matches_using_write_stats_data_wrapper_multiple_rows():
    """
    test that values given matches what is stored in the s3 bucket when 
    using write_stats_data_wrapper function with multiple rows (multiple
    stats entries)
    """

    stats_data = mock_data.create_mock_stats_data(sensors=5)

    bname = 'mhspredict-site-mock-testing-bk'
    conn = boto3.resource('s3', region_name='us-east-1')
    conn.create_bucket(Bucket=bname)
    engine = DWL(bname)

    engine.write_stats_data_wrapper(stats_data_list=stats_data)

    for row in stats_data:

        path = 'mock-testing-bk/stats'

        fname = 'stats_table_{}.csv'.format(row['sensor'])

        full_path = "s3://{bucket}/{path}/{fname}".format(
            bucket=bname,
            path=path,
            fname=fname
        )


        fs = s3fs.S3FileSystem()
        with fs.open(full_path, 'rb') as fl:
            data = fl.read().decode("utf-8").split('\n')
            reader = csv.DictReader(data, delimiter=',', quotechar='`')
            query_result_lines = [row for row in reader]


        for key,value in row.items():
            assert str(value) == query_result_lines[0][key]

        fs.rm(full_path)


@mock_s3
def test_write_stats_data_wrapper_missing_columns():
    """
    test that TableColumnMismatch is raised when a stats table with missing
    columns is passed in
    """
    bname = 'mhspredict-site-mock-testing-bk'
    conn = boto3.resource('s3', region_name='us-east-1')
    conn.create_bucket(Bucket=bname)
    engine = DWL(bname)

    missing_fields = ['sensor', 'temperature_mean', 'hf_rms_acceleration_x_mean']

    stats_data_missing_cols = mock_data.create_mock_stats_data(sensors=1, fields=missing_fields)

    with pytest.raises(exceptions.TableColumnMismatch) as missingColsError:
        engine.write_stats_data_wrapper(stats_data_list=stats_data_missing_cols)
    assert missingColsError


@mock_s3
def test_write_stats_data_wrapper_extra_columns():
    """
    test that TableColumnMismatch is raised when a stats table with extra
    column(s) passed in
    """

    bname = 'mhspredict-site-mock-testing-bk'
    conn = boto3.resource('s3', region_name='us-east-1')
    conn.create_bucket(Bucket=bname)
    engine = DWL(bname)

    stats_table_extra_column = [OrderedDict([('sensor', '0'), 
                                ('temperature_mean', 117.1956), 
                                ('temperature_perc95', 96.2579), 
                                ('rms_velocity_z_mean', 0.0), 
                                ('rms_velocity_z_perc95', 0.4172), 
                                ('hf_rms_acceleration_x_mean', 6.0888), 
                                ('hf_rms_acceleration_x_perc95', 0.0), 
                                ('hf_rms_acceleration_z_mean', 0.5895), 
                                ('hf_rms_acceleration_z_perc95', 4.5701), 
                                ('rms_velocity_x_mean', 0.0), 
                                ('rms_velocity_x_perc95', 0.0482),
                                ('extra_field' , 'should raise error')])]

    with pytest.raises(exceptions.TableColumnMismatch) as extraColsError:
        engine.write_stats_data_wrapper(stats_data_list=stats_table_extra_column)
    assert extraColsError


@mock_s3
def test_write_stats_data_wrapper_wrong_data_types():
    """
    test that WrongDataType exception is raised when a stats table with
    wrong data types is passed in
    """

    bname = 'mhspredict-site-mock-testing-bk'
    conn = boto3.resource('s3', region_name='us-east-1')
    conn.create_bucket(Bucket=bname)
    engine = DWL(bname)

    stats_table_wdt = [OrderedDict([('sensor', '0'), 
                                ('temperature_mean', 'string instead of float'), 
                                ('temperature_perc95', 96.2579), 
                                ('rms_velocity_z_mean', 0.0), 
                                ('rms_velocity_z_perc95', 0.4172), 
                                ('hf_rms_acceleration_x_mean', 6.0888), 
                                ('hf_rms_acceleration_x_perc95', 0.0), 
                                ('hf_rms_acceleration_z_mean', 'string instead of float'), 
                                ('hf_rms_acceleration_z_perc95', 4.5701), 
                                ('rms_velocity_x_mean', 0.0), 
                                ('rms_velocity_x_perc95', 0.0482)])]

    with pytest.raises(exceptions.WrongDataType) as wdt:
        engine.write_stats_data_wrapper(stats_data_list=stats_table_wdt)
    assert wdt


@mock_s3
def test_write_stats_data_wrapper_updates_existing_files():
    """
    test that if there is already an existing file with that sensor,
    that the data is updated when writing to the file again

    only one stats row is tested since every sensor is a separate file
    """

    bname = 'mhspredict-site-mock-testing-bk'
    conn = boto3.resource('s3', region_name='us-east-1')
    conn.create_bucket(Bucket=bname)
    engine = DWL(bname)

    old_data = [OrderedDict([('sensor', '34'), 
                ('temperature_mean', 117.1956), 
                ('temperature_perc95', 96.2579), 
                ('rms_velocity_z_mean', 0.0), 
                ('rms_velocity_z_perc95', 0.4172), 
                ('hf_rms_acceleration_x_mean', 6.0888), 
                ('hf_rms_acceleration_x_perc95', 0.0), 
                ('hf_rms_acceleration_z_mean', 0.5895), 
                ('hf_rms_acceleration_z_perc95', 4.5701), 
                ('rms_velocity_x_mean', 0.0), 
                ('rms_velocity_x_perc95', 0.0482)])]

    #the values from old data are changed
    new_data = [OrderedDict([('sensor', '34'), 
                ('temperature_mean', 187.19), 
                ('temperature_perc95', 46.9), 
                ('rms_velocity_z_mean', 1.0), 
                ('rms_velocity_z_perc95', 0.672), 
                ('hf_rms_acceleration_x_mean', 8.0888), 
                ('hf_rms_acceleration_x_perc95', 0.4), 
                ('hf_rms_acceleration_z_mean', 0.535), 
                ('hf_rms_acceleration_z_perc95', 4.3984701), 
                ('rms_velocity_x_mean', 0.2348), 
                ('rms_velocity_x_perc95', 0.082)])]

    #checking that the data really is different
    assert old_data != new_data

    #but the sensors should be the same
    assert old_data[0]['sensor'] == new_data[0]['sensor']

    #writing the old data to the stats table
    engine.write_stats_data_wrapper(stats_data_list=old_data)

    path = 'mock-testing-bk/stats'
    fname = 'stats_table_{}.csv'.format(old_data[0]['sensor'])

    full_path = "s3://{bucket}/{path}/{fname}".format(
        bucket=bname,
        path=path,
        fname=fname
    ) 

    #asserting that the old data given is what is really stored
    for row in old_data:

        fs = s3fs.S3FileSystem()
        with fs.open(full_path, 'rb') as fl:
            data = fl.read().decode("utf-8").split('\n')
            reader = csv.DictReader(data, delimiter=',', quotechar='`')
            query_result_lines = [row for row in reader]


        for key,value in row.items():
            assert str(value) == query_result_lines[0][key]

    #writing the new data to the stats table
    engine.write_stats_data_wrapper(stats_data_list=new_data)

    #asserting that the new data given is what is really stored
    for row in new_data:

        fs = s3fs.S3FileSystem()
        with fs.open(full_path, 'rb') as fl:
            data = fl.read().decode("utf-8").split('\n')
            reader = csv.DictReader(data, delimiter=',', quotechar='`')
            query_result_lines = [row for row in reader]


        for key,value in row.items():
            assert str(value) == query_result_lines[0][key]

        fs.rm(full_path)


