#!/usr/bin/env python3
"""
Test writing alarms functionality from the Data Writer Lite class
.. sectionauthor:: Oluwatosin Sonuyi <oluwatosinsonuyi@mhsinc.net>

"""

import ast
import json
import pytest
import csv
from datetime import datetime, timezone, timedelta
from collections import OrderedDict
from dateutil import parser

from moto import mock_s3
import boto3

import s3fs

from analytics_sdk_core.analytic_exceptions import exceptions
from analytics_sdk_core.constants import timezones
from analytics_sdk_core.constants.card_colors import RED, YELLOW, GREEN
from analytics_sdk_core.data_init.data_pull import DataQueryEngineLite as DQE
from analytics_sdk_core.data_init.data_write_lite import DataWriterLite as DWL
from analytics_sdk_core.testing_utilities import mock_data


# @mock_s3
@pytest.mark.skip("credentials and partitions not mockable")
def test_alarm_data_writing_wrapper():
    """
    test that alarms data that is written using the high level alarms writing api
    is consistent when it is retrieved using an athena query 

    cannot be mocked because when write_csv_data depends on athena for partitions
    """

    bucket_name = 'mhspredict-testing'
    engine = DWL(bucket_name)

    alarms_data = [
        # alarm row 1
        {
            'sensor': '1',
            'conveyor': '',
            'equipment': '',
            'key': 'rms_velocity_z',
            'value': 9.1,
            'severity': 1000,
            'type': 'test_type',
            'criteria': {'rms_velocity_x': [1.12, 2.66, 5.44, 1.22], 'dafkl':[23.1, 1.4, 1111.6]},
            'data_timestamp':1566997854967
        }
    ]

    engine.write_alarm_data_wrapper(alarms_data)

    path_to_dir = "s3://{bucket}/{path}/".format(
        bucket=bucket_name,
        path='alarms'
    )

    fs = s3fs.S3FileSystem()
    file = fs.listdir(path_to_dir)
    with fs.open(file[0], 'rb') as fl:
        data = fl.read().decode("utf-8").split('\n')
        reader = csv.DictReader(data, delimiter=',', quotechar='`')
        alarm_received = [row for row in reader]

    for alarm_written, alarm_received in zip(alarms_data, alarms_received):
        for k, v in alarm_written.items():
            # the type key is not included in query
            if k == 'type':
                continue
            # dicts need to be json loaded and get most recent alarms doesnt return type
            elif isinstance(v, dict):
                assert json.loads(alarm_received[k]) == alarm_written[k]
            else:
                assert str(alarm_received[k]) == str(alarm_written[k])

    fs.rm(full_path)


@mock_s3
def test_error_thrown_alarms_api_when_writing_wrong_timestamp():
    """
    test for generic csv data writing method with generic dictionary
    throws an error
    """

    bname = 'mock-testing-bk'
    conn = boto3.resource('s3', region_name='us-east-1')
    conn.create_bucket(Bucket=bname)

    dictrow_data = [{
        'timestamp': '2019-02-13 14:33.23',
        'sensor': '32',
        'conveyor': '',
        'equipment': '',
        'severity': 23,
        'type': '',
        'key': '',
        'value': '',
        'criteria': {},
        'data_timestamp': 1566997854967
    }]

    # session = boto3.Session(profile_name='mhs')
    engine = DWL(bname)

    with pytest.raises(exceptions.TimestampFormatError) as e:

        engine.write_alarm_data_wrapper(dictrow_data)


@mock_s3
def test_error_thrown_alarms_api_when_missing_columns():
    """
    test for generic csv data writing method with generic dictionary
    throws an error
    """

    bname = 'mock-testing-bk'
    conn = boto3.resource('s3', region_name='us-east-1')
    conn.create_bucket(Bucket=bname)

    dictrow_data = [{
        'type': '',
        'key': '',
        'criteria': {}
    }]

    # session = boto3.Session(profile_name='mhs')
    engine = DWL(bname)

    with pytest.raises(exceptions.TableColumnMismatch) as e:

        engine.write_alarm_data_wrapper(dictrow_data)
