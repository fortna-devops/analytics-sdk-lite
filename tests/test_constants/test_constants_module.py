#!/usr/bin/env python3
"""
Tests constants
.. sectionauthor:: Oluwatosin Sonuyi <oluwatosinsonuyi@mhsinc.net>
"""


import pytest
from analytics_sdk_core.constants import dataranges, table_schemas
from analytics_sdk_core.constants.databases import invalid_entry, schemas, table_tags, valid_ranges


def test_dataranges_retrieval():
    """
    test that dataranges are dictionaries
    """
    dataranges.sensor_datarange.values()
    dataranges.sensor_datarange.keys()


def test_sensor_dataranges_tags_are_valid():
    """
    test dataranges keys are present in the database schema
    soon to be deprecated with constants re-organization
    """

    sensor_table_tags = set(table_schemas.SENSOR)
    for tag in dataranges.sensor_datarange.keys():
        assert tag in sensor_table_tags, f"{tag} not present in any data tables."


def test_sensor_dataranges_tags_are_valid_database_schemas():
    """
    test dataranges keys are present in the database schema
    """

    sensor_table_tags = set(schemas.SENSOR)
    for tag in valid_ranges.SENSOR.keys():
        assert tag in sensor_table_tags, f"{tag} not present in any data tables."

def test_dataranges_retrieval_datbase_constants_sensor():
    """
    assert datatypes make sense in data ranges sensor table
    """
    for k,v in valid_ranges.SENSOR.items():
        assert isinstance(k,str)
        for datavalue in v:
            assert isinstance(datavalue,int)
        

def test_dataranges_invalid_type_datbase_constants():
    """
    assert datatypes make sense in invalid data constants
    """
    assert isinstance(invalid_entry.NULL_STR, str)
    assert isinstance(invalid_entry.INVALID_DATA_STR, str)
    assert isinstance(invalid_entry.POSITIVE_INVALID_FLOAT, float)
    assert isinstance(invalid_entry.NEGATIVE_INVALID_FLOAT, float)
    assert isinstance(invalid_entry.NEGATIVE_INVALID_INT, int)

    for key, values in invalid_entry.INVALID_STRING.items():
        assert isinstance(key,str)
        assert isinstance(values, list)
    for key, values in invalid_entry.INVALID_INT.items():
        assert isinstance(key,str)
        assert isinstance(values, list)
    for key, values in invalid_entry.INVALID_FLOAT.items():
        assert isinstance(key,str)
        assert isinstance(values, list)


def test_sensor_dataranges_tags_are_valid():
    """
    test dataranges keys are present in the database schema
    """

    sensor_table_tags = set(schemas.SENSOR)
    for tag in dataranges.sensor_datarange.keys():
        assert tag in sensor_table_tags, f"{tag} not present in any data tables."