#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Tests the configuration functions that handles
configuration variables for lambdas.

author: oluwatosinsonuyi@mhsinc.net
"""

import boto3
import configparser
from moto import mock_s3
import pytest
import s3fs

from analytics_sdk_core.analytic_exceptions.exceptions import WrongDataType
from analytics_sdk_core.config.config import ConfigManager, \
    MASTER_ENV, DEV_ENV, DEFAULT_ENV, PREPROD_ENV, VARS_BY_LAMBDA, \
    TESTING_KEY, ML_ALARMS_KEY, ALERTS_KEY, EMAIL_VARS, \
    DEV_GROUP_KEY, CMMS_GROUP_KEY, SUPPORT_GROUP_KEY, INTERNAL_DIAGNOSTICS_GROUP_KEY, OTHER_GROUP_KEY

BUCKET_NAME = 'mhs-analytics-config'

@pytest.fixture
def test_config_file():
    """
    mix of data types test files to give broad coverage
    lists of ints, list of floats, list of strings
    dicts with values as floats, dicts with values as strings,
    dict of values as ints,
    ints, floats, strings, bools
    """
    with open(f'tests/test_config/{TESTING_KEY}.ini') as f:
        z = f.read()
    return z


@pytest.fixture
def test_emails_config_file():
    """
    valid config file for email addresses
    """
    with open(f'tests/test_config/emails.ini') as f:
        z = f.read()
    return z


@pytest.fixture
def bad_emails_config_file():
    """
    emails config file containing an invalid email address
    """
    with open(f'tests/test_config/bad_emails.ini') as f:
        z = f.read()
    return z


@pytest.fixture
def bad_ml_config_file():
    """ml alarms lambda config missing variable"""
    with open(f'tests/test_config/{ML_ALARMS_KEY}.ini') as f:
        z = f.read()
    return z


@pytest.fixture
def bad_alerts_config_file():
    """alerts lambda config missing variable"""
    with open(f'tests/test_config/{ALERTS_KEY}.ini') as f:
        z = f.read()
    return z


def test_vars_manager():
    """
    test simple initialization
    """
    cvm = ConfigManager(env=DEV_ENV, lambda_name=TESTING_KEY, testing=True)
    assert cvm.lambda_env == DEV_ENV
    cvm = ConfigManager(env=None, lambda_name=TESTING_KEY, testing=True)
    assert cvm.lambda_env == DEFAULT_ENV
    cvm = ConfigManager(env=MASTER_ENV, lambda_name=TESTING_KEY, testing=True)
    assert cvm.lambda_env == MASTER_ENV
    cvm = ConfigManager(env=DEFAULT_ENV, lambda_name=TESTING_KEY, testing=True)
    assert cvm.lambda_env == DEFAULT_ENV
    cvm = ConfigManager(env=PREPROD_ENV, lambda_name=TESTING_KEY, testing=True)
    assert cvm.lambda_env == MASTER_ENV

def test_bad_lambda_name_initialization():
    """asssert that invalid lambda name argument raises an error"""
    with pytest.raises(AssertionError):
        cvm = ConfigManager(lambda_name="whatitdobaybee")

@mock_s3
def test_reading_default_env(test_config_file):
    """
    test that config manager can read files
    with the default environment variables set
    but missing from the actual passed in environment
    in this test, we pass Develop key, but variables
    exist within the default config section
    """
    conn = boto3.resource('s3', region_name='us-east-1')
    conn.create_bucket(Bucket=BUCKET_NAME)
    client = boto3.client('s3')
    client.put_object(
        Bucket=BUCKET_NAME,
        Body=test_config_file,
        Key=f'{TESTING_KEY}.ini')

    cvmanager = ConfigManager(env=DEV_ENV, lambda_name=TESTING_KEY, testing=True)
    vals = cvmanager.get_config_vars()
    assert vals, "expected values returned"
    keys_exp = set(VARS_BY_LAMBDA[TESTING_KEY].keys())
    keys_ret = set(vals.keys())
    keys_required_but_not_retrieved = keys_exp - keys_ret
    assert not keys_required_but_not_retrieved, f"missing the expected keys: {keys_required_but_not_retrieved}"

    # delete file so other tests dont see it
    B = conn.Bucket(BUCKET_NAME)
    B.objects.filter(Prefix="/").delete()


@mock_s3
def test_standard_data_type_read_correctly(test_config_file):
    """
    this test checks if standard data types like floats,
    ints, strings, and/or bool can be read correctly
    """
    conn = boto3.resource('s3', region_name='us-east-1')
    conn.create_bucket(Bucket=BUCKET_NAME)
    client = boto3.client('s3')
    client.put_object(
        Bucket=BUCKET_NAME,
        Body=test_config_file,
        Key=f'{TESTING_KEY}.ini')

    cvmanager = ConfigManager(env=DEV_ENV, lambda_name=TESTING_KEY, testing=True)
    vals = cvmanager.get_config_vars()
    assert isinstance(vals['valid_int'], int)
    assert isinstance(vals['valid_float'], float)
    assert isinstance(vals['valid_string'], str)
    assert isinstance(vals['valid_bool'], bool)

    # delete file so other tests dont see it
    B = conn.Bucket(BUCKET_NAME)
    B.objects.filter(Prefix="/").delete()


@mock_s3
def test_list_internal_types_read_correctly(test_config_file):
    """
    this tests if lists of ints, floats, and/or strings
    can be read correctly
    """
    conn = boto3.resource('s3', region_name='us-east-1')
    conn.create_bucket(Bucket=BUCKET_NAME)
    client = boto3.client('s3')
    client.put_object(
        Bucket=BUCKET_NAME,
        Body=test_config_file,
        Key=f'{TESTING_KEY}.ini')

    cvmanager = ConfigManager(env=DEV_ENV, lambda_name=TESTING_KEY, testing=True)
    vals = cvmanager.get_config_vars()
    assert isinstance(vals['string_list'], list)
    assert all([isinstance(x,str) for x in vals['string_list']]), f"expected all strings inside {vals['string_list']}"
    assert isinstance(vals['int_list'], list)
    assert all([isinstance(x,int) for x in vals['int_list']]), f"expected all ints inside {vals['int_list']}"
    assert isinstance(vals['float_list'], list)
    assert all([isinstance(x,float) for x in vals['float_list']]), f"expected all floats inside {vals['float_list']}"

    # delete file so other tests dont see it
    B = conn.Bucket(BUCKET_NAME)
    B.objects.filter(Prefix="/").delete()


@mock_s3
def test_dict_internal_types_read_correctly(test_config_file):
    """
    this tests if dicts with strings, ints, and/or floats
    can be read correctly
    """
    conn = boto3.resource('s3', region_name='us-east-1')
    conn.create_bucket(Bucket=BUCKET_NAME)
    client = boto3.client('s3')
    client.put_object(
        Bucket=BUCKET_NAME,
        Body=test_config_file,
        Key=f'{TESTING_KEY}.ini')

    cvmanager = ConfigManager(env=DEV_ENV, lambda_name=TESTING_KEY, testing=True)
    vals = cvmanager.get_config_vars()
    assert isinstance(vals['dict_str'], dict)
    assert all([isinstance(x,str) for x in vals['dict_str'].values()]), f"expected all strings inside {vals['dict_str']}"
    assert isinstance(vals['dict_int'], dict)
    assert all([isinstance(x,int) for x in vals['dict_int'].values()]), f"expected all ints inside {vals['dict_int']}"
    assert isinstance(vals['dict_float'], dict)
    assert all([isinstance(x,float) for x in vals['dict_float'].values()]), f"expected all floats inside {vals['dict_float']}"

    # delete file so other tests dont see it
    B = conn.Bucket(BUCKET_NAME)
    B.objects.filter(Prefix="/").delete()


@mock_s3
def test_no_file_exists_throws_error():
    """
    test that a non-existant file (skip the step of putting file in mocks3)
    raises an error
    """
    conn = boto3.resource('s3', region_name='us-east-1')
    conn.create_bucket(Bucket=BUCKET_NAME)
    client = boto3.client('s3')
    cvmanager = ConfigManager(env=DEV_ENV, lambda_name=TESTING_KEY, testing=True)
    with pytest.raises((FileNotFoundError, client.exceptions.NoSuchKey)):
        cvmanager.get_config_vars()


@mock_s3
def test_invalid_file_missing_var(bad_ml_config_file):
    """
    test that an ill formatted config
    missing an environment variable
    file spawns a key error
    """
    conn = boto3.resource('s3', region_name='us-east-1')
    conn.create_bucket(Bucket=BUCKET_NAME)
    client = boto3.client('s3')
    client.put_object(
        Bucket=BUCKET_NAME,
        Body=bad_ml_config_file,
        Key=f'{ML_ALARMS_KEY}.ini')
    cvmanager = ConfigManager(env=MASTER_ENV, lambda_name=ML_ALARMS_KEY, testing=True)
    with pytest.raises(configparser.NoOptionError):
    	cvmanager.get_config_vars()


@mock_s3
def test_invalid_data_type_in_config_file(bad_alerts_config_file):
    """
    test that wrong data type in file
    spawns an error
    """
    conn = boto3.resource('s3', region_name='us-east-1')
    conn.create_bucket(Bucket=BUCKET_NAME)
    client = boto3.client('s3')
    client.put_object(
        Bucket=BUCKET_NAME,
        Body=bad_alerts_config_file,
        Key=f'{ALERTS_KEY}.ini')
    cvmanager = ConfigManager(env=MASTER_ENV, lambda_name=ALERTS_KEY, testing=True)
    with pytest.raises(WrongDataType):
    	cvmanager.get_config_vars()

@mock_s3
def test_environment_section_missing_from_file(bad_alerts_config_file):
    """test that we detects missing sections if they are the section we are using"""
    conn = boto3.resource('s3', region_name='us-east-1')
    conn.create_bucket(Bucket=BUCKET_NAME)
    client = boto3.client('s3')
    # remove environment from string
    worse_file_str = bad_alerts_config_file.replace('\n[develop]','')
    client.put_object(
        Bucket=BUCKET_NAME,
        Body=worse_file_str,
        Key=f'{ALERTS_KEY}.ini')
    cvmanager = ConfigManager(env=DEV_ENV, lambda_name=ALERTS_KEY, testing=True)
    with pytest.raises(configparser.NoSectionError):
        cvmanager.get_config_vars()

@mock_s3
def test_environment_section_missing_from_file_but_not_used_is_fine(test_config_file):
    """
    test that if a section is missing but 
    it isnt the section we are using its fine
    """
    conn = boto3.resource('s3', region_name='us-east-1')
    conn.create_bucket(Bucket=BUCKET_NAME)
    client = boto3.client('s3')
    # remove environment from string
    worse_file_str = test_config_file.replace('\n[master]','')
    client.put_object(
        Bucket=BUCKET_NAME,
        Body=worse_file_str,
        Key=f'{TESTING_KEY}.ini')
    cvmanager = ConfigManager(env=DEV_ENV, lambda_name=TESTING_KEY, testing=True)
    cvmanager.get_config_vars()

@mock_s3
def test_emails_return_as_dict_strings(test_emails_config_file):
    """
    Test that the config vars emails return as a dict with str keys
    for each email group and str values for each email address
    """
    conn = boto3.resource('s3', region_name='us-east-1')
    conn.create_bucket(Bucket=BUCKET_NAME)
    client = boto3.client('s3')

    client.put_object(
        Bucket=BUCKET_NAME,
        Body=test_emails_config_file,
        Key='emails.ini')

    cvmanager = ConfigManager(env=DEV_ENV, lambda_name=TESTING_KEY, testing=True)
    emails = cvmanager.get_config_vars(get_emails=True)
    emails_through_other_method = cvmanager.get_all_email_groups()

    expected_emails = {
        DEV_GROUP_KEY: ['hello@gmail.com'],
        CMMS_GROUP_KEY: ['test@aol.com'],
        OTHER_GROUP_KEY: ['one@one.com', 'two@two.com'],
        SUPPORT_GROUP_KEY: ['other@other.com'],
        INTERNAL_DIAGNOSTICS_GROUP_KEY: ['internal_diagnostics_group@test.com', 'email@email.com', 'test@test.com']
    }

    assert emails.keys() == expected_emails.keys() == emails_through_other_method.keys()
    for k,v in emails.items():
        assert set(v) == set(expected_emails[k]) == set(emails_through_other_method[k])

@mock_s3
def test_email_groups_can_be_read_test_emails_config_file(test_emails_config_file):
    """
    Test that the config vars emails return the correct lis tof emails
    for each group selected
    """
    conn = boto3.resource('s3', region_name='us-east-1')
    conn.create_bucket(Bucket=BUCKET_NAME)
    client = boto3.client('s3')

    client.put_object(
        Bucket=BUCKET_NAME,
        Body=test_emails_config_file,
        Key='emails.ini')

    cvmanager = ConfigManager(env=DEV_ENV, lambda_name=TESTING_KEY, testing=True)

    expected_emails = {
        DEV_GROUP_KEY: ['hello@gmail.com'],
        CMMS_GROUP_KEY: ['test@aol.com'],
        OTHER_GROUP_KEY: ['one@one.com', 'two@two.com'],
        SUPPORT_GROUP_KEY: ['other@other.com'],
        INTERNAL_DIAGNOSTICS_GROUP_KEY: ['internal_diagnostics_group@test.com', 'email@email.com', 'test@test.com']
    }

    for groupkey in [OTHER_GROUP_KEY, DEV_GROUP_KEY, CMMS_GROUP_KEY, SUPPORT_GROUP_KEY, INTERNAL_DIAGNOSTICS_GROUP_KEY]:
        assert set(cvmanager.get_email_group(groupkey)) == set(expected_emails[groupkey])

@mock_s3
def test_email_invalid_email_group_raises_error(test_emails_config_file):
    """
    Test that the config vars get specific group emails raises 
    an error if a bad key is passed in
    """
    conn = boto3.resource('s3', region_name='us-east-1')
    conn.create_bucket(Bucket=BUCKET_NAME)
    client = boto3.client('s3')

    client.put_object(
        Bucket=BUCKET_NAME,
        Body=test_emails_config_file,
        Key='emails.ini')

    cvmanager = ConfigManager(env=DEV_ENV, lambda_name=TESTING_KEY, testing=True)

    with pytest.raises(AssertionError):
        cvmanager.get_email_group('badkey')

@mock_s3
def test_only_valid_email_addresses_return(bad_emails_config_file):
    """
    test that an invalid email address raises an error
    """
    conn = boto3.resource('s3', region_name='us-east-1')
    conn.create_bucket(Bucket=BUCKET_NAME)
    client = boto3.client('s3')

    client.put_object(
        Bucket=BUCKET_NAME,
        Body=bad_emails_config_file,
        Key='emails.ini')

    cm = ConfigManager(env=DEV_ENV, lambda_name=TESTING_KEY, testing=True)

    path = f's3://{cm._config_bucket_name}/{cm._emails_file_name}.ini'

    parser = configparser.ConfigParser()

    # get contents of s3 config file and pass them into the ConfigParser
    s3fs_reader = s3fs.S3FileSystem(session=boto3.Session())
    with s3fs_reader.open(path, 'rb') as fl:
        data = fl.read().decode(encoding='utf-8')
        parser.read_string(data)

    valid_emails_in_config_file = 2
    with pytest.raises(WrongDataType):
        emails_returned_from_file = [email for email in cm.get_email_group(INTERNAL_DIAGNOSTICS_GROUP_KEY)]

@mock_s3
def test_valid_data_type_reading_file_with_lists():
    """
    test that config manager can read files
    list data that is well formatted as expected
    """

@mock_s3
def test_valid_data_type_reading_file_with_dicts():
    """
    test that config manager can read files
    dict data that is well formatted as expected
    """

@mock_s3
def test_invalid_data_type_reading_file_with_lists_bad_internal():
    """
    test that config manager can read files
    list data with bad internal data types
    """
