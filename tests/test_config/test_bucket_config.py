#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Tests the configuration functions that creates mock sensor data.
Would love to make more but https://github.com/spulec/moto/issues/1524

.. sectionauthor:: Olutosin Sonuyi

"""

import pytest

from analytics_sdk_core.config import ConfigManager, \
DEFAULT_ENV, DEV_ENV, MASTER_ENV


def test_config_manager_bad_bucket_init():
	"""
	test that incorrect bucket environment initialization
	yields an error
	"""
	with pytest.raises(AssertionError):
		cm = ConfigManager("lambda name")
