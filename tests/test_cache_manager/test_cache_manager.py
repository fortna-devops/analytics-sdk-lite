#!/usr/bin/env python3
from analytics_sdk_core.cache.cache_functions import CacheManager, CacheClientEmulator
from analytics_sdk_core.testing_utilities import mock_data
import random
import logging
import string
import pytest
import json
import os
import hashlib
from pymemcache.exceptions import MemcacheError

"""
testing cache manager
.. sectionauthor:: Julia Varzari
.. sectionauthor:: Oluwatosin Sonuyi <oluwatosinsonuyi@mhsinc.net>

"""


@pytest.fixture
def cache_manager():
    return CacheManager()


def test_cache_client_is_mocked_locally():
    """
    test that when cache is run locally, the cache is mocked and
    does not use AWS

    this would timeout if the actual AWS cache was ran locally because
    the endpoint can't be discovered since it's inside VPC
    """
    cache_manager = CacheManager()

    assert cache_manager


def test_cache_manager_with_str_expiration():
    """
    test that cache expiration as a string works
    and is converted to proper units
    """
    cm = CacheManager(cache_expiration="2weeks")

    assert cm.cache_expiration == 2 * 7 * 24 * 60 * 60

    cm = CacheManager(cache_expiration="1weeks")

    assert cm.cache_expiration == 1 * 7 * 24 * 60 * 60

    cm = CacheManager(cache_expiration="2 hours")

    assert cm.cache_expiration == 2 * 60 * 60


def test_translate_multiple_key(cache_manager):
    """
    test that key translation is the same for 
    every cache manager instance. regardless
    of how many other keys were translated in between
    """
    new_cm = CacheManager()
    k = 'random*string?data=TEMP_F?start_date=10:01:00TZ-4'
    k1 = new_cm._translate_key(k)
    k2 = cache_manager._translate_key(k)
    k3 = new_cm._translate_key(k)
    k4 = cache_manager._translate_key(k)

    # create a third cm and translate some more keys (make sure hashobject doesnt update)
    another_cm = CacheManager()
    new_k = 'this_string_shouldnt_mess_up_translation'
    cache_manager._translate_key(new_k)
    new_cm._translate_key(new_k)
    another_cm._translate_key(new_k)

    k5 = cache_manager._translate_key(k)
    k6 = new_cm._translate_key(k)
    k7 = another_cm._translate_key(k)

    assert k1 == k2 == k3 == k4 == k5 == k6 == k7
    assert not k == k1


def test_json_cache_function(cache_manager):
    """
    tests that json strings are stored w/o issues
    using cache_function_result
    """

    file_dir = os.path.dirname(__file__)
    fpath = os.path.join(file_dir, 'test_data.json')

    def get_api_response(path):
        with open(path, 'r') as f:
            return f.read()

    # put it in the cache
    original = cache_manager.cache_function_result(get_api_response, "api params", False, fpath)
    # retrieve from cache
    cache_obj_pickle_loaded = cache_manager.cache_function_result(get_api_response, "api params", False, fpath)

    assert cache_obj_pickle_loaded == original


def generate_random_string(length=10):
    """
    generates a random string with numbers, letters, and special characters
    at a length of 10 characters

    :param length: length of the string
    :type length: int

    :returns: random string possibly containing letters, numbers, and/or \
    special characters
    :rtype: str

    """

    string_options = string.ascii_letters + string.digits + string.punctuation
    return (''.join(random.choice(string_options) for i in range(length)))


def test_cache_emulate_with_string_of_special_characters(cache_manager):
    """
    test that the string of special characters placed in cache is what 
    is retrieve from cache

    """
    cache_key = generate_random_string()
    data = generate_random_string()

    cache_manager.place_in_cache(cache_key=cache_key, data2cache=data)

    assert data == cache_manager.retrieve_from_cache(cache_key)


def test_cache_emulate_with_mock_data(cache_manager):
    """
    creating mock data using the mock data functions and testing
    that what we place in cache is what you retrieve from cache

    """

    data = mock_data.create_mock_location_data()

    cache_key = generate_random_string()

    cache_manager.place_in_cache(cache_key=cache_key, data2cache=data)

    assert data == cache_manager.retrieve_from_cache(cache_key)


def test_cache_emulate_with_dict(cache_manager):
    """
    test that a dictionary placed in cache is what is retrieved from
    cache

    """

    cache_key = generate_random_string()

    data = {'first element': 34, 'another': 45.31, 'string': 'another string', '4th': [8, 7, 6]}

    cache_manager.place_in_cache(cache_key=cache_key, data2cache=data)

    assert data == cache_manager.retrieve_from_cache(cache_key)


def test_cache_emulate_with_int(cache_manager):
    """
    test that an integer placed in cache is what is retrieved from cache

    """

    cache_key = generate_random_string()

    data = random.randint(0, 101)

    cache_manager.place_in_cache(cache_key=cache_key, data2cache=data)

    assert data == cache_manager.retrieve_from_cache(cache_key)


def test_cache_emulate_with_float(cache_manager):
    """
    test that a float placed in cache is what is retrieved from cache
    """

    cache_key = generate_random_string()

    data = random.uniform(0.0, 101.0)

    cache_manager.place_in_cache(cache_key=cache_key, data2cache=data)

    assert data == cache_manager.retrieve_from_cache(cache_key)


def test_cache_emulate_with_json_formatted_string(cache_manager):
    """
    test that a json formatted string placed in cache is what is 
    retrieved from cache
    """

    cache_key = generate_random_string()

    dict_for_json = {'just': 'a simple dict', 234: 764, 'ch': 56.7}

    data = json.dumps(dict_for_json)

    cache_manager.place_in_cache(cache_key=cache_key, data2cache=data)

    assert data == cache_manager.retrieve_from_cache(cache_key)


def test_cache_emulate_with_boolean_false(cache_manager):
    """
    test that when False is placed in cache is also what is retrieved from cache

    ..note:: when false is entered as data, it caches successfully, but when \
    retrieved it is logged that the cache key had no value associated with it \
    so it returned None. this is because in `cache_functions.py` it says \
    if cached_data: is this ok? or should it be changed to if not None
    """

    cache_key = generate_random_string()

    data = False

    cache_manager.place_in_cache(cache_key=cache_key, data2cache=data)

    assert data == cache_manager.retrieve_from_cache(cache_key)


def test_cache_emulate_with_boolean_true(cache_manager):
    """
    test that when True is placed in cache it is also retrieved from cache
    """

    cache_key = generate_random_string()

    data = True

    cache_manager.place_in_cache(cache_key=cache_key, data2cache=data)

    assert data == cache_manager.retrieve_from_cache(cache_key)


def test_cache_emulate_with_empty_string(cache_manager):
    """
    test that placing an empty string into cache is what you will retrieve \
    as well
    """

    cache_key = generate_random_string()

    data = ''

    cache_manager.place_in_cache(cache_key=cache_key, data2cache=data)

    assert data == cache_manager.retrieve_from_cache(cache_key)


def test_cache_emulate_with_zero(cache_manager):
    """
    test that placing a zero into cache is what will also be retrieved
    """

    cache_key = generate_random_string()

    data = 0

    cache_manager.place_in_cache(cache_key=cache_key, data2cache=data)

    assert data == cache_manager.retrieve_from_cache(cache_key)


def test_cache_emulate_with_empty_list(cache_manager):
    """
    test that placing an empty list into cache is also what is retrieved
    from cache

    """

    cache_key = generate_random_string()

    data = []

    cache_manager.place_in_cache(cache_key=cache_key, data2cache=data)

    assert data == cache_manager.retrieve_from_cache(cache_key)


def test_cache_emulate_with_none(cache_manager):
    """
    test that when None is placed as data into cache that None is also
    retrieved from cache
    """

    cache_key = generate_random_string()

    data = None

    cache_manager.place_in_cache(cache_key=cache_key, data2cache=data)

    assert cache_manager.retrieve_from_cache(cache_key) is None


def test_cache_emulate_with_list(cache_manager):
    """
    test that a list placed in cache is what is retrieved from cache

    """

    cache_key = generate_random_string()

    data = ['this', 'is', 'a', 'list']

    cache_manager.place_in_cache(cache_key=cache_key, data2cache=data)

    assert data == cache_manager.retrieve_from_cache(cache_key)


def test_retrieving_non_existant_key(cache_manager):
    """
    test that None is returned when retrieving a key that does not exist
    """

    assert cache_manager.retrieve_from_cache(cache_key='not_a_key') is None


def test_cache_key_not_string(cache_manager):
    """
    test that None is returned when passing a non-string cache_key in
    place_in_cache
    """

    cache_key = ['list', 'and', 'not', 'a', 'string']

    data = generate_random_string()

    assert cache_manager.place_in_cache(cache_key=cache_key, data2cache=data) is None


def test_overlapping_keys(cache_manager):
    """
    test that the data associated to a key is replaced when placing the 
    same key with different data in cache
    """

    cache_key = 'key'

    data1 = 'first set of data'

    data2 = 'second set of data'

    cache_manager.place_in_cache(cache_key=cache_key, data2cache=data1)

    assert data1 == cache_manager.retrieve_from_cache(cache_key=cache_key)

    cache_manager.place_in_cache(cache_key=cache_key, data2cache=data2)

    assert data2 == cache_manager.retrieve_from_cache(cache_key=cache_key)


def test_cache_function_result_retrieve(cache_manager):
    """
    cache function result
    then retrieve it from the cache_function_result method
    """

    cache_key = generate_random_string()

    def return_some_data(arg1, arg2, arg3):
        return 'some data'

    # places in the cache
    func_res = cache_manager.cache_function_result(return_some_data, cache_key, False, 1, 2, 3)

    # retrieves from the cache
    retrieved = cache_manager.cache_function_result(return_some_data, cache_key, False, 1, 2, 3)

    assert return_some_data(1, 2, 3) == retrieved


class SomeClassToCache():

    def __init__(self, name, attr):
        self.name = name
        self.attr = attr

    def getName(self):
        return self.name

    def getAttr(self):
        return self.attr


def test_cache_emulate_with_object(cache_manager):
    """
    test that if we place a python object in cache that the same python
    object is retrieved from cache
    """

    some_object = SomeClassToCache('the name', [{'key1': 57.283947}, {'gfdj': {'jj': 'qq'}}, {33: [3, 4, 5]}])

    cache_key = generate_random_string()

    cache_manager.place_in_cache(cache_key=cache_key, data2cache=some_object)

    assert some_object.getName() == cache_manager.retrieve_from_cache(cache_key=cache_key).getName()
    assert some_object.getAttr() == cache_manager.retrieve_from_cache(cache_key=cache_key).getAttr()
    assert isinstance(cache_manager.retrieve_from_cache(cache_key=cache_key), SomeClassToCache)


def test_cache_client_param():
    """
    test cache_client paramater by using the emulator
    """

    cache_manager = CacheManager(cache_client=CacheClientEmulator())

    cache_key = generate_random_string()
    data = generate_random_string()

    cache_manager.place_in_cache(cache_key=cache_key, data2cache=data)

    assert data == cache_manager.retrieve_from_cache(cache_key=cache_key)
