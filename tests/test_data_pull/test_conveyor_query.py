#!/usr/bin/env python3
"""
Tests conveyor query for the Athena Query Engine
.. sectionauthor:: Oluwatosin Sonuyi <oluwatosinsonuyi@mhsinc.net>
"""

import collections
from datetime import datetime
from dateutil import parser
import json
import csv
import pytest
import boto3
import s3fs
from moto import mock_s3
import pdb


from analytics_sdk_core.data_init.data_pull import construct_relative_timestamp_tuple
from analytics_sdk_core.data_init.data_pull import DataQueryEngineLite as DQE
from analytics_sdk_core.units_manager import Q_
from analytics_sdk_core.constants.card_colors import RED, YELLOW


# @mock_s3
@pytest.mark.skip("data query")
def test_conveyor_query_retrieval():

    bucket_name = 'mhspredict-site-mhs-testloop-atlanta'
    # create s3 bucket with mock location file in it
    # conn = boto3.resource('s3', region_name='us-east-1')

    aqe = DQE(s3_bucket_name=bucket_name)

    clist = aqe.get_conveyors()

    assert isinstance(clist, list)
    assert len(clist) > 0