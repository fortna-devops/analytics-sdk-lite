#!/usr/bin/env python3
"""
Tests other functionality of the Data Query Engine
.. sectionauthor:: Oluwatosin Sonuyi <oluwatosinsonuyi@mhsinc.net>
"""

import pytest
import boto3
import s3fs

from analytics_sdk_core.data_init.data_pull import construct_relative_timestamp_tuple, DataQueryEngineLite as DQE
from analytics_sdk_core.units_manager import Q_
from analytics_sdk_core.data_init.data_pull import DataQueryEngineLite as DQE
from analytics_sdk_core.data_init.data_write_lite import DataWriterLite as DW
from analytics_sdk_core.testing_utilities import mock_data
from moto import mock_s3



def test_relative_timestamp_construct():
    """
    test that we can construct tuple of relative timestamps
    """

    tzoffset = -4
    tback = '1 hours'

    timetuple = construct_relative_timestamp_tuple(
        time_back=tback,
        tzoffset=tzoffset
    )

    assert len(timetuple) == 2
    assert (timetuple[1] - timetuple[0]).total_seconds() == pytest.approx((Q_(tback).to('seconds')).magnitude)

def test_supersede_bucket():
    """
    test that s3 bucket name is either passed as an argument to the
    calling function or is specified as an attribute in the DQE object
    """
    
    # instantiate DQE object without an s3 bucket name specified
    dqe = DQE(s3_bucket_name=None)

    # call function on DQE object without an s3 bucket name specified
    with pytest.raises(ValueError) as e:
        dqe.execute_generic_query("""SELECT * FROM location LIMIT 3""", s3_bucket_name=None)
    
    assert e

@mock_s3
def test_cache_key_calculation():
    """
    """
    bname = "mhspredict-site-fake-bucket"
    conn = boto3.resource('s3', region_name='us-east-1')
    conn.create_bucket(Bucket=bname)

    stats_data = mock_data.create_mock_stats_data(sensors=7)
    dw = DW(bname)
    dw.write_stats_data_wrapper(stats_data)
    dqe = DQE(bname)
    set_of_keys = set()
    for idx,stat_entry in enumerate(stats_data):
        dqe.execute_stats_table_query(sensor_name=stat_entry['sensor'],gateway_name="fake-bucket",cache_expiration=1)
        key = list(dqe._cache_manager.memcache_client.cache_object.keys())[idx]
        set_of_keys.add(key)

    cache_struct = dqe._cache_manager.memcache_client.cache_object

    assert len(set_of_keys) == len(stats_data)
    


@mock_s3
def test_cache_manager_is_shared_within_DQE_instance():
    """
    test that cache_manager object is shared within DQE
    by checking that the number of keys stored in the 
    cache object is the same as the number of query calls
    """
    bname = "mhspredict-site-fake-bucket"
    conn = boto3.resource('s3', region_name='us-east-1')
    conn.create_bucket(Bucket=bname)

    stats_data = mock_data.create_mock_stats_data(sensors=7)
    dw = DW(bname)
    dw.write_stats_data_wrapper(stats_data)
    dqe = DQE(bname)
    set_of_keys = set()
    for idx,stat_entry in enumerate(stats_data):
        dqe.execute_stats_table_query(sensor_name=stat_entry['sensor'],gateway_name="fake-bucket",cache_expiration=1)
        key = list(dqe._cache_manager.memcache_client.cache_object.keys())[idx]
        set_of_keys.add(key)

    cache_struct = dqe._cache_manager.memcache_client.cache_object

    assert len(cache_struct.keys()) == len(set_of_keys)

@mock_s3
def test_cache_retrieval():
    """
    call stats retrieval twice and make sure that data is retrieved from cache
    """
    bname = "mhspredict-site-fake-bucket"
    gwname = "fake-bucket"
    conn = boto3.resource('s3', region_name='us-east-1')
    conn.create_bucket(Bucket=bname)

    stats_data = mock_data.create_mock_stats_data(sensors=1)
    dw = DW(bname)
    dw.write_stats_data_wrapper(stats_data)
    dqe = DQE(bname)
    set_of_keys = set()
    fs = s3fs.S3FileSystem()

    cached_stats_data = []
    for idx, stat_entry in enumerate(stats_data):
        sname = stat_entry['sensor']
        dqe.execute_stats_table_query(sensor_name=sname,gateway_name=gwname,cache_expiration=1)
        key = list(dqe._cache_manager.memcache_client.cache_object.keys())[idx]

        set_of_keys.add(key)

        # Delete stats data from S3 so subsequent query returns results from cache
        full_path = f"s3://{bname}/{gwname}/stats"
        # fs.rm(full_path)
        fs.bulk_delete(fs.ls(full_path))

        results_after_deletion = dqe.execute_stats_table_query(sensor_name=sname,gateway_name=gwname,cache_expiration=9909)

        # Return cached query results
        cached_stats_data.append(results_after_deletion)

        assert len(stats_data) == len(cached_stats_data)
