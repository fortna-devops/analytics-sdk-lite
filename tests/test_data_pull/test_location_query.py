#!/usr/bin/env python3
"""
Tests location query for the Athena Query Engine
.. sectionauthor:: Oluwatosin Sonuyi <oluwatosinsonuyi@mhsinc.net>
"""
import collections
from datetime import datetime
from dateutil import parser
import json
import csv
import pytest
import boto3
import s3fs
from moto import mock_s3
import pdb


from analytics_sdk_core.data_init.data_pull import construct_relative_timestamp_tuple
from analytics_sdk_core.data_init.data_pull import DataQueryEngineLite as DQE
from analytics_sdk_core.units_manager import Q_
from analytics_sdk_core.constants.card_colors import RED, YELLOW


@mock_s3
def test_sensor_location_query_with_df_raises_error():
    """
    test sensor location query with s3 direct file read
    raises an error when you try to return_as_df=True
    """

    bucket_name = 'mhspredict-testing'
    snumber = '444'

    # create s3 bucket with mock location file in it
    conn = boto3.resource('s3', region_name='us-east-1')

    conn.create_bucket(Bucket=bucket_name)
    s3_client = boto3.client('s3')
    data2store = 'sensor,description,manufacturer,conveyor,location,equipment,m_sensor,standards_score,temp_score,vvrms_score,thresholds\n444,Recirc 1,banner,SR1-8,M,M,10,1,1,1,`{"temperature": {"red": 140, "orange": 122}, "rms_acceleration": {"red": 0.37, "orange": 0.18, "blue": 0.07}}`\n'
    s3_client.put_object(Bucket=bucket_name, Body=data2store, Key='test/location/location_{}.csv'.format(snumber))

    dqe = DQE(s3_bucket_name=bucket_name)

    with pytest.raises(NotImplementedError) as ni_error:
        location_result = dqe.execute_location_query(sensor_name=snumber, gateway_name='test', return_as_df=True)
    assert ni_error


@mock_s3
def test_sensor_location_query_without_df():
    """
    test sensor location query with s3 direct file read
    with return_as_df=False
    """

    bucket_name = 'mhspredict-testing'
    snumber = '444'

    # create s3 bucket with mock location file in it
    conn = boto3.resource('s3', region_name='us-east-1')

    conn.create_bucket(Bucket=bucket_name)
    s3_client = boto3.client('s3')
    data2store = 'sensor,description,manufacturer,conveyor,location,equipment,m_sensor,standards_score,temp_score,vvrms_score,thresholds\n444,Recirc 1,banner,SR1-8,M,M,10,1,1,1,`{"temperature": {"red": 140, "orange": 122}, "rms_acceleration": {"red": 0.37, "orange": 0.18, "blue": 0.07}}`\n'
    s3_client.put_object(Bucket=bucket_name, Body=data2store, Key='test/location/location_{}.csv'.format(snumber))

    dqe = DQE(s3_bucket_name=bucket_name)

    location_result = dqe.execute_location_query(sensor_name=snumber, gateway_name='test', return_as_df=False)

    assert isinstance(location_result[0], collections.OrderedDict)
    header_names = 'sensor,description,manufacturer,conveyor,location,equipment,m_sensor,standards_score,temp_score,vvrms_score,thresholds'

    assert len(location_result[0].keys()) == len(header_names.split(","))
    for colname in location_result[0].keys():
        assert colname in header_names.split(",")

    assert int(location_result[0]['sensor']) == int(snumber)
    # make sure jsons can be parsed appropriately
    assert isinstance(json.loads(location_result[0]['thresholds']), dict)


@mock_s3
def test_sensor_location_query_with_cache():
    """
    test sensor location query with s3 direct file read
    with cache
    """

    bucket_name = 'mhspredict-testing'
    snumber = '444'

    # create s3 bucket with mock location file in it
    conn = boto3.resource('s3', region_name='us-east-1')

    conn.create_bucket(Bucket=bucket_name)
    s3_client = boto3.client('s3')
    data2store = 'sensor,description,manufacturer,conveyor,location,equipment,m_sensor,standards_score,temp_score,vvrms_score,thresholds\n444,Recirc 1,banner,SR1-8,M,M,10,1,1,1,`{"temperature": {"red": 140, "orange": 122}, "rms_acceleration": {"red": 0.37, "orange": 0.18, "blue": 0.07}}`\n'
    s3_client.put_object(Bucket=bucket_name, Body=data2store, Key='test/location/location_{}.csv'.format(snumber))

    dqe = DQE(s3_bucket_name=bucket_name)

    location_result = dqe.execute_location_query(
        sensor_name=snumber, gateway_name='test', return_as_df=False, cache_expiration=1)

    assert isinstance(location_result[0], collections.OrderedDict)
    header_names = 'sensor,description,manufacturer,conveyor,location,equipment,m_sensor,standards_score,temp_score,vvrms_score,thresholds'

    assert len(location_result[0].keys()) == len(header_names.split(","))
    for colname in location_result[0].keys():
        assert colname in header_names.split(",")

    assert int(location_result[0]['sensor']) == int(snumber)
    # make sure jsons can be parsed appropriately
    assert isinstance(json.loads(location_result[0]['thresholds']), dict)

    # call again with cached data and make sure ther esults are the same
    location_result_cached = dqe.execute_location_query(
        sensor_name=snumber, gateway_name='test', return_as_df=False, cache_expiration=1)

    assert isinstance(location_result_cached[0], collections.OrderedDict)
    header_names = 'sensor,description,manufacturer,conveyor,location,equipment,m_sensor,standards_score,temp_score,vvrms_score,thresholds'

    assert len(location_result_cached[0].keys()) == len(header_names.split(","))
    for colname in location_result_cached[0].keys():
        assert colname in header_names.split(",")

    assert int(location_result_cached[0]['sensor']) == int(snumber)
    # make sure jsons can be parsed appropriately
    assert isinstance(json.loads(location_result_cached[0]['thresholds']), dict)
