#!/usr/bin/env python3
"""
Tests functionality of the Data Query Engine that is only in full version
Each of these tests just makes sure that NotImplementedError are raised
for functions that should not exist in here
.. sectionauthor:: Oluwatosin Sonuyi <oluwatosinsonuyi@mhsinc.net>
"""

import pytest


from analytics_sdk_core.data_init.data_pull import DataQueryEngineLite as DQE
from analytics_sdk_core.units_manager import Q_


def test_timeseries_pull():
    """
    test that we cant call timeseries query execution
    """

    dqe = DQE("")

    with pytest.raises(NotImplementedError) as e:
        dqe.execute_timeseries_data_query(
            taglist=[],
            conveyor='',
            equip='',
            timerange_tuple=('', '')
        )
    assert e


def test_timeseries_string_constructions():
    """
    test that we cant call construct ts query string
    """

    dqe = DQE("")

    with pytest.raises(NotImplementedError) as e:
        dqe._construct_ts_query_string(
            conveyor='',
            equip='',
            taglist=[],
            timerange_tuple=('', '')
        )
    assert e


def test_query_results_as_dataframe():
    """
    test that we cannot call dataframe return result
    """

    dqe = DQE("")

    with pytest.raises(NotImplementedError) as e:
        dqe._format_query_results_as_df(
            result_lines=[]
        )
    assert e
