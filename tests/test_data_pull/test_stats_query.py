#!/usr/bin/env python3
"""
Tests stats query for the Athena Query Engine
.. sectionauthor:: Oluwatosin Sonuyi <oluwatosinsonuyi@mhsinc.net>
"""

import collections
from datetime import datetime
from dateutil import parser
import json
import csv
import pytest
import boto3
import s3fs
from moto import mock_s3
import pdb


from analytics_sdk_core.data_init.data_pull import construct_relative_timestamp_tuple
from analytics_sdk_core.data_init.data_pull import DataQueryEngineLite as DQE
from analytics_sdk_core.units_manager import Q_
from analytics_sdk_core.constants.card_colors import RED, YELLOW


@mock_s3
def test_sensor_stats_query_with_df_raises_error():
    """
    test sensor stats query with s3 direct file read
    raises an error when you try to return_as_df=True
    """
    bucket_name = 'mhspredict-testing'
    snumber = '449'

    # create s3 bucket with mock location file in it
    conn = boto3.resource('s3', region_name='us-east-1')
    conn.create_bucket(Bucket=bucket_name)
    s3_client = boto3.client('s3')
    data2store = "sensor,temperature_mean,temperature_perc95,rms_velocity_z_mean,rms_velocity_z_perc95\n449,0,0,0,0"
    s3_client.put_object(Bucket=bucket_name, Body=data2store, Key='test/stats/stats_table_{}.csv'.format(snumber))

    dqe = DQE(s3_bucket_name=bucket_name)
    with pytest.raises(NotImplementedError) as ni_error:
        stats_results = dqe.execute_stats_table_query(sensor_name=snumber, gateway_name='test', return_as_df=True)
    assert ni_error


@mock_s3
def test_sensor_stats_query_without_df():
    """
    test sensor stats query with s3 direct file read
    with return_as_df=False
    """
    bucket_name = 'mhspredict-testing'
    snumber = '449'

    # create s3 bucket with mock location file in it
    conn = boto3.resource('s3', region_name='us-east-1')
    conn.create_bucket(Bucket=bucket_name)
    s3_client = boto3.client('s3')
    data2store = "sensor,temperature_mean,temperature_perc95,rms_velocity_z_mean,rms_velocity_z_perc95\n449,0,0,0,0"
    s3_client.put_object(Bucket=bucket_name, Body=data2store, Key='test/stats/stats_table_{}.csv'.format(snumber))

    dqe = DQE(s3_bucket_name=bucket_name)

    stats_results = dqe.execute_stats_table_query(sensor_name=snumber, gateway_name='test', return_as_df=False)

    assert isinstance(stats_results[0], collections.OrderedDict)
    header_names = "sensor,temperature_mean,temperature_perc95,rms_velocity_z_mean,rms_velocity_z_perc95"
    assert len(stats_results[0].keys()) == len(header_names.split(","))
    for colname in stats_results[0].keys():
        assert colname in header_names.split(",")
    assert int(stats_results[0]['sensor']) == int(snumber)


@mock_s3
def test_sensor_stats_query_with_caching():
    """
    test sensor stats query with s3 direct file read
    with cache_expiration set retrieves correct
    """
    bucket_name = 'mhspredict-testing'
    snumber = '449'

    # create s3 bucket with mock location file in it
    conn = boto3.resource('s3', region_name='us-east-1')
    conn.create_bucket(Bucket=bucket_name)
    s3_client = boto3.client('s3')
    data2store = "sensor,temperature_mean,temperature_perc95,rms_velocity_z_mean,rms_velocity_z_perc95\n449,0,0,0,0"
    s3_client.put_object(Bucket=bucket_name, Body=data2store, Key='test/stats/stats_table_{}.csv'.format(snumber))

    dqe = DQE(s3_bucket_name=bucket_name)

    stats_results = dqe.execute_stats_table_query(sensor_name=snumber, gateway_name='test',
                                                  return_as_df=False, cache_expiration=50)

    assert isinstance(stats_results[0], collections.OrderedDict)
    header_names = "sensor,temperature_mean,temperature_perc95,rms_velocity_z_mean,rms_velocity_z_perc95"
    assert len(stats_results[0].keys()) == len(header_names.split(","))
    for colname in stats_results[0].keys():
        assert colname in header_names.split(",")
    assert int(stats_results[0]['sensor']) == int(snumber)

    # call again using the cached data and ensure data is the same
    stats_results_cached = dqe.execute_stats_table_query(sensor_name=snumber, gateway_name='test',
                                                         return_as_df=False, cache_expiration=50)

    assert isinstance(stats_results_cached[0], collections.OrderedDict)
    header_names = "sensor,temperature_mean,temperature_perc95,rms_velocity_z_mean,rms_velocity_z_perc95"
    assert len(stats_results[0].keys()) == len(header_names.split(","))
    for colname in stats_results_cached[0].keys():
        assert colname in header_names.split(",")
    assert int(stats_results_cached[0]['sensor']) == int(snumber)
