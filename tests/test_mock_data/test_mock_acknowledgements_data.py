from analytics_sdk_core.testing_utilities import mock_data
import pytest


def test_ackn_num_param():
    """
    Test that create_mock_acknowledgements_data() actually creates
    as many acknowledgements as specified by the ackn_num argument
    """
    # Generate random int to use as ackn_num
    random_int = mock_data.generate_random_int()
    ackn_num = random_int

    # Create ackn_num number of acknowledgements
    ackn_created = mock_data.create_mock_acknowledgements_data(ackn_num)

    assert len(ackn_created) == ackn_num


def test_invalid_ackn_num_argument_type():
    """
    Test that TypeError exception is raised when wrong type is passed as
    ackn_num argument
    """
    # Create mock acknowledgement using invalid ackn_num argument type
    with pytest.raises(TypeError):
        mock_data.create_mock_acknowledgements_data('a')


def test_return_list_of_dicts():
    """
    Test that the acknowledgements are a list of dicts inside the JSON
    encoded string
    """
    # Create random number of mock acknowledgements
    rand_num = mock_data.generate_random_int()
    ackn = mock_data.create_mock_acknowledgements_data(rand_num)

    # Check if all items in ackn list are dicts
    assert all(isinstance(ack, dict) for ack in ackn)
