#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Tests the function that creates mock sensor data.

.. sectionauthor:: Julia Varzari

"""

import pytest
from collections import OrderedDict
import time
import random
#import pdb

from analytics_sdk_core.testing_utilities import mock_data
from analytics_sdk_core.testing_utilities import table_types
from analytics_sdk_core.constants import table_schemas


def test_sensors_param():
    """
    test that the amount of sensors returned is the same as the amount 
    passed in
    """

    sensors_given = 10
    sensor_table = mock_data.create_mock_sensor_data(sensors=sensors_given, datapts=1)
    sensors_returned = len(sensor_table)

    assert sensors_given == sensors_returned


def test_location_table_param():
    """
    test that the amound of sensors returned is the same as the amound
    of sensors in the location table that was passed in
    """

    loc_table = [OrderedDict([('sensor', '0'), ('conveyor', 'FGkpe'), ('equipment', 'KbE3f')]),
                 OrderedDict([('sensor', '1'), ('conveyor', 'QS60J'), ('equipment', 'peIKu')])]
    datapts_given = 5
    sensor_table = mock_data.create_mock_sensor_data(location_table=loc_table, datapts=datapts_given)

    assert len(loc_table) == (len(sensor_table) / datapts_given)


def test_datapts_with_sensors_param():
    """
    test that the amount of datapoints per sensor is as specified in the `datapts` 
    parameter when using the `sensors` parameter
    """

    datapts_given = 10
    sensors_given = 5
    sensor_table = mock_data.create_mock_sensor_data(sensors=sensors_given, datapts=datapts_given)

    assert datapts_given == (len(sensor_table) / sensors_given)


def test_datapts_with_location_table_param():
    """
    test that the amount of datapoints per sensor is as specified in the `datapts`
    parameter when using the `location_table` parameter
    """

    datapts_given = 10
    loc_table = loc_table = [OrderedDict([('sensor', '0'), ('conveyor', 'FGkpe'), ('equipment', 'KbE3f')]), OrderedDict([
        ('sensor', '1'), ('conveyor', 'QS60J'), ('equipment', 'peIKu')])]
    sensor_table = mock_data.create_mock_sensor_data(location_table=loc_table, datapts=datapts_given)

    assert datapts_given == (len(sensor_table) / len(loc_table))


def test_both_sensor_and_loc_table_param():
    """
    test that an error is rasied when both the sensor parameter and location_table
    paramater is passed in
    """

    with pytest.raises(ValueError) as both_error:
        mock_data.create_mock_sensor_data(sensors=10, location_table=[OrderedDict([('sensor', '0'), ('conveyor', 'FGkpe'), (
            'equipment', 'KbE3f')]), OrderedDict([('sensor', '1'), ('conveyor', 'QS60J'), ('equipment', 'peIKu')])])
    assert both_error


def test_neither_sensor_nor_loc_table_param():
    """
    test that an error is raised when neither the sensor paramater nor the
    location_table parameter is passed in
    """

    with pytest.raises(ValueError) as neither_error:
        mock_data.create_mock_sensor_data(sensors=0, location_table=None)
    assert neither_error


def test_warning_invalid_distr():
    """
    test that there is a warning when an invalid distribution is passed in
    and that the distribution is then set to the default 'random'
    """

    with pytest.warns(UserWarning) as invalid_distr:
        mock_data.create_mock_sensor_data(sensors=10, distribution='invalid_distr')
    assert invalid_distr


def test_field_validation():
    """
    test that an error is raised when invalid fields are passed in
    """

    fields_passed = ['timestamp', 'temperature', 'not_a_valid_field']

    with pytest.raises(AttributeError) as invalid_field:
        mock_data.create_mock_sensor_data(sensors=10, fields=fields_passed)
    assert invalid_field


def test_passed_fields_match_returned():
    """
    tests that if a list of fields are passed as a parameter that those
    fields are actually used when a sensor table is created
    """

    # random amount of fields
    rand_num_fields = random.randint(0, len(table_schemas.SENSOR))

    # random fields with random amount
    fields_passed = random.sample(table_schemas.SENSOR, k=rand_num_fields)

    sensor_table = mock_data.create_mock_sensor_data(sensors=10, fields=fields_passed)

    for row in sensor_table:

        # checking if amount is equal
        assert len(row) == len(fields_passed)

        for col in row:

            # checking if the content is the same
            assert col in fields_passed


def test_matching_data_type_str():
    """
    test that if the field data type should be a string that the function is
    in fact returning a string for that field
    """

    fields_passed = []

    for field in table_types.SENSOR_TYPES:
        if table_types.SENSOR_TYPES[field] is str:
            fields_passed.append(field)

    sensor_table = mock_data.create_mock_sensor_data(sensors=10, fields=fields_passed)

    for row in sensor_table:

        for col in row:

            assert isinstance(row[col], str)


def test_matching_data_type_float():
    """
    test that if the field data type should be a float that the function is
    in fact returning a float for that field
    """

    fields_passed = []

    for field in table_types.SENSOR_TYPES:
        if table_types.SENSOR_TYPES[field] is float:
            fields_passed.append(field)

    sensor_table = mock_data.create_mock_sensor_data(sensors=10, fields=fields_passed)

    for row in sensor_table:

        for col in row:

            assert isinstance(row[col], float)


def test_matching_data_type_timestamp():
    """
    test that a timestamp in the correct format is returned when the timestamp
    field is needed
    """
    sensor_table = mock_data.create_mock_sensor_data(sensors=1, fields=['timestamp'])
    sensor_table_row = sensor_table[0]
    sensor_timestamp = sensor_table_row['timestamp']

    assert time.strptime(sensor_timestamp, '%Y-%m-%d %H:%M:%S')


def test_returns_list_of_dicts():
    """
    tests if the function returns a list of dictionaries
    """

    sensor_table = mock_data.create_mock_sensor_data(sensors=5)

    assert isinstance(sensor_table, list)

    for row in sensor_table:

        assert isinstance(row, dict)
