#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Tests the function that creates mock stats data.

.. sectionauthor:: Julia Varzari

"""

import pytest
from collections import OrderedDict
import time
import random
#import pdb

from analytics_sdk_core.testing_utilities import mock_data
from analytics_sdk_core.testing_utilities import table_types
from analytics_sdk_core.constants import table_schemas

# hardcoded location table for tests
LOCATION_TABLE = [OrderedDict([('sensor', '0'), ('conveyor', 'FGkpe'), ('equipment', 'KbE3f')]),
                  OrderedDict([('sensor', '1'), ('conveyor', 'QS60J'), ('equipment', 'peIKu')])]


def test_sensors_param():
    """
    test that the amount of sensors returned is the same as the amount 
    passed in
    """

    sensors_given = 10
    stats_table = mock_data.create_mock_stats_data(sensors=sensors_given)
    sensors_returned = len(stats_table)

    assert sensors_given == sensors_returned


def test_location_table_param():
    """
    test that the amound of sensors returned is the same as the amount
    of sensors in the location table that was passed in
    """

    stats_table = mock_data.create_mock_stats_data(location_table=LOCATION_TABLE)

    assert len(LOCATION_TABLE) == len(stats_table)


def test_both_sensor_and_loc_table_param():
    """
    test that an error is rasied when both the sensor parameter and location_table
    paramater is passed in
    """

    with pytest.raises(ValueError) as both_error:
        mock_data.create_mock_stats_data(sensors=10, location_table=LOCATION_TABLE)
    assert both_error


def test_neither_sensor_nor_loc_table_param():
    """
    test that an error is raised when neither the sensor paramater nor the
    location_table parameter is passed in
    """

    with pytest.raises(ValueError) as neither_error:
        mock_data.create_mock_stats_data(sensors=0, location_table=None)
    assert neither_error


def test_field_validation():
    """
    test that an error is raised when invalid fields are passed in
    """

    fields_passed = ['not_a_valid_field']

    with pytest.raises(AttributeError) as invalid_field:
        mock_data.create_mock_stats_data(sensors=10, fields=fields_passed)
    assert invalid_field


def test_passed_fields_match_returned():
    """
    tests that if a list of fields are passed as a parameter that those
    fields are actually used when a stats table is created
    """

    # random amount of fields
    rand_num_fields = random.randint(0, len(table_schemas.STATS))

    # random fields with random amount
    fields_passed = random.sample(table_schemas.STATS, k=rand_num_fields)

    stats_table = mock_data.create_mock_stats_data(sensors=10, fields=fields_passed)

    for row in stats_table:

        # checking if amount is equal
        assert len(row) == len(fields_passed)

        for col in row:

            # checking if the content is the same
            assert col in fields_passed


def test_matching_data_type_str():
    """
    test that if the field data type should be a string that the function is
    in fact returning a string for that field
    """

    fields_passed = []

    for field in table_types.STATS_TYPES:
        if table_types.STATS_TYPES[field] is str:
            fields_passed.append(field)

    stats_table = mock_data.create_mock_stats_data(sensors=10, fields=fields_passed)

    for row in stats_table:

        for col in row:

            assert isinstance(row[col], str)


def test_matching_data_type_float():
    """
    test that if the field data type should be a float that the function is
    in fact returning a float for that field
    """

    fields_passed = []

    for field in table_types.STATS_TYPES:
        if table_types.STATS_TYPES[field] is float:
            fields_passed.append(field)

    stats_table = mock_data.create_mock_stats_data(sensors=10, fields=fields_passed)

    for row in stats_table:

        for col in row:

            assert isinstance(row[col], float)


def test_returns_list_of_dicts():
    """
    tests if the function returns a list of dictionaries
    """

    stats_table = mock_data.create_mock_stats_data(sensors=5)

    assert isinstance(stats_table, list)

    for row in stats_table:

        assert isinstance(row, dict)
