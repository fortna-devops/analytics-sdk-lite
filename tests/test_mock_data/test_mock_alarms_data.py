#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Tests the function that creates mock alarms data.

.. sectionauthor:: Julia Varzari

"""

import pytest
from collections import OrderedDict
import time
import random
#import pdb

from analytics_sdk_core.testing_utilities import mock_data
from analytics_sdk_core.testing_utilities import table_types
from analytics_sdk_core.constants import table_schemas


def test_alarms_param():
    """
    tests if the create_mock_alarms_data function returns a table with
    the same amount of alarms as specified in the `alarms` parameter
    when given a location table with 2 sensors
    """

    alarms_given = 10
    loc_table = [OrderedDict([('sensor', '0'), ('conveyor', 'FGkpe'), ('equipment', 'KbE3f')]),
                 OrderedDict([('sensor', '1'), ('conveyor', 'QS60J'), ('equipment', 'peIKu')])]

    alarms_table = mock_data.create_mock_alarms_data(alarms=alarms_given, location_table=loc_table)
    alarms_returned = (len(alarms_table) / len(loc_table))

    assert alarms_given == alarms_returned


def test_missing_fields_error():
    """
    test if AttributeError is raised when a location table with missing fields
    is passed
    """

    loc_table_missing_field = [OrderedDict([('sensor', '0'), ('conveyor', '3BL85')]),
                               OrderedDict([('sensor', '1'), ('conveyor', 'w13sY')])]

    with pytest.raises(AttributeError) as missing_field:
        mock_data.create_mock_alarms_data(location_table=loc_table_missing_field)
    assert missing_field


def test_fields_matching_error():
    """
    test that an error is raised when item(s) in the list of fields passed
    does not match the alarms table fields listed in `table_schemas.py`
    """

    fields_passed = ['equipment', 'severety', 'not_a_valid_field']

    with pytest.raises(AttributeError) as field_error:
        mock_data.create_mock_alarms_data(fields=fields_passed)
    assert field_error


def test_value_without_key_warning():
    """
    test that the user is warned when an alarms table is created with
    the field 'value' passed in without the field 'key'

    """

    with pytest.warns(UserWarning) as value_without_key:
        mock_data.create_mock_alarms_data(fields=['value'])
    assert value_without_key


def test_passed_fields_match_returned():
    """
    tests that if a list of fields are passed as a parameter that those
    fields are actually used when a alarms table is created
    """

    # random amount of fields
    rand_num_fields = random.randint(0, len(table_schemas.ALARMS))

    # random fields with random amount
    fields_passed = random.sample(table_schemas.ALARMS, k=rand_num_fields)

    alarms_table = mock_data.create_mock_alarms_data(fields=fields_passed)

    for row in alarms_table:

        # checking if amount is equal
        assert len(row) == len(fields_passed)

        for col in row:

            # checking if the content is the same
            assert col in fields_passed


def test_matching_data_type_str():
    """
    test that if the field data type should be a string that the function is
    in fact returning a string for that field
    """

    fields_passed = []

    for field in table_types.ALARM_TYPES:
        if table_types.ALARM_TYPES[field] is str:
            fields_passed.append(field)

    alarms_table = mock_data.create_mock_alarms_data(fields=fields_passed)

    for row in alarms_table:

        for col in row:

            assert isinstance(row[col], str)


def test_matching_data_type_int():
    """
    test that if the field data type should be an integer that the function is
    in fact returning an integer for that field
    """

    fields_passed = []

    for field in table_types.ALARM_TYPES:
        if table_types.ALARM_TYPES[field] is int:
            fields_passed.append(field)

    alarms_table = mock_data.create_mock_alarms_data(fields=fields_passed)

    for row in alarms_table:

        for col in row:

            assert isinstance(row[col], int)


def test_matching_data_type_float():
    """
    test that if the field data type should be a float that the function is
    in fact returning a float for that field
    """

    fields_passed = []

    for field in table_types.ALARM_TYPES:
        if table_types.ALARM_TYPES[field] is float:
            fields_passed.append(field)

    alarms_table = mock_data.create_mock_alarms_data(fields=fields_passed)

    for row in alarms_table:

        for float_col in row:

            assert isinstance(row[float_col], float)


def test_matching_data_type_timestamp():
    """
    test that a timestamp in the correct format is returned when the timestamp
    field is needed
    """
    alarms_table = mock_data.create_mock_alarms_data(alarms=1, fields=['timestamp'])
    alarms_table_row = alarms_table[0]
    alarms_timestamp = alarms_table_row['timestamp']

    assert time.strptime(alarms_timestamp, '%Y-%m-%d %H:%M:%S')


def test_returns_list_of_dicts():
    """
    tests if the function returns a list of dictionaries
    """

    alarms_table = mock_data.create_mock_alarms_data()

    assert isinstance(alarms_table, list)

    for row in alarms_table:

        assert isinstance(row, dict)


def test_backticks_param_true():
    """
    test that if the backticks paramater is true that the thresholds is
    quoted with backticks(`)
    """

    alarms_table = mock_data.create_mock_alarms_data(backticks=True)

    for row in alarms_table:
        for field in row:
            if table_types.ALARM_TYPES[field] == 'stringdict':

                # getting the string dict
                strdict = row[field]

                # asserting that the first and last characters are backticks
                assert strdict[0] == '`'
                assert strdict[len(strdict) - 1] == '`'

                """
                asserting that the second and second to last characters are curly
                brackers since it is a dictionary
                """
                assert strdict[1] == '{'
                assert strdict[len(strdict) - 2] == '}'


def test_backticks_param_false():
    """
    test that if the backticks paramater is false that the thresholds is
    NOT quoted with backticks(`)
    """

    alarms_table = mock_data.create_mock_alarms_data(backticks=False)

    for row in alarms_table:
        for field in row:
            if table_types.ALARM_TYPES[field] == 'stringdict':

                # getting the string dict
                strdict = row[field]

                # asserting that the first and last characters are backticks
                assert strdict[0] != '`'
                assert strdict[len(strdict) - 1] != '`'

                """
                asserting that the second and second to last characters are curly
                brackers since it is a dictionary
                """
                assert strdict[0] == '{'
                assert strdict[len(strdict) - 1] == '}'
