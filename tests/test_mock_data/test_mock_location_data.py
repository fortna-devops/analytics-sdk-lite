#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Tests the function that creates mock location data.

.. sectionauthor:: Julia Varzari

"""

import pytest
from collections import OrderedDict
import time
import random
#import pdb

from analytics_sdk_core.testing_utilities import mock_data
from analytics_sdk_core.testing_utilities import table_types
from analytics_sdk_core.constants import table_schemas


def test_sensor_param():
    """
    tests if the create_mock_location_data function returns a table with
    the same amount of sensors as specified in the `sensors` parameter
    """

    sensors_given = 10
    location_table = mock_data.create_mock_location_data(sensors=sensors_given)
    sensors_returned = len(location_table)

    assert sensors_given == sensors_returned


def test_fields_matching_error():
    """
    test that an error is raised when item(s) in the list of fields passed
    does not match the location table fields listed in `table_schemas.py`
    """

    fields_passed = ['sensor', 'conveyor', 'not_a_valid_field']

    with pytest.raises(AttributeError) as field_error:
        mock_data.create_mock_location_data(fields=fields_passed)
    assert field_error


def test_passed_fields_match_returned():
    """
    tests that if a list of fields are passed as a parameter that those
    fields are actually used when a location table is created
    """

    # random amount of fields
    rand_num_fields = random.randint(0, len(table_schemas.LOCATION))

    # random fields with random amount
    fields_passed = random.sample(table_schemas.LOCATION, k=rand_num_fields)

    location_table = mock_data.create_mock_location_data(fields=fields_passed)

    for row in location_table:

        # checking if amount is equal
        assert len(row) == len(fields_passed)

        for col in row:

            # checking if the content is the same
            assert col in fields_passed


def test_matching_data_type_str():
    """
    test that if the field data type should be a string that the function is
    in fact returning a string for that field
    """

    fields_passed = []

    for field in table_types.LOCATION_TYPES:
        if table_types.LOCATION_TYPES[field] is str:
            fields_passed.append(field)

    location_table = mock_data.create_mock_location_data(fields=fields_passed)

    for row in location_table:

        for col in row:

            assert isinstance(row[col], str)


def test_matching_data_type_int():
    """
    test that if the field data type should be an integer that the function is
    in fact returning an integer for that field
    """

    fields_passed = []

    for field in table_types.LOCATION_TYPES:
        if table_types.LOCATION_TYPES[field] is int:
            fields_passed.append(field)

    location_table = mock_data.create_mock_location_data(fields=fields_passed)

    for row in location_table:

        for col in row:

            assert isinstance(row[col], int)


def test_returns_list_of_dicts():
    """
    tests if the function returns a list of dictionaries
    """

    location_table = mock_data.create_mock_location_data()

    assert isinstance(location_table, list)

    for row in location_table:

        assert isinstance(row, dict)


def test_backticks_param_true():
    """
    test that if the backticks paramater is true that the thresholds is
    quoted with backticks(`)
    """

    location_table = mock_data.create_mock_location_data(backticks=True)

    for row in location_table:
        for field in row:
            if table_types.LOCATION_TYPES[field] == 'stringdict':

                # getting the string dict
                strdict = row[field]

                # asserting that the first and last characters are backticks
                assert strdict[0] == '`'
                assert strdict[len(strdict) - 1] == '`'

                """
                asserting that the second and second to last characters are curly
                brackers since it is a dictionary
                """
                assert strdict[1] == '{'
                assert strdict[len(strdict) - 2] == '}'


def test_backticks_param_false():
    """
    test that if the backticks paramater is false that the thresholds is
    NOT quoted with backticks(`)
    """

    location_table = mock_data.create_mock_location_data(backticks=False)

    for row in location_table:
        for field in row:
            if table_types.LOCATION_TYPES[field] == 'stringdict':

                # getting the string dict
                strdict = row[field]

                # asserting that the first and last characters are not backticks
                assert strdict[0] != '`'
                assert strdict[len(strdict) - 1] != '`'

                # asserting that the first and last characters are curly brackets since it's a dict
                assert strdict[0] == '{'
                assert strdict[len(strdict) - 1] == '}'

test_backticks_param_false()
print("ran")
