from setuptools import setup,find_packages
import versioneer

with open('requirements.txt') as f:
	required = f.read().splitlines()

setup(
	name='analytics_sdk_core',
	install_requires=required,
	version=versioneer.get_version(),
	cmdclass=versioneer.get_cmdclass(),
	packages=find_packages(),
	description='A lite implementation of analytic sdk core functionality.',
	author='MHS Insights Team',
	author_email='oluwatosinsonuyi@mhsinc.net'
)
